﻿using Arbitrage.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Arbitrage.Exchange.Binance
{
	public class CoinsWithdrawFeeCollection
    {
	    private readonly HttpClient _httpClient;
	    private readonly string _feeUrl;
		private readonly SemaphoreSlim _semaphoreSlim = new SemaphoreSlim(1, 1);

		private Dictionary<Coin, CoinFee> _fees;

	    public CoinsWithdrawFeeCollection(HttpClient httpClient, string feeUrl)
	    {
		    _httpClient = httpClient;
		    _feeUrl = feeUrl;
	    }

		public async Task<decimal> Fee(Coin coin, decimal amount)
		{
			var fees = await Fees();
			if (fees.ContainsKey(coin))
			{
				return fees[coin].transactionFee;
			}

		    throw new ArgumentException($"Fees collection doesn't contain fee for the {coin}. Count: {fees.Count}");
	    }

	    public async Task<decimal?> MinimalWithdraw(Coin coin)
	    {
			var fees = await Fees();
		    if (fees.ContainsKey(coin))
		    {
			    return fees[coin].minProductWithdraw;
		    }

		    throw new ArgumentException($"Fees collection doesn't contain fee for the {coin}. Count: {fees.Count}");
		}

		private async Task<Dictionary<Coin, CoinFee>> Fees()
	    {
		    if (_fees == null)
		    {
				await _semaphoreSlim.WaitAsync();

				try
				{
					if (_fees == null)
					{
						using (var response = await _httpClient.GetAsync(new Uri(_feeUrl)))
						{
							var json = await response.Content.ReadAsStringAsync();
							var fees = JsonConvert.DeserializeObject<List<CoinFee>>(json);

							Debug.Assert(fees.Count > 0);

							_fees = new Dictionary<Coin, CoinFee>();
							foreach (var coinFee in fees)
							{
								_fees.Add(new Coin(coinFee.assetCode), coinFee);
							}
						}
					}
				}
				finally
				{
					_semaphoreSlim.Release();
				}
		    }

		    return _fees;
	    }

	    private class CoinFee
	    {
		    public string assetCode;
		    public string assetName;
		    public decimal transactionFee;
		    public decimal minProductWithdraw;
	    }
    }
}
