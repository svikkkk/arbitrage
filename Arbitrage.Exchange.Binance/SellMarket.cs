﻿using Arbitrage.Models;
using Arbitrage.Models.Exchange.Concretes;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Orders;
using BinanceExchange.API.Client.Interfaces;
using BinanceExchange.API.Models.Response;
using System.Threading.Tasks;

namespace Arbitrage.Exchange.Binance
{
	internal class SellMarket : Market
	{
		private readonly IBinanceClient _client;
		private readonly decimal _tradingFeeRatio;
		private OrderBookResponse _orderBookResponse;

		public SellMarket(
			IBinanceClient client,
			ITradePair tradePair,
			decimal tradingFeeRatio,
			decimal aproximatePrice)
			: base(tradePair, new SellOrder(aproximatePrice, decimal.One, tradePair.CoinToSell, tradePair.CoinToBuy, tradingFeeRatio))
		{
			_client = client;
			_tradingFeeRatio = tradingFeeRatio;
		}

		protected override async Task OnInit()
		{
			_orderBookResponse = await _client.GetOrderBook(MarketSymbol(TradePair.CoinToBuy, TradePair.CoinToSell));
		}

		protected override Task<Order> OrderAt(int index)
		{
			if (index >= _orderBookResponse.Asks.Count)
			{
				RaiseNotEnoughOrdersException();
			}

			var originalOrder = _orderBookResponse.Asks[index];
			var order = new SellOrder(
				price: originalOrder.Price, 
				volume: originalOrder.Quantity, 
				sellCoin: TradePair.CoinToSell,
				buyCoin: TradePair.CoinToBuy, 
				tradeFeeFactor: _tradingFeeRatio);

			return Task.FromResult((Order)order);
		}

		private string MarketSymbol(Coin baseCoin, Coin quoteCoin)
		{
			return baseCoin.Code + quoteCoin.Code;
		}
	}
}
