﻿using Arbitrage.Models;
using Arbitrage.Models.Exchange;
using Arbitrage.Models.Exchange.Interfaces;
using BinanceExchange.API.Enums;
using BinanceExchange.API.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Arbitrage.Models.Exchange.Concretes;
using BinanceExchange.API.Client.Interfaces;

namespace Arbitrage.Exchange.Binance
{
	public class ExchangeClient : IExchangeClient
	{
		/// <summary>
		/// Trading fee in percentage. See https://www.binance.com/fees.html
		/// </summary>
		private const decimal TradeFee = 0.1m;

		private readonly IBinanceClient _client;
		private readonly SemaphoreSlim _coinsSemaphoreSlim = new SemaphoreSlim(1, 1);

		private readonly CoinsWithdrawFeeCollection _coinsWithdrawFeeCollection;

		public ExchangeClient(IBinanceClient binanceClient, CoinsWithdrawFeeCollection coinsWithdrawFeeCollection)
		{
			_client = binanceClient;
			_coinsWithdrawFeeCollection = coinsWithdrawFeeCollection;
		}

		public Task<decimal> Balance(Coin coin)
		{
			throw new NotImplementedException();
		}

		public async Task<IMarketsCollection> Markets()
		{
			var exchangeInfo = await _client.GetExchangeInfo();

			var list = await _client.GetSymbolOrderBookTicker();

			return new MarketsCollection(list.SelectMany(response => TwoMarketsFromTradePair(response, exchangeInfo)).ToList());
		}

		public Task SubmitWithdraw(Coin coin, decimal amount, string destinationAddress)
		{
			throw new NotImplementedException();
		}

		public Task<decimal> WithdrawFee(Coin coin, decimal amount)
		{
			return _coinsWithdrawFeeCollection.Fee(coin, amount);
		}

		public Task<decimal?> MinimalWithdraw(Coin coin)
		{
			return _coinsWithdrawFeeCollection.MinimalWithdraw(coin);
		}

		public Task<string> DepositDestinationAddress(Coin coin)
		{
			throw new NotImplementedException();
		}

		public void Dispose()
		{
			_coinsSemaphoreSlim?.Dispose();
		}

		private IEnumerable<IMarket> TwoMarketsFromTradePair(SymbolOrderBookResponse symbolOrderBookResponse, ExchangeInfoResponse exchangeInfo)
		{
			var symbol = exchangeInfo.Symbols.Single(s => s.Symbol == symbolOrderBookResponse.Symbol);

			var minimumTrade = ((ExchangeInfoSymbolFilterLotSize)symbol.Filters
				.Single(f => f.FilterType == ExchangeInfoSymbolFilterType.LotSize)).MinQty;

			var minimumBaseTrade = ((ExchangeInfoSymbolFilterMinNotional)symbol.Filters
				.Single(f => f.FilterType == ExchangeInfoSymbolFilterType.MinNotional)).MinNotional;

			var quoteCoin = new Coin(symbol.QuoteAsset);
			var baseCoin = new Coin(symbol.BaseAsset);

			return new IMarket[]
			{
				new BuyMarket(
					_client,

					new TradePair(
						coinToSell: baseCoin, 
						coinToBuy: quoteCoin, 
						minimumCoinToSellAmount: minimumTrade, 
						minimumCoinToBuyAmount: minimumBaseTrade), 

					tradingFeeRatio: 1m - TradeFee / 100m,
					aproximatePrice: symbolOrderBookResponse.BidPrice),

				new SellMarket(
					_client,

					new TradePair(
						coinToSell: quoteCoin, 
						coinToBuy: baseCoin, 
						minimumCoinToSellAmount: minimumBaseTrade, 
						minimumCoinToBuyAmount: minimumTrade), 

					tradingFeeRatio: 1m + TradeFee / 100m,
					aproximatePrice: symbolOrderBookResponse.AskPrice)
			};
		}

		private string MarketSymbol(Coin baseCoin, Coin quoteCoin)
		{
			return baseCoin.Code + quoteCoin.Code;
		}
	}
}
