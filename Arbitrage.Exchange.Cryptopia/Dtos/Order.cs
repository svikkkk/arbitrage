﻿namespace Arbitrage.Exchange.Cryptopia.Dtos
{
	internal class Order
	{
		/// <summary>
		/// e.g. 4945
		/// </summary>
		public int TradePairId { get; set; }

		/// <summary>
		/// e.g. "SAFEX/BTC"
		/// </summary>
		public string Label { get; set; }

		/// <summary>
		/// Price in quote currency (e.g. 0.00000201 BTC)
		/// </summary>
		public decimal Price { get; set; }
		
		/// <summary>
		/// Amount in base currency (e.g. 282.00000000 SAFEX)
		/// </summary>
		public decimal Volume { get; set; }

		/// <summary>
		/// Price * Volume (e.g. 0.00056682 BTC)
		/// </summary>
		public decimal Total { get; set; }
	}
}
