﻿using System.Collections.Generic;

namespace Arbitrage.Exchange.Cryptopia.Dtos
{
	internal class Balances : Base
	{
		public List<Balance> Data { get; set; }
	}
}
