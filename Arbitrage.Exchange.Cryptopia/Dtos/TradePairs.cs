﻿using System.Collections.Generic;

namespace Arbitrage.Exchange.Cryptopia.Dtos
{
	internal class TradePairs : Base
	{
		public List<TradePair> Data { get; set; }
	}
}
