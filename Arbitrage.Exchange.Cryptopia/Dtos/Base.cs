﻿namespace Arbitrage.Exchange.Cryptopia.Dtos
{
	internal class Base
	{
		public bool Success { get; set; }
		public string Message { get; set; }
		public string Error { get; set; }
	}
}
