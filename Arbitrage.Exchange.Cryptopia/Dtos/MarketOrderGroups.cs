﻿using System.Collections.Generic;

namespace Arbitrage.Exchange.Cryptopia.Dtos
{
	internal class MarketOrderGroups : Base
	{
		public List<MarketOrderGroup> Data { get; set; }
	}
}
