﻿using System.Collections.Generic;

namespace Arbitrage.Exchange.Cryptopia.Dtos
{
	internal class MarketOrdersCollection
	{
		public List<Order> Buy { get; set; }

		public List<Order> Sell { get; set; }
	}
}