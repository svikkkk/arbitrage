﻿using System.Collections.Generic;

namespace Arbitrage.Exchange.Cryptopia.Dtos
{
	internal class Markets : Base
	{
		public List<Market> Data { get; set; }
	}
}