﻿using System.Collections.Generic;

namespace Arbitrage.Exchange.Cryptopia.Dtos
{
	internal class MarketOrderGroup
	{
		public int TradePairId { get; set; }
		public string Market { get; set; }
		public List<Order> Buy { get; set; }
		public List<Order> Sell { get; set; }
	}
}
