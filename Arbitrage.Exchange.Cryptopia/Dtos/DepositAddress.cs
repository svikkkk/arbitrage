﻿namespace Arbitrage.Exchange.Cryptopia.Dtos
{
	internal class DepositAddress : Base
	{
		public DepositAddressData Data { get; set; }
	}
}
