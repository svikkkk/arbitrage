﻿namespace Arbitrage.Exchange.Cryptopia.Dtos
{
	internal class DepositAddressData
	{
		public string Currency { get; set; }
		public string Address { get; set; }
		public string BaseAddress { get; set; }
	}
}
