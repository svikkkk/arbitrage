﻿using System.Collections.Generic;

namespace Arbitrage.Exchange.Cryptopia.Dtos
{
	internal class Currencies : Base
	{
		public List<Currency> Data { get; set; }
	}
}
