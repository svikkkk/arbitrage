﻿using Arbitrage.Exchange.Cryptopia.Dtos;
using Arbitrage.Models.Exchange.Interfaces;
using System;
using System.Threading.Tasks;
using Arbitrage.Models.Orders;
using Order = Arbitrage.Models.Orders.Order;

namespace Arbitrage.Exchange.Cryptopia
{
	internal class BuyMarket : Models.Exchange.Concretes.Market
	{
		private readonly RestClient _restClient;
		private readonly decimal _tradingFeeRatio;

		private MarketOrdersCollection _orders;

		public BuyMarket(
			RestClient restClient,
			ITradePair tradePair,
			decimal tradingFeeRatio,
			decimal aproximatePrice)
			: base(tradePair, new BuyOrder(aproximatePrice, decimal.One, tradePair.CoinToSell, tradePair.CoinToBuy, tradingFeeRatio))
		{
			_restClient = restClient;
			_tradingFeeRatio = tradingFeeRatio;
		}

		protected override async Task OnInit()
		{
			var response = await _restClient.MarketOrders(TradePair.CoinToSell.Code, TradePair.CoinToBuy.Code);
			_orders = response.Data;

			if (_orders == null)
			{
				throw new Exception("Cryptopia Market Init error: " + response.Message);
			}
		}

		protected override Task<Order> OrderAt(int index)
		{
			if (index >= _orders.Buy.Count)
			{
				RaiseNotEnoughOrdersException();
			}

			var originalOrder = _orders.Buy[index];

			return Task.FromResult((Order)new BuyOrder(
				originalOrder.Price,
				originalOrder.Volume,
				TradePair.CoinToSell, 
				TradePair.CoinToBuy,
				_tradingFeeRatio));
		}
	}
}
