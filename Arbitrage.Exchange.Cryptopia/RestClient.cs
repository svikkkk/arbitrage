﻿using Arbitrage.Exchange.Cryptopia.Dtos;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Arbitrage.Exchange.Cryptopia
{
	internal class RestClient
	{
		private const string BaseUrl = "https://www.cryptopia.co.nz/api/";

		private readonly string _apiKey;
		private readonly string _apiSecret;

		private readonly RestSharp.RestClient _client;
		private readonly SemaphoreSlim _semaphoreSlim;

		public RestClient(string apiSecret, string apiKey, int concurrentRequestsLimit)
		{
			_apiSecret = apiSecret;
			_apiKey = apiKey;
			_semaphoreSlim = new SemaphoreSlim(concurrentRequestsLimit);
			_client = new RestSharp.RestClient(BaseUrl);
		}

		public Task<Balances> Balances()
		{
			var request = new RestRequest("GetBalance", Method.POST);

			request.AddJsonBody(new { });

			return Execute<Balances>(request, true);
		}

		public Task<Balances> Balances(string symbol)
		{
			var request = new RestRequest("GetBalance", Method.POST);

			request.AddJsonBody(new { Currency = symbol });

			return Execute<Balances>(request, true);
		}

		public Task<DepositAddress> DepositAddress(string symbol)
		{
			var request = new RestRequest("GetDepositAddress", Method.POST);

			request.AddJsonBody(new { Currency = symbol });

			return Execute<DepositAddress>(request, true);
		}

		public Task<Currencies> Currencies()
		{
			var request = new RestRequest("GetCurrencies");

			return Execute<Currencies>(request);
		}

		public Task<TradePairs> TradePairs()
		{
			var request = new RestRequest("GetTradePairs");

			return Execute<TradePairs>(request);
		}

		public Task<Markets> Markets()
		{
			var request = new RestRequest("GetMarkets");

			return Execute<Markets>(request);
		}

		public Task SubmitWithdraw(string symbol, decimal amount, string address)
		{
			var request = new RestRequest("SubmitWithdraw", Method.POST);

			request.AddJsonBody(new
			{
				Currency = symbol,
				Address = address,
				Amount = amount,
				PaymentId = Guid.NewGuid().ToString("N") // TODO Is it valid?
			});

			return Execute<TradePairs>(request, true);
		}

		/// <summary>
		/// Submits a new trade order
		/// </summary>
		/// <param name="tradePairId">The Cryptopia tradepair identifier of trade e.g. '100'</param>
		/// <param name="type">The type of trade e.g. 'Buy' or 'Sell'</param>
		/// <param name="rate">The rate or price to pay for the coins e.g. 0.00000034</param>
		/// <param name="amount">The amount of coins to buy e.g. 123.00000000</param>
		/// <returns></returns>
		public Task SubmitTrade(int tradePairId, TradeType type, decimal rate, decimal amount)
		{
			var request = new RestRequest("SubmitTrade", Method.POST);

			request.AddJsonBody(new
			{
				TradePairId = tradePairId,
				Type = type,
				Rate = rate,
				Amount = amount
			});

			return Execute<TradePairs>(request, true);
		}

		public enum TradeType
		{
			None,
			Buy,
			Sell
		}

		public Task<MarketOrders> MarketOrders(int tradePairId)
		{
			var request = new RestRequest($"GetMarketOrders/{tradePairId}");

			return Execute<MarketOrders>(request);
		}

		public Task<MarketOrders> MarketOrders(string baseCoinSymbol, string quoteCoinSymbol)
		{
			var request = new RestRequest($"GetMarketOrders/{baseCoinSymbol}_{quoteCoinSymbol}");

			return Execute<MarketOrders>(request);
		}

		public async Task<IEnumerable<MarketOrderGroup>> MarketOrderGroups(IEnumerable<int> tradePairIds)
		{
			const int partSize = 48;

			var result = new List<MarketOrderGroup>();

			var pairIds = tradePairIds as int[] ?? tradePairIds.ToArray();

			for (var i = 0; i < pairIds.Length; i += partSize)
			{
				var segmentSize = i + partSize <= pairIds.Length ?
					partSize :
					pairIds.Length - i;

				result.AddRange(await SmallMarketOrderGroups(new ArraySegment<int>(pairIds, i, segmentSize)));
			}

			return result;
		}

		private async Task<IEnumerable<MarketOrderGroup>> SmallMarketOrderGroups(IEnumerable<int> tradePairIds)
		{
			var resource = "GetMarketOrderGroups/";

			var pairIds = tradePairIds as int[] ?? tradePairIds.ToArray();

			var builder = new StringBuilder(resource, pairIds.Length * 5 + resource.Length);

			foreach (var tradePairId in pairIds)
			{
				builder.Append(tradePairId);
				builder.Append('-');
			}

			builder.Remove(builder.Length - 1, 1); // remove last dash

			resource = builder.ToString();

			var request = new RestRequest(resource);

			return (await Execute<MarketOrderGroups>(request)).Data;
		}

		private async Task<T> Execute<T>(IRestRequest request, bool authenticate = false) where T : new()
		{
			try
			{
				await _semaphoreSlim.WaitAsync();

				const int requestsInterval = 60;
				const int additionalDelay = 10;

				await Task.Delay(requestsInterval + additionalDelay);	// API rate limit: 1000 req/min

				if (authenticate)
				{
					_client.Authenticator = Authenticator(request);
				}

				var response = await _client.ExecuteTaskAsync<T>(request);

				if (response.ErrorException == null)
				{
					return response.Data;
				}

				const string message = "Error retrieving response. Check inner details for more info.";

				throw new ApplicationException(message, response.ErrorException);
			}
			finally
			{
				_semaphoreSlim.Release();
			}
		}

		private IAuthenticator Authenticator(IRestRequest request)
		{
			var nonce = Guid.NewGuid().ToString();
			var requestUrl = $"{BaseUrl}{request.Resource}";

			string requestSignatureBase64String;
			string signature;

			using (var md5 = MD5.Create())
			{
				var requestBody = request.Parameters.Single(p => p.Type == ParameterType.RequestBody).Value.ToString();
				requestSignatureBase64String = Convert.ToBase64String(md5.ComputeHash(Encoding.UTF8.GetBytes(requestBody)));
			}

			var requestContent = $"{_apiKey}POST{Uri.EscapeDataString(requestUrl).ToLower()}{nonce}{requestSignatureBase64String}";

			using (var hmacsha256 = new HMACSHA256(Convert.FromBase64String(_apiSecret)))
			{
				signature = Convert.ToBase64String(hmacsha256.ComputeHash(Encoding.UTF8.GetBytes(requestContent)));
			}

			return new OAuth2AuthorizationRequestHeaderAuthenticator($"{_apiKey}:{signature}:{nonce}", "amx");
		}
	}
}
