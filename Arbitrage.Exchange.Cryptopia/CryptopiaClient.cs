﻿using Arbitrage.Exchange.Cryptopia.Dtos;
using Arbitrage.Models;
using Arbitrage.Models.Exchange;
using Arbitrage.Models.Exchange.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Arbitrage.Exchange.Cryptopia
{
	public class CryptopiaClient : IExchangeClient
	{
		private readonly RestClient _restClient;
		private readonly SemaphoreSlim _coinsSemaphoreSlim = new SemaphoreSlim(1, 1);

		private CoinWithdrawFeeCollection _coinWithdrawFeeCollection;

		public CryptopiaClient(string apiKey, string apiSecret, int concurrentRequestsLimit)
		{
			_restClient = new RestClient(apiSecret, apiKey, concurrentRequestsLimit);
		}

		public async Task<decimal> Balance(Coin coin)
		{
			var balances = await _restClient.Balances(coin.Code);

			return balances.Data.Single().Available;
		}

		public Task<decimal?> MinimalWithdraw(Coin coin)
		{
			return Task.FromResult((decimal?)null);
		}

		public async Task<string> DepositDestinationAddress(Coin coin)
		{
			return (await _restClient.DepositAddress(coin.Code)).Data.Address;
		}

		public async Task<IMarketsCollection> Markets()
		{
			var currencies = (await _restClient.Currencies()).Data;
			var tradePairs = (await _restClient.TradePairs()).Data.Where(p => p.Status == "OK").ToArray();
			var rawMarkets = (await _restClient.Markets()).Data;

			_coinWithdrawFeeCollection = new CoinWithdrawFeeCollection(currencies);

			var markets = tradePairs
				.Join(rawMarkets,
					pair => pair.Id,
					market => market.TradePairId,
					TwoMarketsFromTradePair)
				.SelectMany(l => l);

			return new MarketsCollection(markets.ToList());
		}

		public Task SubmitWithdraw(Coin coin, decimal amount, string destinationAddress)
		{
			throw new NotImplementedException();
		}

		public Task<decimal> WithdrawFee(Coin coin, decimal amount)
		{
			return Task.FromResult(_coinWithdrawFeeCollection.Fee(coin, amount));
		}

		public void Dispose()
		{
			_coinsSemaphoreSlim?.Dispose();
		}

		private IEnumerable<IMarket> TwoMarketsFromTradePair(TradePair pair, Dtos.Market market)
		{
			try
			{
				if (market.AskPrice == 0 || market.BidPrice == 0)
				{
					return new IMarket[0];
				}

				var quoteCoin = new Coin(pair.BaseSymbol);
				var baseCoin = new Coin(pair.Symbol);

				return new IMarket[]
				{
					new BuyMarket(
						_restClient,

						new Models.Exchange.Concretes.TradePair(
							coinToSell: baseCoin,
							coinToBuy: quoteCoin,
							minimumCoinToSellAmount: pair.MinimumTrade,
							minimumCoinToBuyAmount: pair.MinimumBaseTrade),

						tradingFeeRatio: 1m - pair.TradeFee / 100m,
						aproximatePrice: market.BidPrice),

					new SellMarket(
						_restClient,

						new Models.Exchange.Concretes.TradePair(
							coinToSell: quoteCoin,
							coinToBuy: baseCoin,
							minimumCoinToSellAmount: pair.MinimumBaseTrade,
							minimumCoinToBuyAmount: pair.MinimumTrade),

						tradingFeeRatio: 1m + pair.TradeFee / 100m,
						aproximatePrice: market.AskPrice)
				};
			}
			catch (Exception)
			{
				return new IMarket[0];
			}
		}
	}
}
