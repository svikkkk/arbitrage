﻿using Arbitrage.Exchange.Cryptopia.Dtos;
using Arbitrage.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Arbitrage.Exchange.Cryptopia
{
	internal class CoinWithdrawFeeCollection
    {
		private readonly IEnumerable<Currency> _currencies;

		public CoinWithdrawFeeCollection(IEnumerable<Currency> currencies)
		{
			_currencies = currencies;
		}

		public decimal Fee(Coin coin, decimal amount)
		{
			var currency = _currencies.FirstOrDefault(c => c.Symbol == coin.Code);

			if (currency == null)
			{
				throw new InvalidOperationException($"Coin {coin} is not found in collection.");
			}

			return currency.WithdrawFee;
		}
	}
}
