﻿using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Orders;
using System;
using System.Threading.Tasks;
using Order = Arbitrage.Models.Orders.Order;

namespace Arbitrage.Exchange.Cryptopia
{
	internal class SellMarket : Models.Exchange.Concretes.Market
	{
		private readonly RestClient _restClient;
		private readonly decimal _tradingFeeRatio;
		private Dtos.MarketOrdersCollection _orders;

		public SellMarket(
			RestClient restClient,
			ITradePair tradePair,
			decimal tradingFeeRatio,
			decimal aproximatePrice)
			: base(tradePair, new SellOrder(aproximatePrice, decimal.One, tradePair.CoinToSell, tradePair.CoinToBuy, tradingFeeRatio))
		{
			_restClient = restClient;
			_tradingFeeRatio = tradingFeeRatio;
		}

		protected override async Task OnInit()
		{
			var response = await _restClient.MarketOrders(TradePair.CoinToBuy.Code, TradePair.CoinToSell.Code);
			_orders = response.Data;

			if (_orders == null)
			{
				throw new Exception("Cryptopia Market Init error: " + response.Message);
			}
		}

		protected override Task<Order> OrderAt(int index)
		{
			if (index >= _orders.Sell.Count)
			{
				RaiseNotEnoughOrdersException();
			}

			var originalOrder = _orders.Sell[index];

			return Task.FromResult((Order)new SellOrder(
				originalOrder.Price,
				originalOrder.Volume,
				TradePair.CoinToSell,
				TradePair.CoinToBuy, 
				_tradingFeeRatio));
		}
	}
}
