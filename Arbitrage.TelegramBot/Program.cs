﻿using Arbitrage.Business;
using Arbitrage.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace Arbitrage.TelegramBot
{
	internal class Program
	{
		private const decimal StartAmount = 0.1m;
		private const int Depth = 4;

		private static void Main()
		{
			var builder = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json");

			var configuration = builder.Build();

			var filter = new PlansAnalyzationFilter();

			var tradingPlansAnalyzation = new TradingPlansAnalyzation(
				filter,
				new SupportedExchangesCollection(configuration),
				Coin.Ethereum(),
				StartAmount,
				Depth);

			var tradingBot = new TelegramTradingBot(new BotClient(configuration["telegramBotToken"]), tradingPlansAnalyzation);
			tradingBot.Start();

			Console.ReadLine();

			tradingBot.Stop();
		}
	}
}
