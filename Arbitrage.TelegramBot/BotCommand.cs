﻿namespace Arbitrage.TelegramBot
{
	internal class BotCommand
    {
		public string Text { get; }

		public string Id { get; }

	    public bool IsReadonly { get; }

		public BotCommand(string text, string id, bool isReadOnly = false)
		{
			Text = text;
			Id = id;
			IsReadonly = isReadOnly;
		}
	}
}
