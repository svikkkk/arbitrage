﻿using System;

namespace Arbitrage.TelegramBot.Models
{
	public class Chat : IEquatable<Chat>
	{
		public long Id { get; set; }

		public bool IsDebugger { get; set; }

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((Chat) obj);
		}

		public bool Equals(Chat other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Id == other.Id;
		}

		public override int GetHashCode()
		{
			return Id.GetHashCode();
		}
	}
}