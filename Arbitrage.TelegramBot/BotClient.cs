﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Arbitrage.TelegramBot.Models;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace Arbitrage.TelegramBot
{
	internal class BotClient
	{
		private readonly TelegramBotClient _bot;

		public event Action<long, string> MessageReceived = delegate { };

		public event Action<long, string> CommandReceived = delegate { };

		public BotClient(string token)
		{
			_bot = new TelegramBotClient(token);
			_bot.OnMessage += OnMessageReceived;
			_bot.OnCallbackQuery += OnCallbackQueryReceived;
		}

		public void StartReceiving()
		{
			_bot.StartReceiving();
		}

		public void StopReceiving()
		{
			_bot.StopReceiving();
		}

		public async Task SendMessageToAll(string message, IList<BotCommand> commands)
		{
			using (var db = new LocalDbContext())
			{
				var blockedChats = new List<Chat>();

				foreach (var chat in db.Chats)
				{
					try
					{
						await SendCommands(chat.Id, message, commands);
					}
					catch (Telegram.Bot.Exceptions.ApiRequestException exception)
					{
						if (exception.ErrorCode == 403)
						{
							blockedChats.Add(chat);
						}
					}
					catch
					{
						// ignored
					}
				}

				db.Chats.RemoveRange(blockedChats);
				await db.SaveChangesAsync();
			}
		}

		public async Task SendText(long chatId, string text)
		{
			await _bot.SendTextMessageAsync(chatId, text, replyMarkup: new ReplyKeyboardRemove());
		}

		public async Task SendCommands(long chatId, string text, IEnumerable<BotCommand> commands)
		{
			var inlineKeyboard =
				new InlineKeyboardMarkup(commands.Select(c => new[] {InlineKeyboardButton.WithCallbackData(c.Text, c.Id)}));
			
			await _bot.SendTextMessageAsync(chatId, text, replyMarkup: inlineKeyboard);
		}

		private void OnMessageReceived(object sender, MessageEventArgs messageEventArgs)
		{
			var message = messageEventArgs.Message;

			if (message == null || message.Type != MessageType.Text)
			{
				return;
			}

			MessageReceived.Invoke(message.Chat.Id, message.Text);
		}

		private async void OnCallbackQueryReceived(object sender, CallbackQueryEventArgs callbackQueryEventArgs)
		{
			var callbackQuery = callbackQueryEventArgs.CallbackQuery;

			try
			{
				await _bot.AnswerCallbackQueryAsync(callbackQuery.Id);
			}
			catch (Telegram.Bot.Exceptions.ApiRequestException)
			{
				// ignored
			}

			CommandReceived.Invoke(callbackQuery.Message.Chat.Id, callbackQuery.Data);
		}
	}
}
