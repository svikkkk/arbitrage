﻿using Arbitrage.Business;
using Arbitrage.Models;
using Arbitrage.Planner.Plan;
using Arbitrage.TelegramBot.Commands;
using Arbitrage.TelegramBot.Messages;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Arbitrage.TelegramBot
{
	internal class TelegramTradingBot
    {
	    private const int MaxPlansCount = 50;
		private const decimal StartAmountMaxLimit = 0.5m;
	    private const decimal EndAmountMinLimit = 0.01m;

		private readonly BotClient _botClient;
	    private readonly TradingPlansAnalyzation _tradingPlansAnalyzation;
	    private readonly IList<TradingPlan> _plans = new List<TradingPlan>(MaxPlansCount);
	    
	    private int _currentIndex = 0;

		public TelegramTradingBot(BotClient botClient, TradingPlansAnalyzation tradingPlansAnalyzation)
	    {
		    _botClient = botClient;
		    _tradingPlansAnalyzation = tradingPlansAnalyzation;
	    }

		public Task Start()
	    {
			_plans.Clear();
		    _currentIndex = 0;

			UnsubscribeFromBotEvents();
		    UnsubscribeFromAnalyzationEvents();
			SubscribeOnBotEvents();
			SubscribeOnAnalyzationEvents();

			_botClient.StartReceiving();
		    return _tradingPlansAnalyzation.Start();
		}

	    public void Stop()
	    {
		    UnsubscribeFromBotEvents();
		    UnsubscribeFromAnalyzationEvents();
			_tradingPlansAnalyzation.Stop();

			_plans.Clear();
		    _currentIndex = 0;
		}

		private void SubscribeOnBotEvents()
	    {
			_botClient.CommandReceived += BotClientCommandReceived;
		    _botClient.MessageReceived += BotClientMessageReceived;
		}

	    private void UnsubscribeFromBotEvents()
	    {
		    _botClient.CommandReceived -= BotClientCommandReceived;
		    _botClient.MessageReceived -= BotClientMessageReceived;
	    }

		private void SubscribeOnAnalyzationEvents()
	    {
		    _tradingPlansAnalyzation.PlanFound += TradingPlansAnalyzation_PlanFound;
		    _tradingPlansAnalyzation.CalculationStarting += TradingPlansAnalyzation_CalculationStarting;
		    _tradingPlansAnalyzation.CalculationFinished += TradingPlansAnalyzation_CalculationFinished;
		}

	    private void UnsubscribeFromAnalyzationEvents()
	    {
		    _tradingPlansAnalyzation.PlanFound -= TradingPlansAnalyzation_PlanFound;
		    _tradingPlansAnalyzation.CalculationStarting -= TradingPlansAnalyzation_CalculationStarting;
		    _tradingPlansAnalyzation.CalculationFinished -= TradingPlansAnalyzation_CalculationFinished;
	    }

		private async void BotClientMessageReceived(long chatId, string message)
		{
			switch (message)
			{
				case "/start":
					await new AnalyzationListenersCommand(_botClient).Execute(chatId);
					break;
				case "/state":
					await new AnalyzationStateMessage(_botClient, _tradingPlansAnalyzation).Send(chatId);
					break;
				case "/plans":
					await new MostProfitablePlansMessage(_botClient, _plans).Send(chatId);
					break;
			}
		}

	    private async void BotClientCommandReceived(long chatId, string commandId)
	    {
		    try
		    {
			    var commandParams = commandId.Split(' ');
				var planId = int.Parse(commandParams[1]);
				switch (commandParams[0])
			    {
				    case "plan":

					    var plan = _plans.Single(p => p.Id == planId);
					    var updatedPlan = await plan.WithOptimalStartAmount(true);
					    await new TradingProfitRatio(updatedPlan).ToDecimal(updatedPlan.StartAmount);
					    AddPlanToCollection(updatedPlan);

						await new PlanStepsMessage(_botClient).Send(chatId, updatedPlan);
					    break;
				    case "step":
						var stepId = int.Parse(commandParams[2]);
						await new PlanOrdersMessage(_botClient, _plans).Send(chatId, planId, stepId);
					    break;
			    }
		    }
		    catch
		    {
			    // ignored
		    }
	    }

	    private async void TradingPlansAnalyzation_CalculationFinished(AnalyzationResult analyzationResult)
	    {
		    await new AnalyzationFinishedMessage(_botClient).Send(analyzationResult);
	    }

	    private async void TradingPlansAnalyzation_CalculationStarting(TradingPlansAnalyzation tradingPlansAnalyzation, int variants, int coins)
	    {
		    await new AnalyzationStartingMessage(_botClient).Send(_tradingPlansAnalyzation, variants, coins);
	    }

	    private async void TradingPlansAnalyzation_PlanFound(TradingPlan plan)
	    {
		    if (plan.StartAmount * (plan.ProfitRatio - 1m) >= EndAmountMinLimit)
		    {
			    AddPlanToCollection(plan);
				await new FoundPlanMessage(_botClient).Send(plan);
		    }
	    }

	    private void AddPlanToCollection(TradingPlan plan)
	    {
		    lock (_plans)
		    {
			    if (_plans.Count < MaxPlansCount)
			    {
				    _plans.Add(plan);
			    }
			    else
			    {
					_plans[_currentIndex] = plan;
				    _currentIndex++;
				    if (_currentIndex == _plans.Count)
				    {
					    _currentIndex = 0;
				    }
				}
			}
		}
    }
}
