﻿using System.Threading.Tasks;
using Arbitrage.TelegramBot.Models;

namespace Arbitrage.TelegramBot.Commands
{
    class AnalyzationListenersCommand
    {
	    private readonly BotClient _botClient;

	    public AnalyzationListenersCommand(BotClient botClient)
	    {
		    _botClient = botClient;
	    }

	    public async Task Execute(long chatId)
	    {
		    using (var db = new LocalDbContext())
		    {
			    var chat = db.Find<Chat>(chatId);

			    if (chat == null)
			    {
				    db.Chats.Add(new Chat
				    {
					    Id = chatId,
					    IsDebugger = false
				    });

				    await db.SaveChangesAsync();

				    await _botClient.SendText(chatId, "You are subscribed. You will see notifications soon");
			    }
		    }
	    }
	}
}
