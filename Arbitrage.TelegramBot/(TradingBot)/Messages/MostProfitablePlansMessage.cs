﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arbitrage.Planner.Plan;

namespace Arbitrage.TelegramBot.Messages
{
    class MostProfitablePlansMessage
	{
	    private readonly BotClient _botClient;
		private readonly IList<TradingPlan> _plans;

		public MostProfitablePlansMessage(BotClient botClient, IList<TradingPlan> plans)
		{
			_botClient = botClient;
			_plans = plans;
		}

		public Task Send(long chatId)
		{
			return SendMostProfitablePlans(chatId, _plans);
		}

		private Task SendMostProfitablePlans(long chatId, IList<TradingPlan> plans)
		{
			const int topLimit = 10;

			if (!plans.Any())
			{
				return _botClient.SendText(chatId, "Plans are not found");
			}

			return _botClient.SendCommands(
				chatId, 
				$"Top {topLimit} most profitable plans:",
				plans.OrderByDescending(p => p.ProfitRatio).Take(topLimit)
					.Select(p => new BotCommand(PlanToString(p), $"plan {p.Id}")));
		}

		private string PlanToString(TradingPlan plan)
		{
			var planSteps = new StringBuilder();

			planSteps.Append(plan.Steps[0].StartCoin.Code);

			foreach (var planAction in plan.Steps)
			{
				planSteps.Append("\u2192");
				planSteps.Append(planAction.EndCoin.Code);
			}

			return planSteps.ToString();
		}
	}
}
