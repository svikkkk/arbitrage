﻿using Arbitrage.Planner.Plan;
using Arbitrage.Planner.Plan.Steps;
using System.Text;
using System.Threading.Tasks;

namespace Arbitrage.TelegramBot.Messages
{
	class PlanOrdersString
	{
		private readonly TradingPlan _plan;
		private readonly int _stepId;

		public PlanOrdersString(TradingPlan plan, int stepId)
		{
			_plan = plan;
			_stepId = stepId;
		}

		public async Task<string> ToStringAsync()
		{
			var planOrders = new StringBuilder();

			if (_plan.Steps[_stepId] is TradeStep step)
			{
				var tradeStepWithActualOrders = step.WithOrderActualVolume();

				await tradeStepWithActualOrders.TestRun(step.StartAmount);

				planOrders.AppendLine($"{step.Exchange.Name} Total Orders: {tradeStepWithActualOrders.OrdersToTake.Count}");

				var i = 0;
				var totalSellCoin = 0m;
				var totalBuyCoin = 0m;
				foreach (var order in tradeStepWithActualOrders.OrdersToTake)
				{
					totalSellCoin += order.SellAmount;
					totalBuyCoin += order.BuyAmount;
					planOrders.AppendLine(i++ + ". " + order);
				}

				planOrders.AppendLine($"Total sell: {totalSellCoin:F5} {step.StartCoin}");
				planOrders.AppendLine($"Total buy: {totalBuyCoin:F5} {step.EndCoin}");
			}

			return planOrders.ToString();
		}
	}
}
