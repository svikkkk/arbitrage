﻿using System.Threading.Tasks;
using Arbitrage.Planner.Plan;

namespace Arbitrage.TelegramBot.Messages
{
	class FoundPlanMessage
	{
		private readonly BotClient _botClient;

		public FoundPlanMessage(BotClient botClient)
		{
			_botClient = botClient;
		}

		public Task Send(TradingPlan plan)
		{
			return _botClient.SendMessageToAll(
				new PlanTitle(plan).ToString(),
				new[] { new BotCommand(new PlanSteps(plan).ToString(), $"plan {plan.Id}") });
		}
	}
}
