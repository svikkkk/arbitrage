﻿using System.Linq;
using System.Threading.Tasks;
using Arbitrage.Planner.Plan;
using Arbitrage.Planner.Plan.Steps;

namespace Arbitrage.TelegramBot.Messages
{
    class PlanStepsMessage
    {
	    private readonly BotClient _botClient;

	    public PlanStepsMessage(BotClient botClient)
	    {
		    _botClient = botClient;
	    }

		public async Task Send(long chatId, TradingPlan plan)
		{
			var title = new PlanTitle(plan).ToString();
			var steps = new PlanSteps(plan).ToString();
			var commands = plan.Steps.Select((s, i) => new BotCommand(s.ToString(), $"step {plan.Id} {i}", s is TransferStep)).ToList();
			commands.Add(new BotCommand("Trade", $"trade {plan.Id}"));
			await _botClient.SendCommands(chatId, $"{title}\n{steps}", commands);
		}
    }
}
