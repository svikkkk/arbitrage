﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Arbitrage.Business;

namespace Arbitrage.TelegramBot.Messages
{
    class AnalyzationStartingMessage
    {
	    private readonly BotClient _botClient;

	    public AnalyzationStartingMessage(BotClient botClient)
	    {
		    _botClient = botClient;
	    }

		public async Task Send(TradingPlansAnalyzation tradingPlansAnalyzation, int variants, int coins)
	    {
		    var message =
			    $"Analysis started: {DateTime.Now}\n" +
			    $"StartCoin: {tradingPlansAnalyzation.Configuration.StartCoin}\n" +
			    $"Exchanges: {tradingPlansAnalyzation.Configuration.SupportedExchangesCollection.ToList().Count}\n" +
				$"Coins: {coins}\n" +
			    $"Depth: {tradingPlansAnalyzation.Configuration.AnalyzationDepth}\n" +
			    $"Variants: {variants}";

		    await _botClient.SendMessageToAll(message, new List<BotCommand>());
		}
    }
}
