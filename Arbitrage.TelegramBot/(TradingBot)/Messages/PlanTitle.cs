﻿using Arbitrage.Planner.Plan;

namespace Arbitrage.TelegramBot.Messages
{
    class PlanTitle
    {
	    private readonly TradingPlan _plan;

	    public PlanTitle(TradingPlan plan)
	    {
		    _plan = plan;
	    }

	    public override string ToString()
	    {
		    return $"{_plan.StartAmount:F5} \u2192 {PlanEndAmount():F5} {PlanProfitAmount():+0.#####;-#.#####}";
	    }

	    private decimal PlanEndAmount()
	    {
		    return _plan.StartAmount * _plan.ProfitRatio;
	    }

	    private decimal PlanProfitAmount()
	    {
		    return _plan.StartAmount * (_plan.ProfitRatio - 1m);
	    }
    }
}
