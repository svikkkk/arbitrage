﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Arbitrage.Planner.Plan;

namespace Arbitrage.TelegramBot.Messages
{
    class PlanOrdersMessage
    {
		private readonly BotClient _botClient;
	    private readonly IList<TradingPlan> _plans;

	    public PlanOrdersMessage(BotClient botClient, IList<TradingPlan> plans)
	    {
		    _botClient = botClient;
		    _plans = plans;
	    }

		public async Task Send(long chatId, int planId, int stepId)
		{
			var plan = _plans.Single(p => p.Id == planId);
			var message = await new PlanOrdersString(plan, stepId).ToStringAsync();
			if (!string.IsNullOrEmpty(message))
			{
				await _botClient.SendText(chatId, message);
			}
	    }
	}
}
