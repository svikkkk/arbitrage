﻿using System.Text;
using Arbitrage.Planner.Plan;
using Arbitrage.Planner.Plan.Steps;

namespace Arbitrage.TelegramBot.Messages
{
    class PlanSteps
    {
	    private readonly TradingPlan _plan;

	    public PlanSteps(TradingPlan plan)
	    {
		    _plan = plan;
	    }

		public override string ToString()
	    {
			var planSteps = new StringBuilder();

		    planSteps.Append(_plan.Steps[0].StartCoin.Code);

		    var stepCharacter = new StepCharacter();


			foreach (var planAction in _plan.Steps)
		    {
			    planSteps.Append(stepCharacter.ToString(planAction));
			    planSteps.Append(planAction.EndCoin.Code);
		    }

		    return planSteps.ToString();
		}
	}
}
