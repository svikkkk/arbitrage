﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Arbitrage.Models;

namespace Arbitrage.TelegramBot.Messages
{
    class AnalyzationFinishedMessage
	{
	    private readonly BotClient _botClient;

	    public AnalyzationFinishedMessage(BotClient botClient)
	    {
		    _botClient = botClient;
	    }

		public async Task Send(AnalyzationResult analyzationResult)
		{
			var message =
				$"Analysis finished: {DateTime.Now}\n" +
				$"Not Profitable after balancing: {analyzationResult.NotProfitableAfterAmountRecalculation}\n" +
				$"Skipped by filter: {analyzationResult.NotFilteredRecalculation}\n" +
				$"Exceptions during calculations: {analyzationResult.ExceptionDuringCalculationPlans}\n" +
				$"Profitable plans: {analyzationResult.ProfitablePlans} from {analyzationResult.PotentiallyProfitablePlans}";
				
		    await _botClient.SendMessageToAll(message, new List<BotCommand>());
		}
    }
}
