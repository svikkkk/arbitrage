﻿using System.Threading.Tasks;
using Arbitrage.Business;

namespace Arbitrage.TelegramBot.Messages
{
    class AnalyzationStateMessage
	{
	    private readonly BotClient _botClient;
		private readonly TradingPlansAnalyzation _tradingPlansAnalyzation;

		public AnalyzationStateMessage(BotClient botClient, TradingPlansAnalyzation tradingPlansAnalyzation)
		{
			_botClient = botClient;
			_tradingPlansAnalyzation = tradingPlansAnalyzation;
		}

		public async Task Send(long chatId)
	    {
		    await _botClient.SendText(chatId,
			    $"{_tradingPlansAnalyzation.State}. " +
			    $"Errors count: {_tradingPlansAnalyzation.Errors.Count}. " +
			    $"Start amount: {_tradingPlansAnalyzation.Configuration.StartAmount}. " +
			    $"Depth: {_tradingPlansAnalyzation.Configuration.AnalyzationDepth}");
		}
	}
}
