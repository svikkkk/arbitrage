﻿using Arbitrage.TelegramBot.Models;
using Microsoft.EntityFrameworkCore;

namespace Arbitrage.TelegramBot
{
	internal class LocalDbContext : DbContext
	{
		public DbSet<Chat> Chats { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlite("Data Source=local.db");
		}
	}
}
