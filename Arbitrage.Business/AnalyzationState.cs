﻿namespace Arbitrage.Business
{
    public enum AnalyzationState
    {
		None,
		Starting,
		PlansTreesBuilding,
		PotentialPlansAnalyzation,
		Finished
    }
}
