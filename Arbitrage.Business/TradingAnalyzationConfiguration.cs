﻿using Arbitrage.Models;

namespace Arbitrage.Business
{
    public class TradingAnalyzationConfiguration
    {
	    public SupportedExchangesCollection SupportedExchangesCollection { get; }
	    public Coin StartCoin { get; }
	    public decimal StartAmount { get; }
	    public int AnalyzationDepth { get; }

		public TradingAnalyzationConfiguration(
			SupportedExchangesCollection supportedExchangesCollection, 
			Coin startCoin, 
			decimal startAmount, 
			int analyzationDepth)
	    {
		    SupportedExchangesCollection = supportedExchangesCollection;
		    StartCoin = startCoin;
		    StartAmount = startAmount;
		    AnalyzationDepth = analyzationDepth;
	    }
	}
}
