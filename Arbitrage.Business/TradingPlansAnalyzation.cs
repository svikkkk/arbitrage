﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Arbitrage.Models;
using Arbitrage.Models.Exchange;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Planner.Graph;
using Arbitrage.Planner.Plan;
using Arbitrage.Planner.Tree;

namespace Arbitrage.Business
{
    public class TradingPlansAnalyzation : IDisposable
    {
		private string _prevTradingGraphHashCode = string.Empty;

	    private CancellationTokenSource _cancellationTokenSource;
	    private AnalyzationState _state;

	    public TradingAnalyzationConfiguration Configuration { get; }
	    public PlansAnalyzationFilter Filter { get; }

	    public AnalyzationState State
	    {
		    get => _state;
		    private set { _state = value; StateChanged.Invoke(); }
	    }

	    public event Action StateChanged = delegate { };
	    public event Action<TradingPlan> PlanFound = delegate { };
	    public event Action<TradingPlansAnalyzation, int, int> CalculationStarting = delegate { };
	    public event Action<AnalyzationResult> CalculationFinished = delegate { };

		public List<Exception> Errors { get; } = new List<Exception>();

		public IList<TradingTree> PlanTree { get; private set; }

		public DateTime PlansCalculated { get; private set; }

		public TradingPlansAnalyzation(
		    PlansAnalyzationFilter filter,
			SupportedExchangesCollection supportedExchangesCollection, 
			Coin startCoin, 
			decimal startAmount, 
			int analyzationDepth) : this(new TradingAnalyzationConfiguration(supportedExchangesCollection, startCoin, startAmount, analyzationDepth), filter)
	    {
		}

	    public TradingPlansAnalyzation(TradingAnalyzationConfiguration analyzationConfiguration, PlansAnalyzationFilter filter)
	    {
		    Configuration = analyzationConfiguration;
		    Filter = filter;
	    }

	    public void Clear()
	    {
		    PlanTree = null;
		    Errors.Clear();
		    PlansCalculated = DateTime.MinValue;
	    }

		public async Task Start(int delayBetweenCalculationsInSeconds = 15)
	    {
			try
			{
				Clear();
				State = AnalyzationState.Starting;
				_cancellationTokenSource = new CancellationTokenSource();

				var exchangeList = Configuration.SupportedExchangesCollection.ToList();
				var tradingGraph = await TradingGraph(exchangeList);

				do
				{
					try
					{
						await Analyze(exchangeList, tradingGraph);
						await Task.Delay(TimeSpan.FromSeconds(delayBetweenCalculationsInSeconds));
					}
					catch (Exception e)
					{
						Errors.Add(e);
					}
				} while (!_cancellationTokenSource.IsCancellationRequested);
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
				Errors.Add(e);
			}
		}

	    public void Stop()
	    {
		    State = AnalyzationState.None;
		    _cancellationTokenSource?.Cancel();
		}

	    public void Dispose()
	    {
			Configuration.SupportedExchangesCollection?.Dispose();
		}

	    private async Task Analyze(IList<IExchange> exchangeList, TradingGraph tradingGraph)
	    {
		    foreach (var exchange in exchangeList)
		    {
			    exchange.Clear();
		    }

		    var planTrees = await TradingTree(tradingGraph);
			if (planTrees != null && planTrees.Any())
		    {
			    State = AnalyzationState.PotentialPlansAnalyzation;

			    var planTasks = new List<Task<AnalyzationResult>>();

			    await OnCalculationStarting(planTrees);

				foreach (var planTree in planTrees)
			    {
				    SunscribeOnTradingTreeEvent(planTree);
					planTasks.Add(planTree.GoodPlansCalculation(Configuration.StartAmount, Filter));
				}

			    await Task.WhenAll(planTasks);
				OnCalculationFinished(planTasks);

			    PlansCalculated = DateTime.Now;
		    }

		    State = AnalyzationState.Finished;
		}

	    private void SunscribeOnTradingTreeEvent(TradingTree tradingTree)
	    {
		    tradingTree.PlanFound -= PlanTree_PlanFound;
		    tradingTree.PlanFound += PlanTree_PlanFound;
		}

	    private void OnCalculationFinished(List<Task<AnalyzationResult>> tasks)
	    {
			AnalyzationResult totalAnalyzationResult = new AnalyzationResult();

			foreach (var task in tasks)
		    {
			    totalAnalyzationResult.PotentiallyProfitablePlans += task.Result.PotentiallyProfitablePlans;
			    totalAnalyzationResult.ExceptionDuringCalculationPlans += task.Result.ExceptionDuringCalculationPlans;
			    totalAnalyzationResult.NotFilteredRecalculation += task.Result.NotFilteredRecalculation;
			    totalAnalyzationResult.NotProfitableAfterAmountRecalculation += task.Result.NotProfitableAfterAmountRecalculation;
			    totalAnalyzationResult.ProfitablePlans += task.Result.ProfitablePlans;
		    }

			CalculationFinished(totalAnalyzationResult);
	    }

	    private async Task OnCalculationStarting(IList<TradingTree> plansTrees)
	    {
		    var variants = 0;

		    foreach (var tradingTree in plansTrees)
		    {
			    variants += await tradingTree.Details().Variants();
		    }

		    var coins = await new ExchangeCoinsCollection(Configuration.SupportedExchangesCollection.ToList()).ToList();

			CalculationStarting(this, variants, coins.Count);
	    }

	    private void PlanTree_PlanFound(TradingPlan plan)
		{
			PlanFound.Invoke(plan);
		}

		private async Task<TradingGraph> TradingGraph(IList<IExchange> exchangeList)
	    {
			var exchnageCoins = await new ExchangeCoinsCollection(exchangeList).ToList();
		    var tradingSet = new TradingSet(exchnageCoins, exchangeList, Configuration.AnalyzationDepth);
		    var tradingStartPointsFilter = new TradingStartPoints(new List<Coin> { exchnageCoins.First(c => c.Code == Configuration.StartCoin.Code) });

		    return new TradingGraph(tradingSet, tradingStartPointsFilter, _cancellationTokenSource);
		}

	    private async Task<IList<TradingTree>> TradingTree(TradingGraph tradingGraph)
	    {
			var tradingGraphHashCode = await tradingGraph.ToHashCode(false);
		    if (_prevTradingGraphHashCode != tradingGraphHashCode)
		    {
			    State = AnalyzationState.PlansTreesBuilding;
			    PlanTree = await tradingGraph.ToTreeList();
			    _prevTradingGraphHashCode = tradingGraphHashCode;
		    }

		    return PlanTree;
	    }
	}
}
