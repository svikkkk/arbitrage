﻿using Arbitrage.Exchange.Cryptopia;
using Arbitrage.Exchange.EtherDelta;
using Arbitrage.Models.Exchange.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Numerics;
using Arbitrage.Exchange.Binance;
using Arbitrage.Exchange.EtherDelta.Entities;
using BinanceExchange.API.Client;

namespace Arbitrage.Business
{
	public class SupportedExchangesCollection : IDisposable
	{
		private readonly IConfigurationRoot _configurationRoot;
		private IList<IExchange> _exchanges;

		public SupportedExchangesCollection(IConfigurationRoot configurationRoot)
		{
			_configurationRoot = configurationRoot;
		}

		public IList<IExchange> ToList()
		{
			if (_exchanges == null)
			{
				_exchanges = new List<IExchange>();

				//AddEtherDelta();
				AddForkDelta();
				AddCryptopia();
				AddBinance();
			}

			return _exchanges;
		}

		private void AddCryptopia()
		{
			var cryptopia = new Models.Exchange.Concretes.Exchange("Cryptopia", new CryptopiaClient(
				_configurationRoot["cryptopia:apiKey"],
				_configurationRoot["cryptopia:apiSecret"],
				int.Parse(_configurationRoot["cryptopia:concurrentRequestsLimit"], CultureInfo.InvariantCulture)));

			_exchanges.Add(cryptopia);
		}

		private void AddBinance()
		{
			var configuration = new ClientConfiguration
			{
				ApiKey = _configurationRoot["binance:apiKey"],
				SecretKey = _configurationRoot["binance:apiSecret"],
				CacheTime = TimeSpan.FromSeconds(10),
				EnableRateLimiting = true
			};

			var coinsFeeCollection = new CoinsWithdrawFeeCollection(new HttpClient(), _configurationRoot["binance:feesUrl"]);
			var exchangeClient = new ExchangeClient(new BinanceClient(configuration), coinsFeeCollection);
			var binance = new Models.Exchange.Concretes.Exchange("Binance", exchangeClient);

			_exchanges.Add(binance);
		}

		private void AddEtherDelta()
		{
			var etherDelta = new Models.Exchange.Concretes.Exchange("EtherDelta", new EtherDeltaClient(new Configuration
			{
				SocketUrl = _configurationRoot["etherDelta:socketUrl"],
				GasPrice = BigInteger.Parse(_configurationRoot["etherDelta:gasPriceInWei"], CultureInfo.InvariantCulture),
				GasLimit = BigInteger.Parse(_configurationRoot["etherDelta:gasLimit"], CultureInfo.InvariantCulture),
				TradingFee = decimal.Parse(_configurationRoot["etherDelta:tradingFee"], CultureInfo.InvariantCulture),

				ConcurrentRequestsLimit = int.Parse(_configurationRoot["etherDelta:concurrentRequestsLimit"], CultureInfo.InvariantCulture),
				RequestTimeout = int.Parse(_configurationRoot["etherDelta:requestTimeout"], CultureInfo.InvariantCulture),
				RequestsPerMinute = int.Parse(_configurationRoot["etherDelta:requestsPerMinute"], CultureInfo.InvariantCulture),

				PathToConfig = _configurationRoot["etherDelta:config"],
				AddressEtherDelta = _configurationRoot["etherDelta:addressEtherDelta"],
				Provider = _configurationRoot["etherDelta:provider"]
			}));

			_exchanges.Add(etherDelta);
		}

		private void AddForkDelta()
		{
			var forkDelta = new Models.Exchange.Concretes.Exchange("ForkDelta", new EtherDeltaClient(new Configuration
			{
				SocketUrl = _configurationRoot["forkDelta:socketUrl"],
				GasPrice = BigInteger.Parse(_configurationRoot["forkDelta:gasPriceInWei"], CultureInfo.InvariantCulture),
				GasLimit = BigInteger.Parse(_configurationRoot["forkDelta:gasLimit"], CultureInfo.InvariantCulture),
				TradingFee = decimal.Parse(_configurationRoot["forkDelta:tradingFee"], CultureInfo.InvariantCulture),

				ConcurrentRequestsLimit = int.Parse(_configurationRoot["forkDelta:concurrentRequestsLimit"], CultureInfo.InvariantCulture),
				RequestTimeout = int.Parse(_configurationRoot["forkDelta:requestTimeout"], CultureInfo.InvariantCulture),
				RequestsPerMinute = int.Parse(_configurationRoot["forkDelta:requestsPerMinute"], CultureInfo.InvariantCulture),

				PathToConfig = _configurationRoot["forkDelta:config"],
				AddressEtherDelta = _configurationRoot["forkDelta:addressEtherDelta"],
				Provider = _configurationRoot["forkDelta:provider"]
			}));

			_exchanges.Add(forkDelta);
		}

		public void Dispose()
		{
			if (_exchanges != null)
			{
				foreach (var exchange in _exchanges)
				{
					(exchange as IDisposable)?.Dispose();
				}
			}
		}
	}
}
