﻿using Arbitrage.Models.Interfaces;
using Arbitrage.Models.Wallet;
using Arbitrage.Planner.Interfaces;
using System.Threading.Tasks;

namespace Arbitrage.Business
{
	public class SinglePlanStrategy : IStrategy
	{
		private readonly IPlanner _planner;
		private readonly IWallet _wallet;
		private readonly decimal _planCoinAmount;

		public SinglePlanStrategy(IPlanner planner, IWallet wallet, decimal planCoinAmount)
		{
			_planner = planner;
			_wallet = wallet;
			_planCoinAmount = planCoinAmount;
		}

		public bool IsRunning { get; private set; }

		public async Task Start()
		{
			IsRunning = true;
			var plans = await _planner.Plans(_wallet, _planCoinAmount, 0);
		}

		public Task Stop()
		{
			IsRunning = false;
			return Task.FromResult(true);
		}
	}
}
