﻿using Arbitrage.Planner.Interfaces;
using Arbitrage.Planner.Plan;

namespace Arbitrage.Business
{
    public class PlansAnalyzationFilter : IPlanFilter
	{
	    private const decimal StartAmountMaxLimit = 0.5m;
	    private const decimal EndAmountMinLimit = 0.01m;

		public bool Pass(TradingPlan plan)
		{
			if (plan.StartAmount * (plan.ProfitRatio - 1m) >= EndAmountMinLimit)
			{
				return true;
			}

			return false;
		}
	}
}
