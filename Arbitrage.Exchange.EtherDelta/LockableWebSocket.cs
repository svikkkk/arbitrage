﻿using WebSocketSharp;

namespace Arbitrage.Exchange.EtherDelta
{
	internal class LockableWebSocket : WebSocket
    {
	    public bool IsLocked { get; private set; }

	    public LockableWebSocket(string url, params string[] protocols) : base(url, protocols)
	    {
	    }

	    public void Lock()
	    {
		    IsLocked = true;
	    }

	    public void Unlock()
	    {
		    IsLocked = false;
	    }
	}
}
