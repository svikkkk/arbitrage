﻿using Arbitrage.Exchange.EtherDelta.Dtos;
using Arbitrage.Exchange.EtherDelta.Markets;
using Arbitrage.Models;
using Arbitrage.Models.Exchange;
using Arbitrage.Models.Exchange.Concretes;
using Arbitrage.Models.Exchange.Interfaces;
using Nethereum.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Arbitrage.Exchange.EtherDelta.Entities
{
	public class EtherDeltaMarketCollection : MarketsCollection
	{
		private readonly EtherDeltaCoinsCollection _etherDeltaCoinsCollection;
		private readonly Contract _etherDeltaContract;
		private readonly decimal _tradingFee;
		private readonly OrdersStorage _ordersStorage;

		internal EtherDeltaMarketCollection(OrdersStorage ordersStorage, EtherDeltaCoinsCollection etherDeltaCoinsCollection,
			Contract etherDeltaContract, decimal tradingFee)
			: this(ordersStorage, etherDeltaCoinsCollection, etherDeltaContract, tradingFee, new List<IMarket>())
		{
		}

		private EtherDeltaMarketCollection(OrdersStorage ordersStorage, EtherDeltaCoinsCollection etherDeltaCoinsCollection,
			Contract etherDeltaContract, decimal tradingFee, IList<IMarket> markets)
			: base(markets)
		{
			_etherDeltaCoinsCollection = etherDeltaCoinsCollection;
			_etherDeltaContract = etherDeltaContract;
			_tradingFee = tradingFee;
			_ordersStorage = ordersStorage;
		}

		internal async Task<IMarketsCollection> FromWebSocket(WebSocketClient webSocketClient)
		{
			var marketsDto = await webSocketClient.Market();

			var markets = marketsDto
				.ReturnTicker
				.Tickers
				.SelectMany(TwoMarketsFromTicker);

			markets = ExceptEthToEthMarket(markets);

			var marketsList = markets.ToList();

			Console.WriteLine("Got Markets " + marketsList.Count);

			return new EtherDeltaMarketCollection(_ordersStorage, _etherDeltaCoinsCollection, _etherDeltaContract, _tradingFee, marketsList);
		}

		public override IMarket Market(Coin coinToSell, Coin coinToBuy, bool withActualOrdersVolume)
		{
			var market = base.Market(coinToSell, coinToBuy, withActualOrdersVolume);

			if (withActualOrdersVolume)
			{
				if (market is EtherDeltaMarket etherDeltaMarket)
				{
					return etherDeltaMarket.WithActualVolume(_etherDeltaContract, _etherDeltaCoinsCollection);
				}
			}

			return market;
		}

		private IEnumerable<IMarket> TwoMarketsFromTicker(Ticker ticker)
		{
			if (ticker.Ask <= 0 || ticker.Bid <= 0 || !_etherDeltaCoinsCollection.Any(ticker.TokenAddress))
			{
				return new IMarket[0];
			}

			var baseCoin = _etherDeltaCoinsCollection.CoinByAddress(ticker.TokenAddress);
			var ether = _etherDeltaCoinsCollection.Etherium;

			// EtherDelta doesn't have minimum allowed order amount
			const decimal minimumTrade = 0;

			return new IMarket[]
			{
				new BuyMarket(
					ordersStorage: _ordersStorage,
					tradePair: new TradePair(
						coinToSell: baseCoin,
						coinToBuy: ether,
						minimumCoinToSellAmount: minimumTrade,
						minimumCoinToBuyAmount: minimumTrade),
					aproximatePrice: ticker.Bid,
					tradeFeeFactor: 1m + _tradingFee / 100m),

				new SellMarket(
					ordersStorage: _ordersStorage,
					tradePair: new TradePair(
						coinToSell: ether,
						coinToBuy: baseCoin,
						minimumCoinToSellAmount: minimumTrade,
						minimumCoinToBuyAmount: minimumTrade),
					aproximatePrice: ticker.Ask,
					tradeFeeFactor: 1m + _tradingFee / 100m)
			};
		}

		private IEnumerable<IMarket> ExceptEthToEthMarket(IEnumerable<IMarket> markets)
		{
			return markets.Where(m =>
				!m.TradePair.CoinToBuy.Equals(_etherDeltaCoinsCollection.Etherium) ||
				!m.TradePair.CoinToSell.Equals(_etherDeltaCoinsCollection.Etherium));
		}
	}
}
