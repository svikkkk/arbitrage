﻿using Arbitrage.Models;
using Nethereum.Util;
using System.Numerics;

namespace Arbitrage.Exchange.EtherDelta.Entities
{
	public class EtherDeltaCoin : Coin
	{
	    public string Address { get; }
	    public int UnitDecimals { get; }
		
		public EtherDeltaCoin(string addr, string code, int decimals) : base(code)
		{
			Address = addr;
			UnitDecimals = decimals;
		}

	    public decimal ToDecimal(BigInteger number)
	    {
            var uc = new UnitConversion();

		    return uc.FromWei(number, UnitDecimals);
	    }
    }
}
