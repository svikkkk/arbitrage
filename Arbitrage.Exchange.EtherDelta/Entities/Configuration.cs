﻿using System;
using System.Numerics;
using Arbitrage.Exchange.EtherDelta.Orders;

namespace Arbitrage.Exchange.EtherDelta.Entities
{
	public class Configuration
	{
		public string SocketUrl { get; set; }

		public BigInteger GasLimit { get; set; }

		public BigInteger GasPrice { get; set; }

		public decimal TradingFee { get; set; }

		public int ConcurrentRequestsLimit { get; set; }

		public int RequestTimeout { get; set; }

		public int RequestsPerMinute { get; set; }

		public string Provider { get; set; }

		public string AddressEtherDelta { get; set; }

		public string PathToConfig { get; set; }
	}
}
