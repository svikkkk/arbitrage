﻿using Arbitrage.Exchange.EtherDelta.Entities;
using Arbitrage.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Arbitrage.Exchange.EtherDelta
{
	internal class EtherDeltaCoinsCollection : CoinsCollection
	{
		private readonly IEnumerable<EtherDeltaCoin> _etherDeltaCoins;
		private EtherDeltaCoin _etherium;

		public EtherDeltaCoin Etherium =>
			_etherium ?? (_etherium = new EtherDeltaCoin("0x0000000000000000000000000000000000000000", "ETH", 18));

		public EtherDeltaCoinsCollection(IEnumerable<EtherDeltaCoin> etherDeltaCoins) : base(etherDeltaCoins)
		{
			_etherDeltaCoins = etherDeltaCoins;
		}

		public EtherDeltaCoin CoinByAddress(string address)
		{
			try
			{
				return _etherDeltaCoins.SingleOrDefault(c => c.Address.Equals(address, StringComparison.InvariantCultureIgnoreCase));
			}
			catch (InvalidOperationException exception)
			{
				Console.WriteLine($"EtherDeltaCoins error. Coin {address}: {exception.Message}");
				throw;
			}
		}

		public bool Any(string address)
		{
			return _etherDeltaCoins.Any(c => c.Address.Equals(address, StringComparison.InvariantCultureIgnoreCase));
		}
	}
}
