﻿using System;
using System.Globalization;
using Newtonsoft.Json;

namespace Arbitrage.Exchange.EtherDelta.JsonConverters
{
    public class JsonDecimalScientificConverter : JsonConverter
    {
        public override bool CanRead => true;

        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return decimal.Parse(reader.Value.ToString(), NumberStyles.Float, CultureInfo.InvariantCulture);
        }
    }
}
