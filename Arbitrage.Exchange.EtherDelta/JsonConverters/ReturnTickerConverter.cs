﻿using Arbitrage.Exchange.EtherDelta.Dtos;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Arbitrage.Exchange.EtherDelta.JsonConverters
{
	internal class ReturnTickerConverter : JsonConverter
	{
		public override bool CanRead => true;

		public override bool CanConvert(Type objectType)
		{
			return true;
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			serializer.Serialize(writer, value);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			var returnTicker = serializer.Deserialize<JObject>(reader);

			var tickets = new List<Ticker>();
			foreach (var jtoken in returnTicker.Children())
			{
				tickets.Add(Map(jtoken));
			}

			var result = new ReturnTicker
			{
				Tickers = tickets
			};

			return result;
		}

		private static Ticker Map(JToken token)
		{
			return token.First.ToObject<Ticker>();
		}
	}
}
