﻿using System;
using System.Globalization;
using Newtonsoft.Json;

namespace Arbitrage.Exchange.EtherDelta.JsonConverters
{
    public class TryParseDecimalConverter : JsonConverter
    {
        public override bool CanRead => true;

        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
	        if (decimal.TryParse((string)reader.Value, NumberStyles.Float, CultureInfo.InvariantCulture,
		        out decimal result))
	        {
		        return result;
	        }

	        return decimal.MinusOne;
        }
    }
}
