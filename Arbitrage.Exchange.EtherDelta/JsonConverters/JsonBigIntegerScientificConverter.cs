﻿using System;
using System.Globalization;
using System.Numerics;
using Newtonsoft.Json;

namespace Arbitrage.Exchange.EtherDelta.JsonConverters
{
	public class JsonBigIntegerScientificConverter : JsonConverter
    {
        public override bool CanRead => true;

        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return BigInteger.Parse(reader.Value.ToString(), NumberStyles.Float, CultureInfo.InvariantCulture);
        }
    }
}
