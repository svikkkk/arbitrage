﻿using Arbitrage.Exchange.EtherDelta.Dtos;
using Nethereum.Contracts;
using System.Numerics;

namespace Arbitrage.Exchange.EtherDelta.Orders
{
	internal class BuyOrdersAvailableVolume : OrderAvailableVolume
	{
		private readonly EtherDeltaCoinsCollection _etherDeltaCoinsCollection;

		internal BuyOrdersAvailableVolume(Order order, Contract etherDeltaContract, EtherDeltaCoinsCollection etherDeltaCoinsCollection)
			: base(order, etherDeltaContract)
		{
			_etherDeltaCoinsCollection = etherDeltaCoinsCollection;
		}

		protected override Order UpdateOrderWithNewVolume(Order order, BigInteger newVolume)
		{
			var updatedOrder = new Order
			{
				Price = order.Price,
				AmountGet = order.AmountGet,
				EthAvailableVolumeBase = order.EthAvailableVolumeBase,
				AmountGive = order.AmountGive,
				Id = order.Id,
				User = order.User,
				Expires = order.Expires,
				EthAvailableVolume = order.EthAvailableVolume,
				V = order.V,
				TokenGive = order.TokenGive,
				TokenGet = order.TokenGet,
				IsVolumeUpdated = order.IsVolumeUpdated,
				Deleted = order.Deleted,
				Nonce = order.Nonce,
				R = order.R,
				S = order.S,
				Updated = order.Updated
			};

			var newEthAvailableVolume = Divide(newVolume, UnitDecimals(order));

			updatedOrder.EthAvailableVolume = newEthAvailableVolume;
			updatedOrder.EthAvailableVolumeBase = updatedOrder.EthAvailableVolume * updatedOrder.Price;
			updatedOrder.IsVolumeUpdated = true;

			return updatedOrder;
		}

		private int UnitDecimals(Order order)
		{
			return _etherDeltaCoinsCollection.CoinByAddress(order.TokenGet).UnitDecimals;
		}
	}
}
