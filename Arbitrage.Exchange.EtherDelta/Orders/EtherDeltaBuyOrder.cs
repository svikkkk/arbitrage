﻿using System;
using Arbitrage.Models;
using Arbitrage.Models.Orders;

namespace Arbitrage.Exchange.EtherDelta.Orders
{
	public class EtherDeltaBuyOrder : Order
	{
		/// <summary>
		/// Creates order from buy order with price and volume
		/// </summary>
		/// <param name="price">Order price in quote currency (in Ethereum)</param>
		/// <param name="volume">Order volume in base currency (in some alt coin)</param>
		/// <param name="sellCoin">Base coin (some altcoin)</param>
		/// <param name="buyCoin">Quote coin. It must be Ethereum</param>
		/// <param name="tradeFeeFactor">Increasing factor of SellAmount (e.g. 1.003 for 0.3% trade fee)</param>
		public EtherDeltaBuyOrder(decimal price, decimal volume, Coin sellCoin, Coin buyCoin, decimal tradeFeeFactor)
			: base(sellAmount: volume * tradeFeeFactor,
				buyAmount: volume * price,
				sellCoin: sellCoin,
				buyCoin: buyCoin)
		{
			if (sellCoin == Coin.Ethereum())
			{
				throw new ArgumentException("sellCoin must not be Ethereum", nameof(sellCoin));
			}

			if (buyCoin != Coin.Ethereum())
			{
				throw new ArgumentException("buyCoin must be Ethereum", nameof(buyCoin));
			}

			var noFeeSellAmount = SellAmount / tradeFeeFactor;
			var tradingFee = SellAmount - noFeeSellAmount;

			AdditionalString = $"\nFee: {tradingFee:F5} {Sell.Code}\nPrice: {price:F5} {Buy.Code}";
		}
	}
}