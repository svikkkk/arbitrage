﻿using Arbitrage.Exchange.EtherDelta.Dtos;
using Nethereum.Contracts;
using Nethereum.Hex.HexConvertors.Extensions;
using System.Globalization;
using System.Numerics;
using System.Threading.Tasks;

namespace Arbitrage.Exchange.EtherDelta.Orders
{
	internal abstract class OrderAvailableVolume
	{
		private readonly Order _order;
		private readonly Contract _etherDeltaContract;

		internal OrderAvailableVolume(Order order, Contract etherDeltaContract)
		{
			_order = order;
			_etherDeltaContract = etherDeltaContract;
		}

		internal async Task<Order> ToOrder()
		{
			if (_order.IsVolumeUpdated)
			{
				return _order;
			}

			var functionInput = new object[] {
				_order.TokenGet,
				_order.AmountGet,
				_order.TokenGive,
				_order.AmountGive,
				_order.Expires,
				_order.Nonce,
				_order.User,
				_order.V.Value,
				_order.R.HexToByteArray(),
				_order.S.HexToByteArray()
			};

			var fnTest = _etherDeltaContract.GetFunction("availableVolume");
			var volume = await fnTest.CallAsync<BigInteger>(functionInput);

			return UpdateOrderWithNewVolume(_order, volume);
		}

		protected abstract Order UpdateOrderWithNewVolume(Order order, BigInteger newVolume);

		protected decimal Divide(BigInteger volume, int unitDecimals)
		{
			return decimal.Parse($"{volume}E-{unitDecimals}", NumberStyles.Float, CultureInfo.InvariantCulture);
		}
	}
}
