﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Arbitrage.Exchange.EtherDelta
{
	internal class Message
	{
		internal Message()
		{
			Data = new JObject();
			Event = "";
		}

		public string Event { get; set; }

		public JObject Data { get; set; }

		public override string ToString()
		{
			var ret = $"42[\"{Event}\", {JsonConvert.SerializeObject(Data)}]";
			return ret;
		}

		internal static Message ParseMessage(string messageString)
		{
			var message = new Message();

			if (!messageString.StartsWith("42"))
			{
				return message;
			}

			messageString = messageString.Remove(0, 2);

			var tmpData = JsonConvert.DeserializeObject(messageString);

			if (tmpData == null || tmpData.GetType() != typeof(JArray))
			{
				return message;
			}

			var array = (JArray)tmpData;

			if (array.Count > 0 && array[0].GetType() == typeof(JValue))
			{
				message.Event = array[0].ToString();
			}

			if (array.Count > 1 && array[1].GetType() == typeof(JObject))
			{
				message.Data = (JObject)array[1];
			}

			return message;
		}
	}
}
