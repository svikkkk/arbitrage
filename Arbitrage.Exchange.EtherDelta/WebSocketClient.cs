﻿using Arbitrage.Exchange.EtherDelta.Dtos;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Arbitrage.Exchange.EtherDelta
{
	internal class WebSocketClient
	{
		private readonly int _requestsPerMinute;
		private readonly TimeSpan _timeOut;
		private readonly Dictionary<string, TaskCompletionSource<Market>> _marketTaskCompletionSources;
		private readonly SemaphoreSlim _connectionsLimitSemaphore;
		private readonly SemaphoreSlim _rateLimitSemaphore;
		private readonly SocketPool _socketPool;

		public event Action<Dtos.Orders> ReceivedOrders = delegate { };

		public WebSocketClient(string socketUrl, int concurrentConnections, TimeSpan timeOut, int requestsPerMinute)
		{
			if (requestsPerMinute <= 0)
			{
				throw new ArgumentException("requestsPerMinute must be positive", nameof(requestsPerMinute));
			}

			_requestsPerMinute = requestsPerMinute;

			_timeOut = timeOut;

			_connectionsLimitSemaphore = new SemaphoreSlim(concurrentConnections);

			_rateLimitSemaphore = new SemaphoreSlim(1);

			_marketTaskCompletionSources = new Dictionary<string, TaskCompletionSource<Market>>();

			_socketPool = new SocketPool(socketUrl, concurrentConnections);
			_socketPool.BroadcastMessageReceived += OnBroadcastMessageReceived;
			_socketPool.MessageReceived += OnMessageReceived;
		}

		public async Task<Dtos.Market> Market(string tokenAddress = "")
		{
			tokenAddress = tokenAddress.ToLowerInvariant();

			TaskCompletionSource<Market> tokenTaskCompletion = null;

			lock (_marketTaskCompletionSources)
			{
				if (_marketTaskCompletionSources.ContainsKey(tokenAddress))
				{
					tokenTaskCompletion = _marketTaskCompletionSources[tokenAddress];
				}
				else
				{
					_marketTaskCompletionSources.Add(tokenAddress, new TaskCompletionSource<Market>());
				}
			}

			if (tokenTaskCompletion != null)
			{
				return await tokenTaskCompletion.Task;
			}

			var socketId = -1;

			try
			{
				await _connectionsLimitSemaphore.WaitAsync();

				await RateLimitDelay();

				using (var ct = new CancellationTokenSource(_timeOut))
				{
					ct.Token.Register(() => _marketTaskCompletionSources[tokenAddress].TrySetCanceled(), false);

					socketId = _socketPool.SendAndGetSocketId(
						new Message
						{
							Event = "getMarket",
							Data = new JObject { ["token"] = tokenAddress }
						}.ToString());

					return await _marketTaskCompletionSources[tokenAddress].Task;
				}
			}
			catch (TaskCanceledException exception)
			{
				Console.WriteLine($"EtherDelta TIMEOUT      {tokenAddress} {DateTime.Now:T} TIMEOUT");
				throw new TimeoutException("", exception);
			}
			finally
			{
				if (socketId != -1)
				{
					_socketPool.UnlockSocket(socketId);
				}

				_marketTaskCompletionSources.Remove(tokenAddress);
				_connectionsLimitSemaphore.Release();
			}
		}

		private async Task RateLimitDelay()
		{
			await _rateLimitSemaphore.WaitAsync();

			await Task.Delay(60000 / _requestsPerMinute);

			_rateLimitSemaphore.Release();
		}

		private void OnMessageReceived(string data)
		{
			var message = Message.ParseMessage(data);

			if (message.Event.ToLowerInvariant() == "market")
			{
				HandleMarketMessage(message.Data);
			}
		}

		private void OnBroadcastMessageReceived(string data)
		{
			var message = Message.ParseMessage(data);

			if (message.Event.ToLowerInvariant() == "orders")
			{
				var orders = message.Data.ToObject<Dtos.Orders>();

				ReceivedOrders.Invoke(orders);
			}
		}

		private void HandleMarketMessage(JToken data)
		{
			var market = data.ToObject<Market>();

			var token = market.Orders?.Buys.FirstOrDefault()?.TokenGet ??
						market.Orders?.Sells.FirstOrDefault()?.TokenGive ??
						string.Empty;

			token = token.ToLowerInvariant();

			if (_marketTaskCompletionSources.ContainsKey(token))
			{
				_marketTaskCompletionSources[token].TrySetResult(market);
			}
		}
	}
}
