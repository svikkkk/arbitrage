﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Arbitrage.Exchange.EtherDelta
{
	internal class OrdersStorage
	{
		private readonly WebSocketClient _webSocketClient;
		private readonly Dictionary<string, Dtos.Orders> _orders;

		public OrdersStorage(WebSocketClient webSocketClient)
		{
			_orders = new Dictionary<string, Dtos.Orders>();

			_webSocketClient = webSocketClient;
			_webSocketClient.ReceivedOrders += OnReceivedOrders;
		}

		public async Task<Dtos.Orders> Orders(string tokenAddress)
		{
			Dtos.Orders orders = null;

			lock (_orders)
			{
				if (_orders.ContainsKey(tokenAddress))
				{
					orders = _orders[tokenAddress];
				}
			}

			if (orders != null)
			{
				Console.WriteLine($"Got {tokenAddress} orders from OrdersStorage. Tokens count: {_orders.Count}");
				return orders;
			}

			orders = (await _webSocketClient.Market(tokenAddress)).Orders;

			lock (_orders)
			{
				if (!_orders.ContainsKey(tokenAddress))
				{
					_orders.Add(tokenAddress, orders);
				}
			}

			return orders;
		}

		private void OnReceivedOrders(Dtos.Orders orders)
		{
			UpdateBuyOrders(orders);
			UpdateSellOrders(orders);
		}

		private void UpdateBuyOrders(Dtos.Orders orders)
		{
			foreach (var buyOrder in orders.Buys)
			{
				var tokenAddress = buyOrder.TokenGet;

				if (!_orders.ContainsKey(tokenAddress))
				{
					continue;
				}

				var storedOrders = _orders[tokenAddress];
				
				storedOrders.Buys.Remove(buyOrder);
				
				if (!buyOrder.Deleted ?? true)
				{
					storedOrders.Buys.Add(buyOrder);

					storedOrders.Buys.Sort();

					storedOrders.Buys.Reverse();
				}
			}
		}

		private void UpdateSellOrders(Dtos.Orders orders)
		{
			foreach (var sellOrder in orders.Sells)
			{
				var tokenAddress = sellOrder.TokenGive;

				if (!_orders.ContainsKey(tokenAddress))
				{
					continue;
				}

				var storedOrders = _orders[tokenAddress];

				storedOrders.Sells.Remove(sellOrder);

				if (!sellOrder.Deleted ?? true)
				{
					storedOrders.Sells.Add(sellOrder);

					storedOrders.Sells.Sort();
				}
			}
		}
	}
}
