﻿using Arbitrage.Exchange.EtherDelta.Dtos;
using Arbitrage.Exchange.EtherDelta.Entities;
using Arbitrage.Models;
using Arbitrage.Models.Exchange;
using Arbitrage.Models.Exchange.Interfaces;
using Nethereum.Contracts;
using Nethereum.Util;
using Nethereum.Web3;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Arbitrage.Exchange.EtherDelta
{
	public class EtherDeltaClient : IExchangeClient, IClearable
	{
		private const string ContractPath = "./contracts/etherdelta.json";

		private readonly Configuration _configuration;
		private readonly EtherDeltaCoinsCollection _etherDeltaCoinsCollection;
		private readonly SemaphoreSlim _marketsCollectionSemaphore;
		private readonly Contract _etherDeltaContract;
		private readonly WebSocketClient _webSocketClient;
		private readonly OrdersStorage _ordersStorage;

		private IMarketsCollection _marketsCollection;

		/// <summary>
		/// Transaction fee in ETH
		/// </summary>
		public decimal TransactionFee
		{
			get
			{
				var uc = new UnitConversion();

				return uc.FromWei(_configuration.GasLimit * _configuration.GasPrice);
			}
		}

		public EtherDeltaClient(Configuration configuration)
		{
			_configuration = configuration;
			_etherDeltaCoinsCollection = new EtherDeltaCoinsCollection(CoinsCollection(configuration.PathToConfig));
			_marketsCollectionSemaphore = new SemaphoreSlim(1);

			_webSocketClient = new WebSocketClient(configuration.SocketUrl, configuration.ConcurrentRequestsLimit,
				TimeSpan.FromMilliseconds(configuration.RequestTimeout), configuration.RequestsPerMinute);

			_ordersStorage = new OrdersStorage(_webSocketClient);

			var web3 = new Web3(configuration.Provider);
			var abi = File.ReadAllText(ContractPath);
			var addressEtherDelta = Web3.ToChecksumAddress(configuration.AddressEtherDelta);
			_etherDeltaContract = web3.Eth.GetContract(abi, addressEtherDelta);
		}

		public void Clear()
		{
			_marketsCollection = null;
		}

		public void Dispose()
		{
			Clear();
		}

		public Task<decimal> Balance(Coin coin)
		{
			throw new NotImplementedException();
		}

		public Task<string> DepositDestinationAddress(Coin coin)
		{
			throw new NotImplementedException();
		}

		public async Task<IMarketsCollection> Markets()
		{
			if (_marketsCollection == null)
			{
				try
				{
					await _marketsCollectionSemaphore.WaitAsync();

					if (_marketsCollection == null)
					{
						_marketsCollection = await new EtherDeltaMarketCollection(
							_ordersStorage,
							_etherDeltaCoinsCollection, 
							_etherDeltaContract,
							_configuration.TradingFee)
							.FromWebSocket(_webSocketClient);
					}
				}
				finally
				{
					_marketsCollectionSemaphore.Release();
				}
			}

			return _marketsCollection;
		}

		public Task SubmitWithdraw(Coin coin, decimal amount, string destinationAddress)
		{
			throw new NotImplementedException();
		}

		public Task<decimal?> MinimalWithdraw(Coin coin)
		{
			return Task.FromResult((decimal?)null);
		}

		public async Task<decimal> WithdrawFee(Coin coin, decimal amount)
		{
			if (coin.Equals(_etherDeltaCoinsCollection.Etherium))
			{
				return TransactionFee;
			}

			var marketsCollection = await Markets();
			var transactionFeeInAltcoin = await marketsCollection.Market(_etherDeltaCoinsCollection.Etherium, coin, false)
				.Rate(MarketRate.Approximate)
				.AvailableAmountToBuy(TransactionFee);

			return transactionFeeInAltcoin;
		}

		private IEnumerable<EtherDeltaCoin> CoinsCollection(string pathToConfig)
		{
			var json = File.ReadAllText(pathToConfig);
			var config = JsonConvert.DeserializeObject<MainConfig>(json);

			return config.Tokens.Select(t => new EtherDeltaCoin(t.Address, t.Name, t.Decimals));
		}
	}
}