﻿using Arbitrage.Exchange.EtherDelta.JsonConverters;
using Newtonsoft.Json;

namespace Arbitrage.Exchange.EtherDelta.Dtos
{
	internal class Market
    {
	    public Orders Orders { get; set; }

	    [JsonConverter(typeof(ReturnTickerConverter))]
		public ReturnTicker ReturnTicker { get; set; }
	}
}
