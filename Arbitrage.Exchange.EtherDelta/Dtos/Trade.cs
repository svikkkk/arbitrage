﻿using System;
using Newtonsoft.Json.Linq;

namespace Arbitrage.Exchange.EtherDelta.Dtos
{
	internal class Trade
	{
		public string TxHash { get; set; }
		public decimal Price { get; set; }
		public DateTime Date { get; set; }
		public decimal Amount { get; set; }
		public decimal AmountBase { get; set; }
		public string Side { get; set; }
		public string Buyer { get; set; }
		public string Seller { get; set; }
		public string TokenAddr { get; set; }
	}
}
