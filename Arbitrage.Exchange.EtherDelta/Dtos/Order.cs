﻿using Arbitrage.Exchange.EtherDelta.JsonConverters;
using Newtonsoft.Json;
using System;
using System.Numerics;

namespace Arbitrage.Exchange.EtherDelta.Dtos
{
	internal class Order : IEquatable<Order>, IComparable<Order>
	{
		private const decimal MinOrderSize = 0.001m;

		public string Id { get; set; }

		public string TokenGet { get; set; }

		public string TokenGive { get; set; }

		public string User { get; set; }

		[JsonConverter(typeof(TryParseDecimalConverter))]
		public decimal Price { get; set; }

		[JsonConverter(typeof(TryParseDecimalConverter))]
		public decimal EthAvailableVolume { get; set; }

		[JsonConverter(typeof(TryParseDecimalConverter))]
		public decimal EthAvailableVolumeBase { get; set; }

		[JsonConverter(typeof(JsonBigIntegerScientificConverter))]
		public BigInteger AmountGet { get; set; }

		[JsonConverter(typeof(JsonBigIntegerScientificConverter))]
		public BigInteger AmountGive { get; set; }

		[JsonConverter(typeof(JsonBigIntegerScientificConverter))]
		public BigInteger Expires { get; set; }

		[JsonConverter(typeof(JsonBigIntegerScientificConverter))]
		public BigInteger Nonce { get; set; }

		public DateTimeOffset Updated { get; set; }

		public bool? Deleted { get; set; }

		public int? V { get; set; }

		public string R { get; set; }

		public string S { get; set; }

		public bool IsVolumeUpdated { get; set; }

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((Order) obj);
		}

		public bool Equals(Order other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return string.Equals(Id, other.Id, StringComparison.InvariantCultureIgnoreCase);
		}

		public override int GetHashCode()
		{
			return Id != null ? StringComparer.InvariantCultureIgnoreCase.GetHashCode(Id) : 0;
		}

		public bool IsValid => 
			Expires.ToByteArray().Length <= 32 && 
			EthAvailableVolumeBase > MinOrderSize &&
		    EthAvailableVolume > 0 && 
			Price > 0 &&
			V.HasValue;

		public int CompareTo(Order other)
		{
			if (ReferenceEquals(this, other)) return 0;
			if (ReferenceEquals(null, other)) return 1;
			return Price.CompareTo(other.Price);
		}
	}
}
