﻿using Arbitrage.Exchange.EtherDelta.JsonConverters;
using Newtonsoft.Json;

namespace Arbitrage.Exchange.EtherDelta.Dtos
{
	internal class Ticker
	{
		[JsonConverter(typeof(TryParseDecimalConverter))]
		public decimal Ask { get; set; }

		[JsonConverter(typeof(TryParseDecimalConverter))]
		public decimal Bid { get; set; }

		[JsonProperty("tokenAddr")]
		public string TokenAddress { get; set; }
	}
}
