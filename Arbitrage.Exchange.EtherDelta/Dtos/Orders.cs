﻿using System.Collections.Generic;

namespace Arbitrage.Exchange.EtherDelta.Dtos
{
	internal class Orders
	{
		public List<Order> Sells { get; set; }

		public List<Order> Buys { get; set; }
	}
}
