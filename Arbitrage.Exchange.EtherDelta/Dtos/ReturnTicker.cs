﻿using System.Collections.Generic;

namespace Arbitrage.Exchange.EtherDelta.Dtos
{
	internal class ReturnTicker
    {
		public IEnumerable<Ticker> Tickers { get; set; }
	}
}
