﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Arbitrage.Exchange.EtherDelta.Dtos
{
	internal class MainConfig
    {
		[JsonProperty("tokens")]
	    public Token[] Tokens { get; set; }
    }

	internal class Token
	{
		[JsonProperty("addr")]
		public string Address { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("decimals")]
		public int Decimals { get; set; }
	}
}
