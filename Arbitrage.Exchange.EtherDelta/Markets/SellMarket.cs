﻿using Arbitrage.Exchange.EtherDelta.Entities;
using Arbitrage.Exchange.EtherDelta.Orders;
using Arbitrage.Models;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Orders;
using Nethereum.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Arbitrage.Exchange.EtherDelta.Markets
{
	internal class SellMarket : EtherDeltaMarket
	{
		private readonly decimal _aproximatePrice;
		private readonly decimal _tradeFeeFactor;

		private ActualVolumeSellMarket _actualVolumeSellMarket;

		public SellMarket(
			OrdersStorage ordersStorage,
			ITradePair tradePair,
			decimal aproximatePrice,
			decimal tradeFeeFactor) 
			: base(ordersStorage, tradePair, new EtherDeltaSellOrder(aproximatePrice, decimal.One, tradePair.CoinToSell, tradePair.CoinToBuy, tradeFeeFactor))
		{
			_aproximatePrice = aproximatePrice;
			_tradeFeeFactor = tradeFeeFactor;
		}

		public override IMarket WithActualVolume(Contract etherDeltaContract, EtherDeltaCoinsCollection etherDeltaCoinsCollection)
		{
			return _actualVolumeSellMarket ?? (_actualVolumeSellMarket = new ActualVolumeSellMarket(
				       OrdersStorage,
				       etherDeltaContract,
				       etherDeltaCoinsCollection,
				       TradePair,
				       _aproximatePrice,
				       _tradeFeeFactor));
		}

		protected override string MarketAddress(Coin coinToSell, Coin coinToBuy)
		{
			return ((EtherDeltaCoin)coinToBuy).Address;
		}

		protected override Order MapOrder(Dtos.Order etherDeltaOrder)
		{
			return new EtherDeltaSellOrder(
				price: etherDeltaOrder.Price, 
				volume: etherDeltaOrder.EthAvailableVolume,
				sellCoin: TradePair.CoinToSell,
				buyCoin: TradePair.CoinToBuy,
				tradeFeeFactor: _tradeFeeFactor);
		}

		protected override IEnumerable<Dtos.Order> FilteredOrders(Dtos.Orders remoteOrders)
		{
			return remoteOrders.Sells.Where(o => o.IsValid);
		}

		protected override void ValidateCoinsPair(Coin coinToSell, Coin coinToBuy)
		{
			if (coinToSell != Coin.Ethereum())
			{
				throw new ArgumentException("coinToSell must be Ethereum", nameof(coinToSell));
			}
		}
	}
}
