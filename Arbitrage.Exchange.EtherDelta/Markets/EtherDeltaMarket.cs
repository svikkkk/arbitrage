﻿using Arbitrage.Models;
using Arbitrage.Models.Exchange.Concretes;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Orders;
using Nethereum.Contracts;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Arbitrage.Exchange.EtherDelta.Markets
{
	internal abstract class EtherDeltaMarket : Market
	{
		protected readonly OrdersStorage OrdersStorage;

		protected List<Dtos.Order> RemoteOrders;

		protected EtherDeltaMarket(OrdersStorage ordersStorage, ITradePair tradePair, Order approximateOrder) : 
			base(tradePair, approximateOrder)
		{
			OrdersStorage = ordersStorage;
		}

		public abstract IMarket WithActualVolume(Contract etherDeltaContract,
			EtherDeltaCoinsCollection etherDeltaCoinsCollection);

		protected override async Task OnInit()
		{
			ValidateCoinsPair(TradePair.CoinToSell, TradePair.CoinToBuy);

			var orders = await OrdersStorage.Orders(MarketAddress(TradePair.CoinToSell, TradePair.CoinToBuy));

			RemoteOrders = FilteredOrders(orders).ToList();
		}

		protected override Task<Order> OrderAt(int index)
		{
			if (index >= RemoteOrders.Count)
			{
				RaiseNotEnoughOrdersException();
			}

			return Task.FromResult(MapOrder(RemoteOrders[index]));
		}

		protected abstract Order MapOrder(Dtos.Order orders);

		protected abstract IEnumerable<Dtos.Order> FilteredOrders(Dtos.Orders remoteOrders);

		protected abstract string MarketAddress(Coin coinToSell, Coin coinToBuy);

		protected abstract void ValidateCoinsPair(Coin coinToSell, Coin coinToBuy);
	}
}
