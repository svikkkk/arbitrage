﻿using Arbitrage.Exchange.EtherDelta.Entities;
using Arbitrage.Exchange.EtherDelta.Orders;
using Arbitrage.Models;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Orders;
using Nethereum.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Arbitrage.Exchange.EtherDelta.Markets
{
	internal class BuyMarket : EtherDeltaMarket
	{
		private readonly decimal _aproximatePrice;
		private readonly decimal _tradeFeeFactor;

		private ActualVolumeBuyMarket _actualVolumeBuyMarket;

		public BuyMarket(
			OrdersStorage ordersStorage,
			ITradePair tradePair,
			decimal aproximatePrice,
			decimal tradeFeeFactor)
			: base(ordersStorage, tradePair, new EtherDeltaBuyOrder(aproximatePrice, decimal.One, tradePair.CoinToSell, tradePair.CoinToBuy, tradeFeeFactor))
		{
			_aproximatePrice = aproximatePrice;
			_tradeFeeFactor = tradeFeeFactor;
		}

		public override IMarket WithActualVolume(Contract etherDeltaContract, EtherDeltaCoinsCollection etherDeltaCoinsCollection)
		{
			return _actualVolumeBuyMarket ?? (_actualVolumeBuyMarket = new ActualVolumeBuyMarket(
				       OrdersStorage,
				       etherDeltaContract,
				       etherDeltaCoinsCollection,
				       TradePair,
				       _aproximatePrice, 
					   _tradeFeeFactor));
		}

		protected override string MarketAddress(Coin coinToSell, Coin coinToBuy)
		{
			return ((EtherDeltaCoin)coinToSell).Address;
		}

		protected override Order MapOrder(Dtos.Order etherDeltaOrder)
		{
			return new EtherDeltaBuyOrder(
				price: etherDeltaOrder.Price, 
				volume: etherDeltaOrder.EthAvailableVolume,
				sellCoin: TradePair.CoinToSell, 
				buyCoin: TradePair.CoinToBuy,
				tradeFeeFactor: _tradeFeeFactor);
		}

		protected override IEnumerable<Dtos.Order> FilteredOrders(Dtos.Orders remoteOrders)
		{
			return remoteOrders.Buys.Where(o => o.IsValid);
		}

		protected override void ValidateCoinsPair(Coin coinToSell, Coin coinToBuy)
		{
			if (coinToBuy != Coin.Ethereum())
			{
				throw new ArgumentException("coinToBuy must be Ethereum", nameof(coinToBuy));
			}
		}
	}
}
