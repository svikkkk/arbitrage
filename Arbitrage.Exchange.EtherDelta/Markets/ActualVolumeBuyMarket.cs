﻿using Arbitrage.Exchange.EtherDelta.Orders;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Orders;
using Nethereum.Contracts;
using System.Threading.Tasks;

namespace Arbitrage.Exchange.EtherDelta.Markets
{
	internal class ActualVolumeBuyMarket : BuyMarket
	{
		private readonly Contract _etherDeltaContract;
		private readonly EtherDeltaCoinsCollection _etherDeltaCoinsCollection;

		public ActualVolumeBuyMarket(
			OrdersStorage ordersStorage,
			Contract etherDeltaContract,
			EtherDeltaCoinsCollection etherDeltaCoinsCollection,
			ITradePair tradePair,
			decimal aproximatePrice,
			decimal tradeFeeFactor)
			: base(ordersStorage, tradePair, aproximatePrice, tradeFeeFactor)
		{
			_etherDeltaContract = etherDeltaContract;
			_etherDeltaCoinsCollection = etherDeltaCoinsCollection;
		}

		protected override async Task<Order> OrderAt(int index)
		{
			if (index >= RemoteOrders.Count)
			{
				RaiseNotEnoughOrdersException();
			}

			for (int i = 0, validOrdersIndex = 0; i < RemoteOrders.Count; i++)
			{
				RemoteOrders[i] = await new BuyOrdersAvailableVolume(RemoteOrders[i], _etherDeltaContract, _etherDeltaCoinsCollection)
					.ToOrder();

				if (!RemoteOrders[i].IsValid)
				{
					continue;
				}

				if (validOrdersIndex == index)
				{
					return MapOrder(RemoteOrders[i]);
				}

				validOrdersIndex++;
			}

			RaiseNotEnoughOrdersException();

			return null;
		}
	}
}
