﻿using System;
using System.Linq;
using System.Threading.Tasks;
using WebSocketSharp;

namespace Arbitrage.Exchange.EtherDelta
{
	internal class SocketPool
	{
		private const int PingInterval = 25000;

		private readonly WebSocket _listenerSocket;
		private readonly LockableWebSocket[] _webSockets;

		public event Action<string> MessageReceived = delegate { };
		public event Action<string> BroadcastMessageReceived = delegate { };

		public SocketPool(string socketUrl, int concurrentConnections)
		{
			_webSockets = new LockableWebSocket[concurrentConnections];

			for (var i = 0; i < concurrentConnections; i++)
			{
				var webSocket = new LockableWebSocket(socketUrl);

				webSocket.OnClose += OnSocketClose;
				webSocket.OnOpen += OnSocketOpen;
				webSocket.OnError += OnSocketError;
				webSocket.OnMessage += OnMessageReceive;

				webSocket.Connect();

				_webSockets[i] = webSocket;
			}

			_listenerSocket = _webSockets[0];

			_listenerSocket.OnClose += ReconnectOnSocketClose;
			_listenerSocket.OnMessage += OnBroadcastMessageReceive;

			var unused = StartPinging();
		}

		public int SendAndGetSocketId(string data)
		{
			var webSocket = _webSockets.First(s => !s.IsLocked);

			webSocket.Lock();

			if (webSocket.ReadyState == WebSocketState.Closed)
			{
				webSocket.Connect();
			}

			webSocket.Send("2");	// ping
			webSocket.Send(data);

			return webSocket.GetHashCode();
		}

		public void UnlockSocket(int socketId)
		{
			var webSocket = _webSockets.First(s => s.GetHashCode() == socketId);

			webSocket.Unlock();
		}

		private async Task StartPinging()
		{
			while (true)
			{
				await Task.Delay(PingInterval);

				try
				{
					switch (_listenerSocket.ReadyState)
					{
						case WebSocketState.Closed:
							_listenerSocket.Connect();
							break;
						case WebSocketState.Open:
							_listenerSocket.Send("2");
							break;
					}
				}
				catch
				{
					// ignore
				}
			}
		}

		private void OnMessageReceive(object sender, MessageEventArgs e)
		{
			MessageReceived.Invoke(e.Data);
		}

		private void OnBroadcastMessageReceive(object sender, MessageEventArgs e)
		{
			BroadcastMessageReceived.Invoke(e.Data);
		}

		private void OnSocketOpen(object sender, EventArgs e)
		{
			Console.WriteLine($"EtherDelta socket {sender.GetHashCode()} is open");
		}

		private void OnSocketClose(object sender, CloseEventArgs e)
		{
			Console.WriteLine($"EtherDelta socket {sender.GetHashCode()} is closed");
		}

		private void ReconnectOnSocketClose(object sender, CloseEventArgs e)
		{
			Console.WriteLine($"EtherDelta socket {sender.GetHashCode()} is reconnecting");

			((WebSocket)sender).Connect();
		}

		private void OnSocketError(object sender, ErrorEventArgs e)
		{
			Console.WriteLine($"EtherDelta socket error: {e.Message}");
		}
	}
}
