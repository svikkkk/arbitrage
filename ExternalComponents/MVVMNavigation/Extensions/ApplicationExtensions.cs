﻿using Xamarin.Forms;

namespace MVVMNavigation.Extensions
{
	public static class ApplicationExtensions
	{
		public static bool HasMainPage(this Application application)
		{
			return application.MainPage != null;
		}
	}
}
