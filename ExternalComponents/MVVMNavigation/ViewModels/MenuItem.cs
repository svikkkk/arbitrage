﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace MVVMNavigation.ViewModels
{
	public class MenuItem
    {
        public MenuItem(string icon, string text, Action onSelect)
        {
            Icon = icon;
            Text = text;
            SelectItemCommand = new Command(onSelect);
        }

        public string Icon { get; set; }

        public string Text { get; set; }

		public ICommand SelectItemCommand { get; set; }
	}
}

