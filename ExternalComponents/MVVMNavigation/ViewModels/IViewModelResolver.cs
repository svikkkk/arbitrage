﻿using System;
using MVVMNavigation.Interfaces;

namespace MVVMNavigation.ViewModels
{
	public interface IViewModelResolver
    {
        TViewModel ViewModel<TViewModel>() where TViewModel: IViewModel;

        IViewModel ViewModel(Type viewModelType);
    }
}

