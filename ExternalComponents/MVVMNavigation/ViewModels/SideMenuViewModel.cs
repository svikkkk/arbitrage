﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using CommandResolver.Interfaces;
using MVVMNavigation.Interfaces;
using INavigation = MVVMNavigation.Navigation.INavigation;

namespace MVVMNavigation.ViewModels
{
	public abstract class SideMenuViewModel : ViewModel, ISideMenuViewModel
	{
		private bool _isSideMenuVisible;

		public override bool IsVisible
		{
			get { return _isSideMenuVisible; }
			set
			{
				if (_isSideMenuVisible != value)
				{
					_isSideMenuVisible = value;
					OnPropertyChanged();
				}
			}
		}

		public string Icon { get; set; }

		protected SideMenuViewModel(INavigation navigation, ICommandResolver commandResolver) 
			: base(commandResolver, navigation)
		{
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			var handler = PropertyChanged;
			handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
