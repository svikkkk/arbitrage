﻿using System;

namespace MVVMNavigation.Pages
{
	public interface IVisibilityControlledPage
	{
		bool IsVisibleOnTheScreen { get; }

		void ViewWillAppear();

		void ViewWillDisappear();

		void DidEnterBackground();

		event Action WillAppear;
	}
}