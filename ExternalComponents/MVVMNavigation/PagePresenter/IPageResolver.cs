﻿using Xamarin.Forms;

namespace MVVMNavigation.Interfaces
{
	public interface IPageResolver
	{
		Page Page(string pageName);
	}
}
