﻿using System;

namespace Arbitrage.Models.Orders
{
	public class Order
	{
		public Order(decimal sellAmount, decimal buyAmount, Coin sellCoin, Coin buyCoin)
		{
			Sell = sellCoin;
			SellAmount = sellAmount;

			Buy = buyCoin;
			BuyAmount = buyAmount;
		}

		public Coin Sell { get; protected set; }

		public Coin Buy { get; protected set; }

		public decimal SellAmount { get; protected set; }

		public decimal BuyAmount { get; protected set; }

		public string AdditionalString { get; protected set; }

		public override string ToString()
		{
			return $"{Sell.Code} {SellAmount:F5} \u2192 {Buy.Code} {BuyAmount:F5}" + AdditionalString;
		}

		public Order WithSellBuyAmount(decimal sellAmountLimit, decimal buyAmountLimit)
		{
			var orderSellAmount = Math.Min(BuyAmount, buyAmountLimit);
			var orderBuyAmount = Math.Min(SellAmount, sellAmountLimit);

			var newBuyAmount = Math.Min(orderSellAmount, orderBuyAmount * BuyAmount / SellAmount);
			var newSellAmount = Math.Min(orderSellAmount * SellAmount / BuyAmount, orderBuyAmount);

			return new Order(
				sellAmount: newSellAmount,
				buyAmount: newBuyAmount, 
				sellCoin: Sell, 
				buyCoin: Buy)
			{
				AdditionalString = AdditionalString
			};
		}
	}
}
