﻿namespace Arbitrage.Models.Orders
{
	public class BuyOrder : Order
	{
		/// <summary>
		/// Creates order from buy order with price and volume
		/// </summary>
		/// <param name="price">Order price in quote currency (e.g. in BTC, ETH or LTC)</param>
		/// <param name="volume">Order volume in base currency (in some alt coin)</param>
		/// <param name="sellCoin">Base coin (some altcoin)</param>
		/// <param name="buyCoin">Quote coin. Usually it's BTC, ETH or LTC</param>
		/// <param name="tradeFeeFactor">Decreasing factor of BuyAmount (e.g. 0.998 for 0.2% trade fee)</param>
		/// <returns></returns>
		public BuyOrder(decimal price, decimal volume, Coin sellCoin, Coin buyCoin, decimal tradeFeeFactor)
			: base(sellAmount: volume, 
				  buyAmount: volume * price * tradeFeeFactor, 
				  sellCoin: sellCoin, 
				  buyCoin: buyCoin)
		{
			var noFeeBuyAmount = BuyAmount / tradeFeeFactor;
			var tradingFee = noFeeBuyAmount - BuyAmount;

			AdditionalString = $"\nFee: {tradingFee:F5} {Buy.Code}\nPrice: {price:F5}";
		}
	}
}
