﻿namespace Arbitrage.Models.Orders
{
	public class SellOrder : Order
	{
		/// <summary>
		/// Creates order from sell order with price and volume
		/// </summary>
		/// <param name="price">Order price in quote currency (e.g. in BTC, ETH or LTC)</param>
		/// <param name="volume">Order volume in base currency (in some alt coin)</param>
		/// <param name="sellCoin">Quote coin. Usually it's BTC, ETH or LTC</param>
		/// <param name="buyCoin">Base coin (some altcoin)</param>
		/// <param name="tradeFeeFactor">Increasing factor of SellAmount (e.g. 1.002 for 0.2% trade fee)</param>
		public SellOrder(decimal price, decimal volume, Coin sellCoin, Coin buyCoin, decimal tradeFeeFactor)
			: base(sellAmount: volume * price * tradeFeeFactor, 
				  buyAmount: volume, 
				  sellCoin: sellCoin, 
				  buyCoin: buyCoin)
		{
			var noFeeSellAmount = SellAmount / tradeFeeFactor;
			var tradingFee = SellAmount - noFeeSellAmount;

			AdditionalString = $"\nFee: {tradingFee:F5} {Sell.Code}\nPrice: {price:F5}";
		}
	}
}
