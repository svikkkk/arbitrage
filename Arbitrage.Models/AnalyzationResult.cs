﻿namespace Arbitrage.Models
{
    public class AnalyzationResult
    {
	    public long PotentiallyProfitablePlans;
	    public long ProfitablePlans;
	    public long ExceptionDuringCalculationPlans;
	    public long NotProfitableAfterAmountRecalculation;
	    public long NotFilteredRecalculation;
    }
}
