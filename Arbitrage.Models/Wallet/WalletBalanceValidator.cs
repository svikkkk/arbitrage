﻿using System;
using System.Threading.Tasks;

namespace Arbitrage.Models.Wallet
{
	public class ExchangeBalanceValidator
	{
		private readonly IWallet _wallet;

		public ExchangeBalanceValidator(IWallet wallet)
		{
			_wallet = wallet;
		}

		public async Task Validate(decimal amount)
		{
			var currentWalletBalance = await _wallet.Balance();
			if (currentWalletBalance < amount)
			{
				throw new ArgumentOutOfRangeException(string.Format("Wallet:{0} doesnt have enough coins", _wallet.Address));
			}
		}
	}
}
