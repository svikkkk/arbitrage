﻿using Arbitrage.Models.Exchange.Interfaces;
using System.Threading.Tasks;

namespace Arbitrage.Models.Wallet
{
	public interface IWallet
	{
		Coin Coin { get; }

		Task<decimal> Balance();

		string Address { get; }

		Task DepositTo(IExchange exchange, decimal amount);

		Task DepositTo(IWallet wallet, decimal amount);
	}
}
