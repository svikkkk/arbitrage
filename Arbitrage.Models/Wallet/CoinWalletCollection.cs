﻿using System.Collections.Generic;

namespace Arbitrage.Models.Wallet
{
	/// <summary>
	/// Exchanges that support coin
	/// </summary>
	public class CoinWalletCollection
	{
		private readonly IEnumerable<Coin> _coins;
		private readonly IEnumerable<IWallet> _wallets;

		public CoinWalletCollection(IEnumerable<Coin> coins, IEnumerable<IWallet> wallets)
		{
			_coins = coins;
			_wallets = wallets;
		}

		public IList<IWallet> ToList()
		{
			var walletsWithCoinSupport = new List<IWallet>();
			if (_wallets != null)
			{
				foreach (var wallet in _wallets)
				{
					var isCoinSupposrted = IsCoinSupported(wallet);
					if (isCoinSupposrted)
					{
						walletsWithCoinSupport.Add(wallet);
					}
				}
			}

			return walletsWithCoinSupport;
		}

		private bool IsCoinSupported(IWallet wallet)
		{
			var isCoinSupported = false;

			foreach (var coin in _coins)
			{
				if (wallet.Coin.Equals(coin))
				{
					isCoinSupported = true;
					break;
				}
			}

			return isCoinSupported;
		}
	}
}
