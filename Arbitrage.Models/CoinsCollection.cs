﻿using System;	
using System.Collections.Generic;
using System.Linq;

namespace Arbitrage.Models
{
	public class CoinsCollection
	{
		protected readonly IEnumerable<Coin> Coins;

		public CoinsCollection(IEnumerable<Coin> coins)
		{
			Coins = coins;
		}

		public IList<Coin> ToList()
		{
			return Coins.ToList();
		}

		public Coin Coin(string code)
		{
			try
			{
				return Coins.Single(c => c.Code.Equals(code));
			}
			catch (InvalidOperationException)
			{
				throw new CoinNotFoundException($"Coin {code} is not found");
			}
		}
	}

	public class CoinNotFoundException : Exception
	{
		public CoinNotFoundException(string message) : base(message)
		{
		}
	}
}
