﻿using System;
using System.Collections.Generic;
using System.Linq;
using Arbitrage.Models.Exchange.Interfaces;

namespace Arbitrage.Models.Exchange
{
	public class MarketsCollection : IMarketsCollection
	{
		private readonly IList<IMarket> _markets;

		public MarketsCollection(IList<IMarket> markets)
		{
			_markets = markets;
		}

		public IList<IMarket> ToList()
		{
			return _markets;
		}

		public virtual IMarket Market(Coin coinToSell, Coin coinToBuy, bool withActualOrdersVolume)
		{
			var market = ToList().SingleOrDefault(m => m.TradePair.CoinToSell.Equals(coinToSell) && m.TradePair.CoinToBuy.Equals(coinToBuy));
			if (market == null)
			{
				throw new MarketNotFoundException($"Market for: {coinToSell} \u2192 {coinToBuy} is not found");
			}

			return market;
		}
	}

	public class MarketNotFoundException : Exception
	{
		public MarketNotFoundException(string message) : base(message)
		{
		}
	}
}
