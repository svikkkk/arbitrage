﻿using Arbitrage.Models.Exchange.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Arbitrage.Models.Exchange
{
	/// <summary>
	/// Exchanges that support coin
	/// </summary>
	public class SupportedCoinsExchangeCollection
	{
		private readonly IEnumerable<Coin> _coins;
		private readonly IList<IExchange> _exchanges;

		public SupportedCoinsExchangeCollection(IEnumerable<Coin> coins, IList<IExchange> exchanges)
		{
			_coins = coins;
			_exchanges = exchanges;
		}

		public async Task<IList<IExchange>> ToList()
		{
			var exchangesWithCoinSupport = new List<IExchange>();

			foreach (var exchange in _exchanges)
			{
				var isCoinSupposrted = await IsAnyCoinSupported(exchange);
				if (isCoinSupposrted)
				{
					exchangesWithCoinSupport.Add(exchange);
				}

			}

			return exchangesWithCoinSupport;
		}

		private async Task<bool> IsAnyCoinSupported(IExchange exchange)
		{
			var isCoinSupported = false;

			var exchangeDeposits = await exchange.DepositsCollection();
			foreach (var exchangeDeposit in exchangeDeposits)
			{
				if (IsCoinSupported(exchangeDeposit.Coin))
				{
					isCoinSupported = true;
					break;
				}
			}

			return isCoinSupported;
		}

		private bool IsCoinSupported(Coin coinToCheck)
		{
			var isCoinSupported = false;
			foreach (var coin in _coins)
			{
				if (coin.Equals(coinToCheck))
				{
					isCoinSupported = true;
					break;
				}
			}

			return isCoinSupported;
		}
	}
}
