﻿namespace Arbitrage.Models.Exchange
{
	public enum MarketRate
	{
		Approximate,
		BasedOnOrders,
		OrdersWithActualVolume
	}
}
