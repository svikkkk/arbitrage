﻿using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Wallet;
using System.Linq;
using System.Threading.Tasks;

namespace Arbitrage.Models.Exchange.Concretes
{
	internal class ExchangeWithdraw : IExchangeWithdraw
	{
		private readonly IExchangeClient _exchangeClient;

		public Coin Coin { get; }
	    
	    public ExchangeWithdraw(Coin coin, IExchangeClient exchangeClient)
	    {
		    Coin = coin;
		    _exchangeClient = exchangeClient;
	    }

	    public Task<decimal> Fee(decimal withdrawAmount)
	    {
		    return _exchangeClient.WithdrawFee(Coin, withdrawAmount);
	    }

		public Task<decimal?> MinimalWithdraw()
		{
			return _exchangeClient.MinimalWithdraw(Coin);
		}

		public Task WithdrawTo(IWallet wallet, decimal amount)
	    {
		    return _exchangeClient.SubmitWithdraw(Coin, amount, wallet.Address);
	    }

	    public async Task WithdrawTo(IExchange exchange, decimal amount)
	    {
		    var address = await(await exchange.DepositsCollection()).Single(d => d.Coin.Equals(Coin)).Address();

		    await _exchangeClient.SubmitWithdraw(Coin, amount, address);
		}
    }
}
