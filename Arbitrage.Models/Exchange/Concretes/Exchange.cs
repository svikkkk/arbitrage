﻿using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Exchange.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Arbitrage.Models.Orders;

namespace Arbitrage.Models.Exchange.Concretes
{
	public class Exchange : IExchange, IDisposable
	{
		private readonly SemaphoreSlim _marketsSemaphoreSlim = new SemaphoreSlim(1, 1);
		private readonly SemaphoreSlim _coinsSemaphoreSlim = new SemaphoreSlim(1, 1);

		private IMarketsCollection _marketsCollection;
		private CoinsCollection _coinsCollection;

		protected IExchangeClient ExchangeClient { get; }

		public virtual string Name { get; }

		public Exchange(string name, IExchangeClient exchangeClient)
		{
			ExchangeClient = exchangeClient;
			Name = name;
		}

		public virtual void Clear()
		{
			_marketsSemaphoreSlim.Wait();
			_marketsCollection = null;
			_marketsSemaphoreSlim.Release();

			_coinsSemaphoreSlim.Wait();
			_coinsCollection = null;
			_coinsSemaphoreSlim.Release();

			(ExchangeClient as IClearable)?.Clear();
		}

		public virtual void Dispose()
		{
			ExchangeClient.Dispose();
		}

		public Task<decimal> Balance(Coin coin)
		{
			return ExchangeClient.Balance(coin);
		}

		public virtual async Task<IMarketsCollection> Markets()
		{
			if (_marketsCollection == null)
			{
				await _marketsSemaphoreSlim.WaitAsync();

				try
				{
					if (_marketsCollection == null)
					{
						_marketsCollection = await ExchangeClient.Markets();
					}
				}
				finally
				{
					_marketsSemaphoreSlim.Release();
				}
			}

			return _marketsCollection;
		}

		/// <summary>
		/// Returns collection of the coins which can be traded, withdrawn and deposited.
		/// </summary>
		/// <returns></returns>
		public virtual async Task<CoinsCollection> CoinsCollection()
		{
			if (_coinsCollection == null)
			{
				await _coinsSemaphoreSlim.WaitAsync();

				try
				{
					if (_coinsCollection == null)
					{
						var markets = await Markets();

						var coins = markets
							.ToList()
							.SelectMany(m => new[] { m.TradePair.CoinToBuy, m.TradePair.CoinToSell })
							.Distinct(new CoinCompare());

						_coinsCollection = new CoinsCollection(coins);
					}
				}
				finally
				{
					_coinsSemaphoreSlim.Release();
				}
			}

			return _coinsCollection;
		}

		public virtual async Task<IEnumerable<IExchangeDeposit>> DepositsCollection()
		{
			return (await CoinsCollection()).ToList().Select(c => new ExchangeDeposit(c, ExchangeClient));
		}

		public virtual async Task<IEnumerable<IExchangeWithdraw>> WithdrawsCollection()
		{
			return (await CoinsCollection()).ToList().Select(c => new ExchangeWithdraw(c, ExchangeClient));
		}

		public async Task Trade(Order order)
		{
			await new ExchangeOrderValidation(this).Validate(order);
			await OnTrade(order);
		}

		protected virtual Task OnTrade(Order order)
		{
			throw new NotImplementedException();
		}
	}
}
