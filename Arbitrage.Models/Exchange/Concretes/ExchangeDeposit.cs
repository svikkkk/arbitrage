﻿using Arbitrage.Models.Exchange.Interfaces;
using System.Threading.Tasks;

namespace Arbitrage.Models.Exchange.Concretes
{
	internal class ExchangeDeposit : IExchangeDeposit
	{
		private readonly IExchangeClient _exchangeClient;

		public Coin Coin { get; }

		public ExchangeDeposit(Coin coin, IExchangeClient exchangeClient)
		{
			Coin = coin;
			_exchangeClient = exchangeClient;
		}

		public Task<string> Address()
		{
			return _exchangeClient.DepositDestinationAddress(Coin);
		}

		public decimal Fee(decimal depositAmount)
		{
			return 0m;
		}

		public decimal ReverseFee(decimal amountToBeDeposited)
		{
			return 0m;
		}
	}
}
