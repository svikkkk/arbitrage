﻿using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Orders;
using System.Threading.Tasks;

namespace Arbitrage.Models.Exchange.Concretes
{
	public class ApproximateMarketRate : IMarketRate
	{
		private readonly Order _approximateOrder;

		public ApproximateMarketRate(Order approximateOrder)
		{
			_approximateOrder = approximateOrder;
		}

		/// <summary>
		/// Returns how many coins you have to sell.
		/// Fast but approximate implementation.
		/// </summary>
		/// <param name="amountToBuy"></param>
		/// <returns>Approximate amount of selling coins that you have to sell if you want to buy expected amount of buying coins.
		/// Transaction fee is already included.</returns>
		public Task<decimal> RequiredAmountToSell(decimal amountToBuy)
		{
			var factor = _approximateOrder.SellAmount / _approximateOrder.BuyAmount;

			return Task.FromResult(factor * amountToBuy);
		}

		/// <summary>
		/// Returns how many coins you can buy.
		/// Fast but approximate implementation.
		/// </summary>
		/// <param name="amountToSell"></param>
		/// <returns>Approximate amount of buying coins that you can buy for selling amount coins.
		/// Transaction fee is already included.</returns>
		public Task<decimal> AvailableAmountToBuy(decimal amountToSell)
		{
			var factor = _approximateOrder.SellAmount / _approximateOrder.BuyAmount;

			return Task.FromResult(amountToSell / factor);
		}
	}
}
