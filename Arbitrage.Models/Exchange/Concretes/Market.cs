﻿using Arbitrage.Models.Exchange.Exceptions;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Orders;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Arbitrage.Models.Exchange.Concretes
{
	public abstract class Market : IMarket
	{
		private readonly Order _approximateOrder;
		private readonly SemaphoreSlim _initSemaphore = new SemaphoreSlim(1);

		private bool _isInited;

		public ITradePair TradePair {get;}

		protected Market(ITradePair tradePair, Order approximateOrder)
		{
			_approximateOrder = approximateOrder;

			TradePair = tradePair;
		}

		/// <inheritdoc />
		public async Task<IList<Order>> OrdersToTake(decimal amountToSellLimit, decimal amountToBuyLimit)
		{
			var ordersToTake = new List<Order>();

			await Init();

			var orderIndex = 0;

			do
			{
				var order = await OrderAt(orderIndex);
				
				var partialOrder = order.WithSellBuyAmount(amountToSellLimit, amountToBuyLimit);

				ordersToTake.Add(partialOrder);
				
				amountToBuyLimit -= partialOrder.BuyAmount;
				amountToSellLimit -= partialOrder.SellAmount;
				orderIndex++;
			} while (!AreLimitsReached(amountToBuyLimit, amountToSellLimit));

			return ordersToTake;
		}

		public IMarketRate Rate(MarketRate rateType)
		{
			return rateType == MarketRate.Approximate
				? (IMarketRate) new ApproximateMarketRate(_approximateOrder)
				: (IMarketRate) new RealOrdersMarketRate(this, rateType == MarketRate.OrdersWithActualVolume);
		}

		protected void RaiseNotEnoughOrdersException()
		{
			throw new NotEnoughOrdersException($"Not enough orders on the {TradePair.CoinToSell} \u2192 {TradePair.CoinToBuy} market.");
		}

		protected abstract Task OnInit();

		protected abstract Task<Order> OrderAt(int index);

		private async Task Init()
		{
			if (!_isInited)
			{
				try
				{
					await _initSemaphore.WaitAsync();

					if (!_isInited)
					{
						await OnInit();
					}

					_isInited = true;
				}
				finally
				{
					_initSemaphore.Release();
				}
			}
		}

		private bool AreLimitsReached(decimal amountToBuyLimit, decimal amountToSellLimit)
		{
			return amountToBuyLimit < TradePair.MinimumCoinToBuyAmount
				   || amountToSellLimit < TradePair.MinimumCoinToSellAmount
				   || amountToBuyLimit <= 0m
			       || amountToSellLimit <= 0m;
		}
	}
}
