﻿using Arbitrage.Models.Exchange.Interfaces;

namespace Arbitrage.Models.Exchange.Concretes
{
	public class TradePair : ITradePair
	{
	    public Coin CoinToSell { get;  }

	    public Coin CoinToBuy { get;  }

	    public decimal MinimumCoinToSellAmount { get; }

	    public decimal MinimumCoinToBuyAmount { get; }

		public TradePair(Coin coinToSell, Coin coinToBuy, decimal minimumCoinToSellAmount, decimal minimumCoinToBuyAmount)
		{
			CoinToSell = coinToSell;
			CoinToBuy = coinToBuy;
			MinimumCoinToSellAmount = minimumCoinToSellAmount;
			MinimumCoinToBuyAmount = minimumCoinToBuyAmount;
		}
    }
}
