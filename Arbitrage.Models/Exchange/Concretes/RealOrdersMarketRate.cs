﻿using System.Linq;
using System.Threading.Tasks;
using Arbitrage.Models.Exchange.Interfaces;

namespace Arbitrage.Models.Exchange.Concretes
{
    public class RealOrdersMarketRate : IMarketRate
    {
	    private readonly IMarket _market;
	    private readonly bool _withActualOrdersVolume;

	    public RealOrdersMarketRate(IMarket market, bool withActualOrdersVolume)
	    {
		    _market = market;
		    _withActualOrdersVolume = withActualOrdersVolume;
	    }

		/// <inheritdoc />
		public async Task<decimal> RequiredAmountToSell(decimal amountToBuy)
	    {
		    return (await _market.OrdersToTake(decimal.MaxValue, amountToBuy)).Sum(o => o.SellAmount);
	    }

	    /// <inheritdoc />
	    public async Task<decimal> AvailableAmountToBuy(decimal amountToSell)
	    {
		    return (await _market.OrdersToTake(amountToSell, decimal.MaxValue)).Sum(o => o.BuyAmount);
	    }
	}
}
