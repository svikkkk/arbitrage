﻿using System;

namespace Arbitrage.Models.Exchange.Exceptions
{
	public class NotEnoughOrdersException : Exception
	{
		public NotEnoughOrdersException(string message) : base(message)
		{
		}
	}
}
