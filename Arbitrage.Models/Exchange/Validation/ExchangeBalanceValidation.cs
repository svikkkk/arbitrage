﻿using System;
using System.Threading.Tasks;
using Arbitrage.Models.Exchange.Interfaces;

namespace Arbitrage.Models.Exchange.Validation
{
	public class ExchangeBalanceValidation
	{
		private readonly IExchange _exchange;

		public ExchangeBalanceValidation(IExchange exchange)
		{
			_exchange = exchange;
		}

		public async Task Validate(Coin coin, decimal amount)
		{
			var currentExchangeBalance = await _exchange.Balance(coin);
			if (currentExchangeBalance < amount)
			{
				throw new ArgumentOutOfRangeException(string.Format("Exchange:{0} doesnt have enough coins: {1}", _exchange.Name, coin.Code));
			}
		}
	}
}
