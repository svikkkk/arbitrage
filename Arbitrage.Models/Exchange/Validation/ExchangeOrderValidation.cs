﻿using Arbitrage.Models.Exchange.Interfaces;
using System;
using System.Threading.Tasks;
using Arbitrage.Models.Orders;

namespace Arbitrage.Models.Exchange.Validation
{
	public class ExchangeOrderValidation
	{
		private readonly IExchange _exchange;

		public ExchangeOrderValidation(IExchange exchange)
		{
			_exchange = exchange;
		}

		public async Task Validate(Order order)
		{
			await new ExchangeMarketSupportValidation(_exchange).Validate(order.Buy, order.Sell);
			await new ExchangeBalanceValidation(_exchange).Validate(order.Buy, await OrderFromAmount(order));
		}

		private async Task<decimal> OrderFromAmount(Order order)
		{
			if (order.SellAmount > 0 && order.BuyAmount > 0)
			{
				return order.BuyAmount;
			}

			if (order.SellAmount > 0 && order.BuyAmount == 0)
			{
				var exchangeMarkets = await _exchange.Markets();
				return await exchangeMarkets.Market(order.Buy, order.Sell, false)
					.Rate(MarketRate.BasedOnOrders)
					.RequiredAmountToSell(order.SellAmount);
			}

			if (order.SellAmount == 0 && order.BuyAmount > 0)
			{
				return order.BuyAmount;
			}

			throw new ArgumentOutOfRangeException("Incorrect order amount arguments");
		}
	}
}
