﻿using Arbitrage.Models.Exchange.Interfaces;
using System;
using System.Threading.Tasks;

namespace Arbitrage.Models.Exchange.Validation
{
	public class ExchangeMarketSupportValidation
	{
		private readonly IExchange _exchange;

		public ExchangeMarketSupportValidation(IExchange exchange)
		{
			_exchange = exchange;
		}

		public async Task Validate(Coin first, Coin second)
		{
			var isMarketSupported = await IsMarketSupported(first, second);
			if (!isMarketSupported)
			{
				throw new ArgumentOutOfRangeException(
					$"Exchange: {_exchange.Name} doesn't support market for coins: {first.Code} \u2192 {second.Code}");
			}
		}

		private async Task<bool> IsMarketSupported(Coin first, Coin second)
		{
			var markets = await _exchange.Markets();
			var isMarketSupported = false;
			foreach (var market in markets.ToList())
			{
				if (market.TradePair.CoinToSell.Equals(first) && market.TradePair.CoinToBuy.Equals(second))
				{
					isMarketSupported = true;
					break;
				}
			}

			return isMarketSupported;
		}
	}
}
