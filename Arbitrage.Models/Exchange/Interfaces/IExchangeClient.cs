﻿using System;
using System.Threading.Tasks;

namespace Arbitrage.Models.Exchange.Interfaces
{
	public interface IExchangeClient : IDisposable
	{
		Task<decimal> Balance(Coin coin);

		Task<IMarketsCollection> Markets();

		Task SubmitWithdraw(Coin coin, decimal amount, string destinationAddress);

		Task<decimal> WithdrawFee(Coin coin, decimal amount);

		Task<decimal?> MinimalWithdraw(Coin coin);

		Task<string> DepositDestinationAddress(Coin coin);
	}
}
