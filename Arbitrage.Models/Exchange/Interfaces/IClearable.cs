﻿namespace Arbitrage.Models.Exchange.Interfaces
{
    public interface IClearable
    {
	    void Clear();
    }
}
