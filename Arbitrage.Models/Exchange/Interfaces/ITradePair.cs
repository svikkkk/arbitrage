﻿namespace Arbitrage.Models.Exchange.Interfaces
{
	public interface ITradePair
	{
		Coin CoinToBuy { get; }
		Coin CoinToSell { get; }
		decimal MinimumCoinToBuyAmount { get; }
		decimal MinimumCoinToSellAmount { get; }
	}
}