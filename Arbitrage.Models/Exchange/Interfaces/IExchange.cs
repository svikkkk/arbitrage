﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Arbitrage.Models.Orders;

namespace Arbitrage.Models.Exchange.Interfaces
{
	public interface IExchange
	{
		string Name { get; }

		Task<decimal> Balance(Coin coin);

		Task<IMarketsCollection> Markets();

		Task<IEnumerable<IExchangeDeposit>> DepositsCollection();

		Task<IEnumerable<IExchangeWithdraw>> WithdrawsCollection();

		Task<CoinsCollection> CoinsCollection();

		Task Trade(Order order);

		void Clear();
	}
}
