﻿using Arbitrage.Models.Wallet;
using System.Threading.Tasks;

namespace Arbitrage.Models.Exchange.Interfaces
{
	public interface IExchangeWithdraw
	{
		Coin Coin { get; }

		Task<decimal> Fee(decimal withdrawAmount);

		Task<decimal?> MinimalWithdraw();

		Task WithdrawTo(IWallet wallet, decimal amount);

		Task WithdrawTo(IExchange exchange, decimal amount);
	}
}
