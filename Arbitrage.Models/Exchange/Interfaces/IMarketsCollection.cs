﻿using System.Collections.Generic;

namespace Arbitrage.Models.Exchange.Interfaces
{
    public interface IMarketsCollection
    {
	    IList<IMarket> ToList();

	    IMarket Market(Coin coinToSell, Coin coinToBuy, bool withActualOrdersVolume);
    }
}
