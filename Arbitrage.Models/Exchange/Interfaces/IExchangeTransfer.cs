﻿using Arbitrage.Models.Wallet;
using System.Threading.Tasks;

namespace Arbitrage.Models.Exchange.Interfaces
{
	public interface IExchangeTransfer
	{
		Task Transfer(Coin coin, IExchange from, IExchange to);

		Task Transfer(Coin coin, IWallet from, IExchange to);

		Task Transfer(Coin coin, IExchange from, IWallet to);

		Task Transfer(Coin coin, IWallet from, IWallet to);
	}
}
