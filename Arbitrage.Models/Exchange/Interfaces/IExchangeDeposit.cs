﻿using System.Threading.Tasks;

namespace Arbitrage.Models.Exchange.Interfaces
{
	public interface IExchangeDeposit
	{
		Coin Coin { get; }

		Task<string> Address();

		decimal Fee(decimal depositAmount);

		decimal ReverseFee(decimal amountToBeDeposited);
	}
}
