﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Arbitrage.Models.Orders;

namespace Arbitrage.Models.Exchange.Interfaces
{
	public interface IMarket
	{
		ITradePair TradePair { get; }

		Task<IList<Order>> OrdersToTake(decimal amountToSellLimit, decimal amountToBuyLimit);

		IMarketRate Rate(MarketRate rateType);
	}
}
