﻿using System.Threading.Tasks;

namespace Arbitrage.Models.Exchange.Interfaces
{
	public interface IMarketRate
	{
		/// <summary>
		/// Returns how many coins you have to sell.
		/// </summary>
		/// <param name="amountToBuy"></param>
		/// <returns>Amount of selling coins that you have to sell if you want to buy expected amount of buying coins.
		/// Transaction fee is already included.</returns>
		Task<decimal> RequiredAmountToSell(decimal amountToBuy);

		/// <summary>
		/// Returns how many coins you can buy.
		/// </summary>
		/// <param name="amountToSell"></param>
		/// <returns>Amount of buying coins that you can buy for selling amount coins.
		/// Transaction fee is already included.</returns>
		Task<decimal> AvailableAmountToBuy(decimal amountToSell);
	}
}