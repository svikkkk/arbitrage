﻿using Arbitrage.Models.Exchange.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Arbitrage.Models.Exchange
{
	public class ExchangeCoinsCollection
	{
		public static Coin[] BlackList =
		{
			new Coin("MINT"),
			new Coin("PLC"),
			new Coin("BTCS"),
			new Coin("CAT"),
			new Coin("BAT"),
			new Coin("BCD"),
			new Coin("NET"),
			new Coin("BLZ"),
			new Coin("ACC"),
			new Coin("PCC"),
			new Coin("ATH"),
			new Coin("RC"),
			new Coin("SKR"),
			new Coin("BTG"),
			new Coin("HAV"),
			new Coin("CMT"),
			new Coin("GAS"),
			new Coin("BTA"),
			new Coin("ARC"),
		};

		private readonly IList<IExchange> _exchanges;

		public ExchangeCoinsCollection(IList<IExchange> exchanges)
		{
			_exchanges = exchanges;
		}

		public async Task<IList<Coin>> ToList()
		{
			var coins = new List<Coin>();
			foreach (var exchange in _exchanges)
			{
				var exchangeCoins = await exchange.CoinsCollection();
				coins.AddRange(exchangeCoins.ToList());
			}

			coins = coins.Except(BlackList, new CoinCompare()).ToList();

			return coins.Distinct(new CoinCompare()).ToList();
		}
	}
}
