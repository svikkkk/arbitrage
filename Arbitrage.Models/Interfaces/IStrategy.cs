﻿using System.Threading.Tasks;

namespace Arbitrage.Models.Interfaces
{
	public interface IStrategy
	{
		bool IsRunning { get; }

		Task Start();

		Task Stop();
	}
}
