﻿using System;
using System.Collections.Generic;

namespace Arbitrage.Models
{
	public class Coin : IEquatable<Coin>
	{
		public string Code { get; }

		public Coin(string code)
		{
			Code = code;
		}

		public static Coin Ethereum()
		{
			return new Coin("ETH");
		}

		public static Coin Bitcoin()
		{
			return new Coin("BTC");
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			return Equals(obj as Coin);
		}

		public bool Equals(Coin other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return string.Equals(Code, other.Code);
		}

		public override int GetHashCode()
		{
			return (Code != null ? Code.GetHashCode() : 0);
		}

		public static bool operator ==(Coin left, Coin right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(Coin left, Coin right)
		{
			return !Equals(left, right);
		}

		public override string ToString()
		{
			return Code;
		}
	}

	public class CoinCompare : IEqualityComparer<Coin>
	{
		public bool Equals(Coin x, Coin y)
		{
			return x.Equals(y);
		}

		public int GetHashCode(Coin coin)
		{
			return coin.GetHashCode();
		}
	}
}
