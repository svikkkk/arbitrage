﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Arbitrage.Business;
using Arbitrage.Planner.Plan;
using Microsoft.AspNetCore.Mvc;
using Arbitrage.Server.Models;
using Quartz;

namespace Arbitrage.Server.Controllers
{
	public class HomeController : Controller
    {
	    private readonly IScheduler _scheduler;

	    private JobKey JobKey => new JobKey("job1", "group1");

	    public HomeController(IScheduler scheduler)
	    {
		    _scheduler = scheduler;
	    }

		public async Task<IActionResult> Index()
		{
			var planAnalyzation = await PlansAnalyzation();

			if (planAnalyzation == null || planAnalyzation.State == AnalyzationState.None)
	        {
				return View(StateViewModel(planAnalyzation));
			}

			return RedirectToAction("AnalyzationDetails");
		}

	    public async Task<IActionResult> AnalyzationDetails()
	    {
		    var planAnalyzation = await PlansAnalyzation();

		    if (planAnalyzation == null || planAnalyzation.State == AnalyzationState.None)
		    {
				return RedirectToAction("Index");
			}

		    var viewModel = new AnalyzationDetailsViewModel
		    {
			    State = StateViewModel(planAnalyzation),
				LastCalculatedDate = planAnalyzation.PlansCalculated,
			    Plans = new List<AnalyzationDetailsViewModel.PlanListItem>(),
				Errors = planAnalyzation.Errors.Select(e => e.Message).ToList().ToList(),
		    };

		    return View(viewModel);
	    }

	    public async Task<IActionResult> Start()
	    {
		    // define the job and tie it to our HelloJob class
		    IJobDetail job = JobBuilder.Create<PlansAnalyzationJob>()
			    .WithIdentity(JobKey)
			    .Build();

		    // Trigger the job to run now, and then repeat every 10 seconds
		    ITrigger trigger = TriggerBuilder.Create()
			    .WithIdentity("trigger1", "group1")
			    .StartNow()
			    .Build();

		    // Tell quartz to schedule the job using our trigger
		    await _scheduler.ScheduleJob(job, trigger);

			return RedirectToAction("AnalyzationDetails");
	    }

		public async Task<IActionResult> Stop()
	    {
		    var planAnalyzation = await PlansAnalyzation();
			planAnalyzation?.Stop();
			return RedirectToAction("Index");
	    }

		public async Task<IActionResult> PlanDetails(int id)
		{
			var planAnalyzation = await PlansAnalyzation();
			var plan = new TradingPlan(null, 0, 0);
			var stepNumber = 0;
			var viewModel = new PlanDetailsViewModel
			{
				Id = plan.Id,
				TotalFactor = plan.ProfitRatio,
				Steps = plan.Steps.Select(step => new PlanDetailsViewModel.StepListItem
				{
					Number = stepNumber++,
					StepDetails = step.ToString()
				}).ToList()				
			};

			return View(viewModel);
		}

		public async Task<IActionResult> StepDetails(int planId, int stepNumber)
		{
			var planAnalyzation = await PlansAnalyzation();
			//var plan = planAnalyzation.Plans.FirstOrDefault(p => p.Id == planId);
		//	var step = plan.Steps[stepNumber];
			
			var viewModel = new PlanStepViewModel
			{
				StepDetails = "",
				Orders = new List<string>()
			};

//			if (step is TradeAction tradeAction)
//			{
//				var orders = await tradeAction.Orders(tradeAction.Ratio);
//				viewModel.Orders = orders.Select(o => o.BuyAmount + "||" + o.SellAmount).ToList();
//			}

			return View(viewModel);
		}

		public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

	    private async Task<TradingPlansAnalyzation> PlansAnalyzation()
	    {
		    var jobs = await _scheduler.GetCurrentlyExecutingJobs();
		    foreach (var jobExecutionContext in jobs)
		    {
			    if (jobExecutionContext.JobInstance is PlansAnalyzationJob analyzationJob)
			    {
				    return analyzationJob.Analyzation;
			    }
				
		    }

		    return null;
	    }

		private AnalyzationStateViewModel StateViewModel(TradingPlansAnalyzation plansAnalyzation)
	    {
		    if (plansAnalyzation == null)
		    {
			    return new AnalyzationStateViewModel();
		    }

		    return new AnalyzationStateViewModel
		    {
			    Depth = plansAnalyzation.Configuration.AnalyzationDepth,
			    StartAmount = plansAnalyzation.Configuration.StartAmount,
			    StartCoin = plansAnalyzation.Configuration.StartCoin,
			    State = plansAnalyzation.State.ToString()
		    };
		}
	}
}
