﻿using System.Collections.Generic;

namespace Arbitrage.Server.Models
{
    public class PlanDetailsViewModel
    {
	    public int Id { get; set; }

		public decimal TotalFactor { get; set; }

		public List<StepListItem> Steps { get; set; }

		public class StepListItem
		{
			public int Number { get; set; }

			public string StepDetails { get; set; }
		}
	}
}
