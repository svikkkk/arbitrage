﻿using System.Collections.Generic;
using Arbitrage.Models;

namespace Arbitrage.Server.Models
{
    public class AnalyzationStateViewModel
    {
		public int Depth { get; set; }

		public Coin StartCoin { get; set; }

		public decimal StartAmount { get; set; }

		public int ExchangeCoins { get; set; }

		public string State { get; set; }

		public int PotentialsPlansCalculated { get; set; }

		public List<string> Plans { get; set; }
	}
}
