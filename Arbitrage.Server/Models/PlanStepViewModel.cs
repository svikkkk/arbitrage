﻿using System.Collections.Generic;

namespace Arbitrage.Server.Models
{
    public class PlanStepViewModel
    {
	    public string StepDetails { get; set; }

		public List<string> Orders { get; set; }
	}
}
