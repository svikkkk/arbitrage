﻿using System;
using System.Collections.Generic;

namespace Arbitrage.Server.Models
{
    public class AnalyzationDetailsViewModel
    {
		public AnalyzationStateViewModel State { get; set; }

		public List<PlanListItem> Plans { get; set; }

		public DateTime LastCalculatedDate { get; set; }

	    public List<string> Errors { get; set; }

		public class PlanListItem
		{
			public int Id { get; set; }

			public decimal TotalFactor { get; set; }

			public int StepsCount { get; set; }
		}
	}
}
