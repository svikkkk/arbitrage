﻿using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Quartz;

namespace Arbitrage.Server
{
    public class Startup
    {
	    public IConfiguration Configuration { get; }

		public Startup(IHostingEnvironment env)
        {
	        var builder = new ConfigurationBuilder()
		        .SetBasePath(env.ContentRootPath)
		        .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
		        .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
		        .AddEnvironmentVariables();
	        Configuration = builder.Build();
        }

	    // ConfigureContainer is where you can register things directly
	    // with Autofac. This runs after ConfigureServices so the things
	    // here will override registrations made in ConfigureServices.
	    // Don't build the container; that gets done for you. If you
	    // need a reference to the container, you need to use the
	    // "Without ConfigureContainer" mechanism shown later.
	    public void ConfigureContainer(ContainerBuilder builder)
	    {
		    var scheduler = new Quartz().Start().GetAwaiter().GetResult();
		    builder.Register<IScheduler>((c) => scheduler).SingleInstance();
		}

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
        {
	        services.AddMvc();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
			if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
