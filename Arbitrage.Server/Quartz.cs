﻿using System.Collections.Specialized;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;

namespace Arbitrage.Server
{
    public class Quartz
    {
	    public async Task<IScheduler> Start()
	    {
			var scheduler = await Scheduler();
		    await scheduler.Start();
		    return scheduler;
	    }

	    private Task<IScheduler> Scheduler()
	    {
		    return new StdSchedulerFactory(ShedulerInitProperties()).GetScheduler();
		}

		private NameValueCollection ShedulerInitProperties()
	    {
			return new NameValueCollection
			{
				["quartz.scheduler.instanceName"] = "RemoteServer",
				["quartz.threadPool.type"] = "Quartz.Simpl.SimpleThreadPool, Quartz",
				["quartz.threadPool.threadCount"] = "5",
				["quartz.threadPool.threadPriority"] = "Normal"
			};
		}
	}
}
