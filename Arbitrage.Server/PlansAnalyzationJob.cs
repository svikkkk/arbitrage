﻿using System.IO;
using System.Threading.Tasks;
using Arbitrage.Business;
using Arbitrage.Models;
using Microsoft.Extensions.Configuration;
using Quartz;

namespace Arbitrage.Server
{
    public class PlansAnalyzationJob : IJob
    {
	    public TradingPlansAnalyzation Analyzation { get; private set; }

		public Task Execute(IJobExecutionContext context)
		{
			var builder = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json");

			var configuration = builder.Build();
			Analyzation = new TradingPlansAnalyzation(new PlansAnalyzationFilter(), new SupportedExchangesCollection(configuration), Coin.Ethereum(), 2m, 6);
			return Analyzation.Start();
	    }
    }
}
