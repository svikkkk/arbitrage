﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Arbitrage.Business;
using Arbitrage.Models;
using Arbitrage.Models.Exchange;
using Arbitrage.Planner.Graph;
using Arbitrage.Planner.Tree;
using Microsoft.Extensions.Configuration;

namespace Arbitrage.Console
{
	internal class Program
	{
		private static async Task Main()
		{
			System.Console.OutputEncoding = Encoding.UTF8;

			var builder = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json");

			var configuration = builder.Build();

			using (var supportedExchangeCollection = new SupportedExchangesCollection(configuration))
			{
				var exchangeList = supportedExchangeCollection.ToList();
				var exchnageCoins = await new ExchangeCoinsCollection(exchangeList).ToList();

				System.Console.WriteLine($"Exchange coins: {exchnageCoins.Count}");

				var tradingSet = new TradingSet(exchnageCoins, exchangeList, 4);
				var tradingStartPointsFilter = new TradingStartPoints(new List<Coin> { exchnageCoins.First(c => c.Code == "ETH") });

				var tradingGraph = new TradingGraph(tradingSet, tradingStartPointsFilter, new CancellationTokenSource());
				var prevTradingGraphHashCode = string.Empty;
				IList<TradingTree> planTreeList = null;

				do
				{
					try
					{
						System.Console.WriteLine("Start");

						foreach (var exchange in exchangeList)
						{
							exchange.Clear();
						}

						var tradingGraphHashCode = await tradingGraph.ToHashCode(false);
						if (prevTradingGraphHashCode != tradingGraphHashCode)
						{
							planTreeList = await tradingGraph.ToTreeList();
							System.Console.WriteLine($"Trading Tree calculated: {planTreeList.Count}");
						}

						if (planTreeList != null && planTreeList.Any())
						{
							var plans = await planTreeList[0].ToGoodPlansList(0.03m);

							System.Console.WriteLine($"Potentials plans calculated: { plans.Count}: {DateTime.Now}");

							var planCount = 1;
							foreach (var plan in plans)
							{
								System.Console.WriteLine($"{planCount}: {plan.ProfitRatio}:::{plan}");
								planCount++;
							}
						}

						System.Console.WriteLine("Finish");
					}
					catch (Exception e)
					{
						System.Console.WriteLine(e);
					}

				} while (System.Console.ReadKey().Key != ConsoleKey.Enter);
			}
		}
	}
}
