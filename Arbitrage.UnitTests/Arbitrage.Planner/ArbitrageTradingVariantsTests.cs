using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Arbitrage.Models;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Wallet;
using Arbitrage.Planner.Graph;
using Arbitrage.UnitTests.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Arbitrage.UnitTests.Arbitrage.Planner
{
    [TestClass]
    public class ArbitrageTradingVariantsTests
    {
	    [TestMethod]
	    public async Task TwoCoins_TwoExchanges_OneWallet_Generates72VarinatsWith10MaxDepth_WhenStartFromWallet()
	    {
		    // Assign
		    var expectedVariants = 72;
		    var expectedDepth = 10;

		    var coinsList = new FakeCoinsCollection(2).ToList();
		    var exchangesList = new List<IExchange>(new FakeExchangeCollection(2, coinsList).ToList());
		    var wallets = new List<IWallet> {new FakeWallet(coinsList[0], 10)};

		    var tradingSet = new TradingSet(coinsList, exchangesList, wallets);
		    var tradingStartPointsFilter = new TradingStartPoints(wallets);

			// Act

			var walletPathList = await new TradingGraph(tradingSet, tradingStartPointsFilter, new CancellationTokenSource()).ToTreeList();
		    var treeDetails = walletPathList[0].Details();
			var actualVariants = await treeDetails.Variants();
		    var actualDepth = treeDetails.Depth();

			// Assert

			Assert.AreEqual(expectedVariants, actualVariants);
		    Assert.AreEqual(expectedDepth, actualDepth);
	    }

	    [TestMethod]
	    public async Task ThreeCoins_TwoExchanges_Generates122VarinatsWith10MaxDepth_WhenStartFromCoin1()
	    {
		    // Assign
		    var expectedVariants = 122;
		    var expectedDepth = 10;

		    var coinsList = new FakeCoinsCollection(3).ToList();
			var exchangesList = new List<IExchange>(new FakeExchangeCollection(2, coinsList).ToList());

			var tradingSet = new TradingSet(coinsList, exchangesList);
		    var tradingStartPointsFilter = new TradingStartPoints(new List<Coin> { coinsList[0] });

		    // Act

		    var walletPathList = await new TradingGraph(tradingSet, tradingStartPointsFilter, new CancellationTokenSource()).ToTreeList();
		    var treeDetails = walletPathList[0].Details();
		    var actualVariants = await treeDetails.Variants();
		    var actualDepth = treeDetails.Depth();

		    // Assert

		    Assert.AreEqual(expectedVariants, actualVariants);
		    Assert.AreEqual(expectedDepth, actualDepth);
	    }

		[TestMethod]
	    public async Task ThreeCoins_OneExchange_OneWallet_Generates15VarinatsWith8MaxDepth_WhenStartFromWallet()
	    {
		    // Assign
		    var expectedVariants = 15;
		    var expectedDepth = 8;

		    var coinsList = new FakeCoinsCollection(3).ToList();
		    var exchangesList = new List<IExchange>(new FakeExchangeCollection(1, coinsList).ToList());

			var wallets = new List<IWallet> { new FakeWallet(coinsList[0], 10) };

		    var tradingSet = new TradingSet(coinsList, exchangesList, wallets);
		    var tradingStartPointsFilter = new TradingStartPoints(wallets);

		    // Act

		    var walletPathList = await new TradingGraph(tradingSet, tradingStartPointsFilter, new CancellationTokenSource()).ToTreeList();
		    var treeDetails = walletPathList[0].Details();
			var actualVariants = await treeDetails.Variants();
			var actualDepth = treeDetails.Depth();

		    // Assert

		    Assert.AreEqual(expectedVariants, actualVariants);
		    Assert.AreEqual(expectedDepth, actualDepth);
	    }

	    [TestMethod]
	    public async Task ThreeCoins_OneExchange_OneWallet_Generates46VarinatsWith8MaxDepth_WhenStartFromAllPossibleNodes()
	    {
		    // Assign
		    var expectedVariants = 46;
		    var expectedMaxDepth = 8;

		    var coinsList = new FakeCoinsCollection(3).ToList();
		    var exchangesList = new List<IExchange>(new FakeExchangeCollection(1, coinsList).ToList());

			var wallets = new List<IWallet> { new FakeWallet(coinsList[0], 10) };

		    var tradingSet = new TradingSet(coinsList, exchangesList, wallets);
		    var tradingStartPointsFilter = new TradingStartPoints();

		    // Act

		    var treeList = await new TradingGraph(tradingSet, tradingStartPointsFilter, new CancellationTokenSource()).ToTreeList();

			int actualVariants = 0;
		    int actualMaxDepth = 0;
			foreach (var tradingTree in treeList)
		    {
			    var treeDetails = tradingTree.Details();
				actualVariants += await treeDetails.Variants();
			    var actualDepth = treeDetails.Depth();
			    if (actualDepth > actualMaxDepth)
			    {
				    actualMaxDepth = actualDepth;
			    }
			}

		    // Assert

		    Assert.AreEqual(expectedVariants, actualVariants);
		    Assert.AreEqual(expectedMaxDepth, actualMaxDepth);
	    }

	    [TestMethod]
	    public async Task ThreeCoins_TwoExchange_OneWallet_Generates137000VarinatsWith20MaxDepth_WhenStartFromWallet()
	    {
		    // Assign
		    var expectedVariants = 137002;
		    var expectedMaxDepth = 20;

		    var coinsList = new FakeCoinsCollection(3).ToList();
		    var exchangesList = new List<IExchange>(new FakeExchangeCollection(2, coinsList).ToList());

			var wallets = new List<IWallet> { new FakeWallet(coinsList[0], 10) };

		    var tradingSet = new TradingSet(coinsList, exchangesList, wallets);
		    var tradingStartPointsFilter = new TradingStartPoints(wallets);

		    // Act

		    var walletPathList = await new TradingGraph(tradingSet, tradingStartPointsFilter, new CancellationTokenSource()).ToTreeList();

			int actualVariants = 0;
		    int actualMaxDepth = 0;
		    foreach (var tradingTree in walletPathList)
		    {
			    var treeDetails = tradingTree.Details();
				actualVariants += await treeDetails.Variants();
			    var actualDepth = treeDetails.Depth();
			    if (actualDepth > actualMaxDepth)
			    {
				    actualMaxDepth = actualDepth;
			    }
		    }

		    // Assert

		    Assert.AreEqual(expectedVariants, actualVariants);
		    Assert.AreEqual(expectedMaxDepth, actualMaxDepth);
	    }

		[TestMethod]
	    public async Task ThreeCoins_TwoExchange_OneWallet_Generates144042VarinatsWith20MaxDepth_WhenStartFromAllPossibleNodes()
	    {
		    // Assign
		    var expectedVariants = 144042;
		    var expectedMaxDepth = 20;

		    var coinsList = new FakeCoinsCollection(3).ToList();
		    var exchangesList = new List<IExchange>(new FakeExchangeCollection(2, coinsList).ToList());

			var wallets = new List<IWallet> { new FakeWallet(coinsList[0], 10) };

		    var tradingSet = new TradingSet(coinsList, exchangesList, wallets);
		    var tradingStartPointsFilter = new TradingStartPoints();

		    // Act

		    var treeList = await new TradingGraph(tradingSet, tradingStartPointsFilter, new CancellationTokenSource()).ToTreeList();

			int actualVariants = 0;
		    int actualMaxDepth = 0;
		    foreach (var tradingTree in treeList)
		    {
			    var treeDetails = tradingTree.Details();
				actualVariants += await treeDetails.Variants();
			    var actualDepth = treeDetails.Depth();
			    if (actualDepth > actualMaxDepth)
			    {
				    actualMaxDepth = actualDepth;
			    }
		    }

		    // Assert

		    Assert.AreEqual(expectedVariants, actualVariants);
		    Assert.AreEqual(expectedMaxDepth, actualMaxDepth);
	    }

		[TestMethod]
	    public async Task ThreeCoins_TwoExchange_OneWallet_Generates490VarinatsWith6MaxDepth_WhenStartFromAllPossibleNodesAndDepthLimitedTo6()
	    {
		    // Assign
		    var expectedVariants = 490;
		    var expectedMaxDepth = 6;

		    var coinsList = new FakeCoinsCollection(3).ToList();
		    var exchangesList = new List<IExchange>(new FakeExchangeCollection(2, coinsList).ToList());

			var wallets = new List<IWallet> { new FakeWallet(coinsList[0], 10) };

		    var tradingSet = new TradingSet(coinsList, exchangesList, wallets, 6);
		    var tradingStartPointsFilter = new TradingStartPoints();

		    // Act

		    var treeList = await new TradingGraph(tradingSet, tradingStartPointsFilter, new CancellationTokenSource()).ToTreeList();

			int actualVariants = 0;
		    int actualMaxDepth = 0;
		    foreach (var tradingTree in treeList)
		    {
			    var treeDetails = tradingTree.Details();
				actualVariants += await treeDetails.Variants();
			    var actualDepth = treeDetails.Depth();
			    if (actualDepth > actualMaxDepth)
			    {
				    actualMaxDepth = actualDepth;
			    }
		    }

		    // Assert

		    Assert.AreEqual(expectedVariants, actualVariants);
		    Assert.AreEqual(expectedMaxDepth, actualMaxDepth);
	    }

		//[TestMethod]
	    public async Task OneThousandThreeHundredCoins_NinetyExchanges_OneWallet_Generates6VarinatsWith6MaxDepth_WhenStartFromWalletNode_DepthLimitedTo15()
	    {
		    // Assign
		    var expectedVariants = 674;
		    var expectedMaxDepth = 6;

		    var coinsList = new FakeCoinsCollection(10).ToList();
		    var exchangesList = new List<IExchange>(new FakeExchangeCollection(3, coinsList).ToList());

			var wallets = new List<IWallet> { new FakeWallet(coinsList[0], 10) };

		    var tradingSet = new TradingSet(coinsList, exchangesList, wallets, 8);
		    var tradingStartPointsFilter = new TradingStartPoints(wallets);

		    // Act

		    var walletPathList = await new TradingGraph(tradingSet, tradingStartPointsFilter, new CancellationTokenSource()).ToTreeList();

			int actualVariants = 0;
		    int actualMaxDepth = 0;
		    foreach (var tradingTree in walletPathList)
		    {
			    var treeDetails = tradingTree.Details();
				actualVariants += await treeDetails.Variants();
			    var actualDepth = treeDetails.Depth();
			    if (actualDepth > actualMaxDepth)
			    {
				    actualMaxDepth = actualDepth;
			    }
		    }

		    // Assert

		    Assert.AreEqual(expectedVariants, actualVariants);
		    Assert.AreEqual(expectedMaxDepth, actualMaxDepth);
	    }
	}
}
