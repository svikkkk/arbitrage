using Arbitrage.Models.Wallet;
using Arbitrage.Planner.Exchange;
using Arbitrage.Planner.Graph;
using Arbitrage.Planner.Wallet;
using Arbitrage.UnitTests.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Arbitrage.Models.Exchange.Interfaces;

namespace Arbitrage.UnitTests.Arbitrage.Planner
{
	[TestClass]
	public class ArbitrageTradingGraphHashCodeTests
	{
		[TestMethod]
		public async Task WalletNodeHashCode_For_TwoCoins_TwoExchanges()
		{
			// Assign
			var expectedHashCode = "072FE62C352F191FB449B95ABA0D9668904C6DB6";

			var coinsList = new FakeCoinsCollection(2).ToList();
			var exchangesList = new FakeExchangeCollection(2, coinsList).ToList();
			var wallets = new List<IWallet> { new FakeWallet(coinsList[0], 10) };

			var tradingSet = new TradingSet(coinsList, new List<IExchange>(exchangesList), wallets);
			var tradingStartPointsFilter = new TradingStartPoints(wallets);

			// Act

			var graphNodes = await new TradingGraph(tradingSet, tradingStartPointsFilter, new CancellationTokenSource()).ToNodesList();
			var walletNode = graphNodes.First(n => n is WalletNode);
			var actualHashCode = walletNode.ToHashCode();

			// Assert

			Assert.AreEqual(expectedHashCode, actualHashCode);
		}

		[TestMethod]
		public async Task ExchangeNodeHashCode_For_3Coins_TwoExchanges()
		{
			// Assign
			var expectedHashCode = "E5572105A6BE779E57052D4B4E6AE8CF1205040B";

			var coinsList = new FakeCoinsCollection(3).ToList();
			var exchangesList = new List<IExchange>(new FakeExchangeCollection(2, coinsList).ToList());
			var wallets = new List<IWallet> { new FakeWallet(coinsList[0], 10) };

			var tradingSet = new TradingSet(coinsList, exchangesList, wallets);
			var tradingStartPointsFilter = new TradingStartPoints(exchangesList);

			// Act

			var graphNodes = await new TradingGraph(tradingSet, tradingStartPointsFilter, new CancellationTokenSource()).ToNodesList();
			var exchangeNode = graphNodes.OfType<ExchangeNode>().First(n => n.Exchange == exchangesList[0]);
			var actualHashCode = exchangeNode.ToHashCode();

			// Assert

			Assert.AreEqual(expectedHashCode, actualHashCode);
		}

		[TestMethod]
		public async Task GraphHashCode_For_OneWallet_TwoCoins_TwoExchanges()
		{
			// Assign
			var expectedHashCode = "08C8A5B1CC49B4691C984C7929A8FA2CD569BD68";

			var coinsList = new FakeCoinsCollection(2).ToList();
			var exchangesList = new List<IExchange>(new FakeExchangeCollection(2, coinsList).ToList());
			var wallets = new List<IWallet> { new FakeWallet(coinsList[0], 10) };

			var tradingSet = new TradingSet(coinsList, exchangesList, wallets);
			var tradingStartPointsFilter = new TradingStartPoints(wallets);

			// Act

			var graph = new TradingGraph(tradingSet, tradingStartPointsFilter, new CancellationTokenSource());
			var actualHashCode = await graph.ToHashCode();

			// Assert

			Assert.AreEqual(expectedHashCode, actualHashCode);
		}
	}
}