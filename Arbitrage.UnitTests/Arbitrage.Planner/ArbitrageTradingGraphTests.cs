﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Wallet;
using Arbitrage.Planner.Graph;
using Arbitrage.UnitTests.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Arbitrage.UnitTests.Arbitrage.Planner
{
	[TestClass]
	class ArbitrageTradingGraphTests
	{
	    [TestMethod]
	    public async Task TwoCoins_TwoExchanges_OneWallet_GraphHashCode()
	    {
		    // Assign
		    var expectedVariants = 72;
		    var expectedDepth = 10;

		    var coinsList = new FakeCoinsCollection(2).ToList();
		    var exchangesList = new List<IExchange>(new FakeExchangeCollection(2, coinsList).ToList());

			var wallets = new List<IWallet> { new FakeWallet(coinsList[0], 10) };

		    var tradingSet = new TradingSet(coinsList, exchangesList, wallets);
		    var tradingStartPointsFilter = new TradingStartPoints(wallets);

		    // Act

		    var walletPathList = await new TradingGraph(tradingSet, tradingStartPointsFilter, new CancellationTokenSource()).ToTreeList();
		    var treeDetails = walletPathList[0].Details();
			var actualVariants = treeDetails.Variants();
		    var actualDepth = treeDetails.Depth();

		    // Assert

		    Assert.AreEqual(expectedVariants, actualVariants);
		    Assert.AreEqual(expectedDepth, actualDepth);
	    }
	}
}
