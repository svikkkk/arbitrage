﻿using Arbitrage.Models;
using Arbitrage.Models.Exchange;
using Arbitrage.Models.Exchange.Concretes;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Orders;
using Arbitrage.Planner.Graph;
using Arbitrage.UnitTests.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Arbitrage.UnitTests.Arbitrage.Planner
{
	[TestClass]
	public class ArbitrageTradingPlanBalancerTests
	{
		/// <summary>
		/// Sell orders on Exchange0
		/// FeeFactor = 1 (0%)
		/// Price [Code1]|	Volume [Code0]	|	Total (Price * Volume * FeeFactor) [Code1]
		/// 55			|	1				|	55
		/// 54			|	1				|	54
		/// 53			|	1				|	53
		/// 52			|	1				|	52
		/// 51			|	1				|	51
		/// 
		/// Buy orders on Exchange0
		/// FeeFactor = 1 (0%)
		/// Price [Code1]|	Volume [Code0]	|	Total (Price * Volume * FeeFactor) [Code1]
		/// 49			|	1				|	49
		/// 48			|	1				|	48
		/// 47			|	1				|	47
		/// 46			|	1				|	46
		/// 45			|	1				|	45
		/// 
		/// Sell orders on Exchange1
		/// FeeFactor = 1 (0%)
		/// Price [Code1]|	Volume [Code0]	|	Total (Price * Volume * FeeFactor) [Code1]
		/// 45			|	1				|	45
		/// 44			|	1				|	44
		/// 43			|	1				|	43
		/// 42			|	1				|	42
		/// 41			|	1				|	41
		/// 
		/// Buy orders on Exchange1
		/// FeeFactor = 1 (0%)
		/// Price [Code1]|	Volume [Code0]	|	Total (Price * Volume * FeeFactor) [Code1]
		/// 39			|	1				|	39
		/// 38			|	1				|	38
		/// 37			|	1				|	37
		/// 36			|	1				|	36
		/// 35			|	1				|	35
		/// </summary>
		[TestMethod]
		public async Task TwoCoins_TwoExchanges_OneWallet_PlanFactors()
		{
			// Assign

			var coinsList = new FakeCoinsCollection(2).ToList();

			var exchangesList = new[]
			{
				new Models.Exchange.Concretes.Exchange("Exchange0",
					new FakeExchangeClient(new MarketsCollection(BothMarkets(coinsList[0], coinsList[1], 50m)), 0m)),
				new Models.Exchange.Concretes.Exchange("Exchange1",
					new FakeExchangeClient(new MarketsCollection(BothMarkets(coinsList[0], coinsList[1], 40m)), 0m)),
			};

			var tradingSet = new TradingSet(coinsList, exchangesList);
			var tradingStartPointsFilter = new TradingStartPoints(new List<Coin> { coinsList[0] }, exchangesList);

			// Act

			var walletPathList = await new TradingGraph(tradingSet, tradingStartPointsFilter, new CancellationTokenSource()).ToTreeList();
			var plans = await walletPathList[0].ToGoodPlansList(10);

			// Assert

			Assert.AreEqual(1, plans.Count);
			Assert.AreEqual(3, plans[0].Steps.Count);
			Assert.AreEqual(4m, plans[0].StartAmount);
			Assert.AreEqual(1.1111111111111111111111111111m, plans[0].ProfitRatio);
		}

		/// <summary>
		/// Sell orders on Exchange0
		/// FeeFactor = 1.01 (1%)
		/// Price [Code1]|	Volume [Code0]	|	Total (Price * Volume * FeeFactor) [Code1]
		/// 55			|	1				|	55.55
		/// 54			|	1				|	54.54
		/// 53			|	1				|	53.53
		/// 52			|	1				|	52.52
		/// 51			|	1				|	51.51
		/// 
		/// Buy orders on Exchange0
		/// FeeFactor = 0.99 (1%)
		/// Price [Code1]|	Volume [Code0]	|	Total (Price * Volume * FeeFactor) [Code1]
		/// 49			|	1				|	48.51
		/// 48			|	1				|	47.52
		/// 47			|	1				|	46.53
		/// 46			|	1				|	45.54
		/// 45			|	1				|	44.55
		/// 
		/// Sell orders on Exchange1
		/// FeeFactor = 1.01 (1%)
		/// Price [Code1]|	Volume [Code0]	|	Total (Price * Volume * FeeFactor) [Code1]
		/// 45			|	1				|	45.45
		/// 44			|	1				|	44.44
		/// 43			|	1				|	43.43
		/// 42			|	1				|	42.42
		/// 41			|	1				|	41.41
		/// 
		/// Buy orders on Exchange1
		/// FeeFactor = 0.99 (1%)
		/// Price [Code1]|	Volume [Code0]	|	Total (Price * Volume * FeeFactor) [Code1]
		/// 39			|	1				|	38.61
		/// 38			|	1				|	37.62
		/// 37			|	1				|	36.63
		/// 36			|	1				|	35.64
		/// 35			|	1				|	34.65
		/// </summary>
		[TestMethod]
		public async Task TwoCoins_TwoExchanges_OneWallet_WithTradingFee_PlanFactors()
		{
			// Assign

			var tradingFeePercentage = 1m;

			var coinsList = new FakeCoinsCollection(2).ToList();

			var exchangesList = new[]
			{
				new Models.Exchange.Concretes.Exchange("Exchange0",
					new FakeExchangeClient(new MarketsCollection(BothMarkets(coinsList[0], coinsList[1], 50m, 1m, tradingFeePercentage)), 0m)),
				new Models.Exchange.Concretes.Exchange("Exchange1",
					new FakeExchangeClient(new MarketsCollection(BothMarkets(coinsList[0], coinsList[1], 40m, 1m, tradingFeePercentage)), 0m)),
			};

			var tradingSet = new TradingSet(coinsList, exchangesList);
			var tradingStartPointsFilter = new TradingStartPoints(new List<Coin> { coinsList[0] }, exchangesList);

			// Act

			var walletPathList = await new TradingGraph(tradingSet, tradingStartPointsFilter, new CancellationTokenSource()).ToTreeList();
			var plans = await walletPathList[0].ToGoodPlansList(10);

			// Assert

			Assert.AreEqual(1, plans.Count);
			Assert.AreEqual(3, plans[0].Steps.Count);
			Assert.AreEqual(4, plans[0].StartAmount);
			Assert.AreEqual(1.0902090209020902090209020902m, plans[0].ProfitRatio);
		}

		/// <summary>
		/// Sell orders on buyMarket of Exchange0
		/// FeeFactor = 1.01 (1%)
		/// Price [Code1]|	Volume [Code0]	|	Total (Price * Volume * FeeFactor) [Code1]
		/// 45			|	1				|	45.45
		/// 44			|	1				|	44.44
		/// 43			|	1				|	43.43
		/// 42			|	1				|	42.42
		/// 41			|	1				|	41.41
		/// 
		/// Buy orders on sellMarket of Exchange0
		/// FeeFactor = 0.99 (1%)
		/// Price [Code1]|	Volume [Code0]	|	Total (Price * Volume * FeeFactor) [Code1]
		/// 49			|	1				|	48.51
		/// 48			|	1				|	47.52
		/// 47			|	1				|	46.53
		/// 46			|	1				|	45.54
		/// 45			|	1				|	44.55
		/// </summary>
		[TestMethod]
		public async Task TwoCoins_OneExchange_OneWallet_WithTradingFee_PlanFactors()
		{
			// Assign

			var tradingFeePercentage = 1m;

			var coinsList = new FakeCoinsCollection(2).ToList();

			var sellMarket = SellMarket(coinsList[0], coinsList[1], 50m, 1m, tradingFeePercentage);
			var buyMarket = BuyMarket(coinsList[0], coinsList[1], 40m, 1m, tradingFeePercentage);

			var exchangesList = new[]
			{
				new Models.Exchange.Concretes.Exchange("Exchange0",
					new FakeExchangeClient(new MarketsCollection( new IMarket[] { sellMarket, buyMarket }), 0m))
			};

			var tradingSet = new TradingSet(coinsList, exchangesList);
			var tradingStartPointsFilter = new TradingStartPoints(new List<Coin> { coinsList[0] }, exchangesList);

			// Act

			var walletPathList = await new TradingGraph(tradingSet, tradingStartPointsFilter, new CancellationTokenSource()).ToTreeList();
			var plans = await walletPathList[0].ToGoodPlansList(10);

			// Assert

			Assert.AreEqual(1, plans.Count);
			Assert.AreEqual(2, plans[0].Steps.Count);
			Assert.AreEqual(4, plans[0].StartAmount);
			Assert.AreEqual(1.0902090209020902090209020902m, plans[0].ProfitRatio);
		}

		private IMarket[] BothMarkets(Coin baseCoin, Coin quoteCoin, decimal lastTradePrice, decimal priceStep = 1m, decimal tradeFeeInPercentage = 0)
		{
			var sellBuyMarket = SellMarket(baseCoin, quoteCoin, lastTradePrice, priceStep, tradeFeeInPercentage);
			var buySellMarket = BuyMarket(baseCoin, quoteCoin, lastTradePrice, priceStep, tradeFeeInPercentage);

			return new IMarket[] { sellBuyMarket, buySellMarket };
		}

		private IMarket SellMarket(Coin baseCoin, Coin quoteCoin, decimal lastTradePrice, decimal priceStep = 1m, decimal tradeFeeInPercentage = 0)
		{
			var minimumBaseCoinAmount = 0m;
			var minimumQuoteCoinAmount = 0m;

			var tradeFeeFactor = 1m - tradeFeeInPercentage / 100m;
			
			var sellBuyMarket = new FakeMarket
			(
				tradePair: new TradePair
				(
					coinToSell: baseCoin,
					coinToBuy: quoteCoin,
					minimumCoinToSellAmount: minimumBaseCoinAmount,
					minimumCoinToBuyAmount: minimumQuoteCoinAmount
				),
				orders: new Order[]
				{
					new BuyOrder(lastTradePrice - priceStep * 1, 1, baseCoin, quoteCoin, tradeFeeFactor),
					new BuyOrder(lastTradePrice - priceStep * 2, 1, baseCoin, quoteCoin, tradeFeeFactor),
					new BuyOrder(lastTradePrice - priceStep * 3, 1, baseCoin, quoteCoin, tradeFeeFactor),
					new BuyOrder(lastTradePrice - priceStep * 4, 1, baseCoin, quoteCoin, tradeFeeFactor),
					new BuyOrder(lastTradePrice - priceStep * 5, 1, baseCoin, quoteCoin, tradeFeeFactor),
				},
				approximateOrder: new BuyOrder(lastTradePrice, decimal.One, baseCoin, quoteCoin, tradeFeeFactor)
			);

			return sellBuyMarket;
		}

		private IMarket BuyMarket(Coin baseCoin, Coin quoteCoin, decimal lastTradePrice, decimal priceStep = 1m, decimal tradeFeeInPercentage = 0)
		{
			var minimumBaseCoinAmount = 0m;
			var minimumQuoteCoinAmount = 0m;

			var tradeFeeFactor = 1m + tradeFeeInPercentage / 100m;

			var buySellMarket = new FakeMarket
			(
				tradePair: new TradePair
				(
					coinToBuy: baseCoin,
					coinToSell: quoteCoin,
					minimumCoinToBuyAmount: minimumBaseCoinAmount,
					minimumCoinToSellAmount: minimumQuoteCoinAmount
				),
				orders: new Order[]
				{
					new SellOrder(lastTradePrice + priceStep * 1, 1, quoteCoin, baseCoin, tradeFeeFactor),
					new SellOrder(lastTradePrice + priceStep * 2, 1, quoteCoin, baseCoin, tradeFeeFactor),
					new SellOrder(lastTradePrice + priceStep * 3, 1, quoteCoin, baseCoin, tradeFeeFactor),
					new SellOrder(lastTradePrice + priceStep * 4, 1, quoteCoin, baseCoin, tradeFeeFactor),
					new SellOrder(lastTradePrice + priceStep * 5, 1, quoteCoin, baseCoin, tradeFeeFactor),
				},
				approximateOrder: new SellOrder(lastTradePrice, decimal.One, baseCoin, quoteCoin, tradeFeeFactor)
			);
			
			return buySellMarket;
		}
	}
}
