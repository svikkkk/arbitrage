﻿using Arbitrage.Models;
using Arbitrage.Planner.Graph;
using Arbitrage.UnitTests.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Orders;

namespace Arbitrage.UnitTests.Arbitrage.Planner
{
	[TestClass]
	public class ArbitrageTradingPlanTests
	{
		[TestMethod]
		public async Task TwoCoins_TwoExchanges_OneWallet_PlanExecution()
		{
			// Assign
			Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

			var expectedPlan1 = "FakeExchange0 Code0 267.09403 → Code1 534.18806" + Environment.NewLine +
								"FakeExchange0 Code1 534.18806 → Code3 854.70090" + Environment.NewLine +
								"FakeExchange0 Code3 854.70090 → Code1 666.66670" + Environment.NewLine +
								"FakeExchange0 Code1 666.66670 → Code2 1000.00005" + Environment.NewLine +
								"FakeExchange0 Code2 999.99997 → Code1 749.99998" + Environment.NewLine +
								"FakeExchange0 Code1 749.99998 → Code0 367.49999" + Environment.NewLine;

			var expectedPlan2 = "FakeExchange0 Code0 277.77779 → Code1 555.55558" + Environment.NewLine +
								"FakeExchange0 Code1 555.55558 → Code2 833.33337" + Environment.NewLine +
								"FakeExchange0 Code2 833.33337 → Code1 625.00003" + Environment.NewLine +
								"FakeExchange0 Code1 625.00003 → Code3 1000.00004" + Environment.NewLine +
								"FakeExchange0 Code3 999.99997 → Code1 779.99998" + Environment.NewLine +
								"FakeExchange0 Code1 779.99998 → Code0 382.19999" + Environment.NewLine;

			var expectedPlan3 = "FakeExchange0 Code0 312.50002 → Code1 625.00004" + Environment.NewLine +
								"FakeExchange0 Code1 625.00004 → Code3 1000.00006" + Environment.NewLine +
								"FakeExchange0 Code3 1000.00000 → Code1 780.00000" + Environment.NewLine +
								"FakeExchange0 Code1 780.00000 → Code0 382.20000" + Environment.NewLine;

			var expectedPlan4 = "FakeExchange0 Code0 333.33335 → Code1 666.66670" + Environment.NewLine +
								"FakeExchange0 Code1 666.66670 → Code2 1000.00005" + Environment.NewLine +
								"FakeExchange0 Code2 999.99999 → Code1 749.99999" + Environment.NewLine +
								"FakeExchange0 Code1 749.99999 → Code0 367.50000" + Environment.NewLine;

			var coinsList = new FakeCoinsCollection(4).ToList();
			var exchangesList = new List<IExchange>(new FakeExchangeCollection(1, coinsList, Markets(coinsList)).ToList());

			var tradingSet = new TradingSet(coinsList, exchangesList);
			var tradingStartPointsFilter = new TradingStartPoints(new List<Coin> { coinsList[0] }, exchangesList);

			// Act

			var walletPathList = await new TradingGraph(tradingSet, tradingStartPointsFilter, new CancellationTokenSource()).ToTreeList();
			var plans = await walletPathList[0].ToGoodPlansList(10);

			var plansExecution = new List<string>(plans.Count);
			foreach (var plan in plans)
			{
				plansExecution.Add(plan.ToString());
			}

			plansExecution.Sort();

			// Assert

			Assert.AreEqual(4, plans.Count);
			Assert.AreEqual(expectedPlan1, plansExecution[0], GetFirstDiffIndex(expectedPlan1, plansExecution[0]).ToString());
			Assert.AreEqual(expectedPlan2, plansExecution[1]);
			Assert.AreEqual(expectedPlan3, plansExecution[2]);
			Assert.AreEqual(expectedPlan4, plansExecution[3]);
		}

		[TestMethod]
		public async Task TwoCoins_TwoExchanges_OneWallet_PlanFactors()
		{
			// Assign

			var expectedPlan1 = (decimal)1.10250;
			var expectedPlan2 = (decimal)1.22304;
			var expectedPlan3 = (decimal)1.37592000;
			var expectedPlan4 = (decimal)1.37592000;


			var coinsList = new FakeCoinsCollection(4).ToList();
			var exchangesList = new List<IExchange>(new FakeExchangeCollection(1, coinsList, Markets(coinsList)).ToList());

			var tradingSet = new TradingSet(coinsList, exchangesList);
			var tradingStartPointsFilter = new TradingStartPoints(new List<Coin> { coinsList[0] }, exchangesList);

			// Act

			var walletPathList = await new TradingGraph(tradingSet, tradingStartPointsFilter, new CancellationTokenSource()).ToTreeList();
			var plans = await walletPathList[0].ToGoodPlansList(10);

			var planFactors = new List<decimal>(plans.Count);
			foreach (var plan in plans)
			{
				planFactors.Add(plan.ProfitRatio);
			}

			planFactors.Sort();

			// Assert

			Assert.AreEqual(4, plans.Count);
			Assert.AreEqual((double)expectedPlan1, (double)planFactors[0], 0.000001);
			Assert.AreEqual((double)expectedPlan2, (double)planFactors[1], 0.000001);
			Assert.AreEqual((double)expectedPlan3, (double)planFactors[2], 0.000001);
			Assert.AreEqual((double)expectedPlan4, (double)planFactors[3], 0.000001);
		}

		private IList<FakeMarket> Markets(IList<Coin> coins)
		{
			return new[]
			{
				new FakeMarket(new BuyOrder(2m, 1000m, coins[0], coins[1], 1)),
				new FakeMarket(new SellOrder(1m / 0.49m, 490m, coins[1], coins[0], 1)),
				new FakeMarket(new BuyOrder(1.5m, 1000m, coins[1], coins[2], 1)),
				new FakeMarket(new SellOrder(1m / 0.75m, 750m, coins[2], coins[1], 1)),
				new FakeMarket(new BuyOrder(1.6m, 1000m, coins[1], coins[3], 1)),
				new FakeMarket(new SellOrder(1m / 0.78m, 780m, coins[3], coins[1], 1))
			};
		}

		public static int GetFirstDiffIndex(string str1, string str2)
		{
			if (str1 == null || str2 == null) return -1;

			int length = Math.Min(str1.Length, str2.Length);

			for (int index = 0; index < length; index++)
			{
				if (str1[index] != str2[index])
				{
					return index;
				}
			}

			return -1;
		}
	}
}
