using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Arbitrage.Models;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Wallet;
using Arbitrage.Planner.Costs;
using Arbitrage.Planner.Exchange;
using Arbitrage.Planner.Graph;
using Arbitrage.Planner.Tree;
using Arbitrage.Planner.Wallet;
using Arbitrage.UnitTests.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Arbitrage.UnitTests.Arbitrage.Planner
{
//	[TestClass]
	public class ArbitrageTreeSerializationTests
	{
		private string _treeInJsonFormat = "{\"Id\":30903363,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":42715336,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":25474675,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":42715336,\"Cldrn\":[{\"Id\":25474675,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]}]}]}]},{\"Id\":12036987,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":25474675,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":25474675,\"Cldrn\":[{\"Id\":42715336,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]}]}]},{\"Id\":12036987,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":25474675,\"Cldrn\":[{\"Id\":42715336,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]}]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":42715336,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":25474675,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]}]},{\"Id\":12036987,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]}]}]}]},{\"Id\":36963566,\"Cldrn\":[{\"Id\":25474675,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":42715336,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":25474675,\"Cldrn\":[{\"Id\":42715336,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]}]}]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":42715336,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":42715336,\"Cldrn\":[{\"Id\":25474675,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]}]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":25474675,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":42715336,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]}]}]}]},{\"Id\":12036987,\"Cldrn\":[{\"Id\":42715336,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":25474675,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":25474675,\"Cldrn\":[{\"Id\":42715336,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]}]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":42715336,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]}]}]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":36963566,\"Cldrn\":[{\"Id\":25474675,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":42715336,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":25474675,\"Cldrn\":[{\"Id\":42715336,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]}]}]}]},{\"Id\":36963566,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":42715336,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":42715336,\"Cldrn\":[{\"Id\":25474675,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]}]}]},{\"Id\":36963566,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":42715336,\"Cldrn\":[{\"Id\":25474675,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]}]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":25474675,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":42715336,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]}]},{\"Id\":30903363,\"Cldrn\":[]},{\"Id\":36963566,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]}]},{\"Id\":30903363,\"Cldrn\":[]}]}]}]}]},{\"Id\":12036987,\"Cldrn\":[{\"Id\":42715336,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":25474675,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":42715336,\"Cldrn\":[{\"Id\":25474675,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]}]}]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":25474675,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":25474675,\"Cldrn\":[{\"Id\":42715336,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]}]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":42715336,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":25474675,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]}]}]}]},{\"Id\":36963566,\"Cldrn\":[{\"Id\":25474675,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":42715336,\"Cldrn\":[{\"Id\":12036987,\"Cldrn\":[{\"Id\":42715336,\"Cldrn\":[{\"Id\":25474675,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]}]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":25474675,\"Cldrn\":[{\"Id\":36963566,\"Cldrn\":[{\"Id\":30903363,\"Cldrn\":[]}]}]}]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]},{\"Id\":30903363,\"Cldrn\":[]}]}]}";

		[TestMethod]
		public async Task TwoCoins_TwoExchanges_OneWallet_Tree_SerializeToJsonString()
		{
			// Assign

			var coinsList = new FakeCoinsCollection(2).ToList();
			var exchangesList = new List<IExchange>(new FakeExchangeCollection(2, coinsList).ToList());

			var wallets = new List<IWallet> { new FakeWallet(coinsList[0], 10) };

			var tradingSet = new TradingSet(coinsList, exchangesList, wallets);
			var tradingStartPointsFilter = new TradingStartPoints(wallets);

			// Act

			var walletPathList = await new TradingGraph(tradingSet, tradingStartPointsFilter, new CancellationTokenSource()).ToTreeList();
			var tree = walletPathList[0];

			var treeJson = tree.ToJson();
			
			// Assert

			Assert.IsNotNull(treeJson);
		}

		[TestMethod]
		public async Task TwoCoins_TwoExchanges_OneWalletTree_DeserializeFromJsonString_72Variants_10Depth()
		{
			// Assign
			var expectedVariants = 72;
			var expectedDepth = 10;

			string jsonString = _treeInJsonFormat;

			var coinsList = new FakeCoinsCollection(2).ToList();
			var exchangesList = new FakeExchangeCollection(2, coinsList).ToList();
			var wallet = new FakeWallet(coinsList[0], 10);
			var tradingGraph = new TradingGraph(
				new TradingSet(new List<Coin>(coinsList), new List<IExchange>(exchangesList), new List<IWallet>{ wallet }),
				new TradingStartPoints(new List<IWallet> { wallet }), new CancellationTokenSource());

			// Act

			var tree = new TradingTree(jsonString, tradingGraph);
			var treeDetails = tree.Details();
			var variants = await treeDetails.Variants();
			var depth = treeDetails.Depth();
			var rootNode = await tradingGraph.Node(tree.RootNode.DataNodeId);

			// Assert

			Assert.AreEqual(expectedVariants, variants);
			Assert.AreEqual(expectedDepth, depth);
			Assert.IsTrue(rootNode.Value is WalletNodeValue);
		}

		[TestMethod]
		public async Task TwoCoins_TwoExchanges_OneWalletTree_DeserializeFromJsonString_72Transfers_10Deposits_10Withdraws_6MarketTrades()
		{
			// Assign
			var expectedTransfers = 60;
			var expectedDeposits = 2;
			var expectedWithdraws = 72;
			var expectedMarkets = 60;

			string jsonString = _treeInJsonFormat;

			var coinsList = new FakeCoinsCollection(2).ToList();
			var exchangesList = new FakeExchangeCollection(2, coinsList).ToList();
			var wallet = new FakeWallet(coinsList[0], 10);
			var tradingGraph = new TradingGraph(
				new TradingSet(new List<Coin>(coinsList), new List<IExchange>(exchangesList), new List<IWallet> { wallet }),
				new TradingStartPoints(new List<IWallet> { wallet }), new CancellationTokenSource());

			// Act

			var tree = new TradingTree(jsonString, tradingGraph);
			var treeDetails = tree.Details();
			var transfers = await treeDetails.Costs<TransferCost>();
			var deposits = await treeDetails.Costs<DepositCost>();
			var withdraws = await treeDetails.Costs<WithdrawCost>();
			var markets = await treeDetails.Costs<MarketCost>();

			// Assert

			Assert.AreEqual(expectedTransfers, transfers);
			Assert.AreEqual(expectedDeposits, deposits);
			Assert.AreEqual(expectedWithdraws, withdraws);
			Assert.AreEqual(expectedMarkets, markets);
		}

		[TestMethod]
		public async Task TwoCoins_TwoExchanges_OneWalletTree_DeserializeFromJsonString_72Exchanges_1Wallet()
		{
			// Assign
			var expectedWallets = 73;
			var expectedExchanges = 122;

			string jsonString = _treeInJsonFormat;

			var coinsList = new FakeCoinsCollection(2).ToList();
			var exchangesList = new FakeExchangeCollection(2, coinsList).ToList();
			var wallet = new FakeWallet(coinsList[0], 10);
			var tradingGraph = new TradingGraph(
				new TradingSet(new List<Coin>(coinsList), new List<IExchange>(exchangesList), new List<IWallet> { wallet }),
				new TradingStartPoints(new List<IWallet> { wallet }), new CancellationTokenSource());

			// Act

			var tree = new TradingTree(jsonString, tradingGraph);
			var treeDetails = tree.Details();

			var wallets = await treeDetails.DataNodes<WalletNodeValue>();
			var exchanges = await treeDetails.DataNodes<ExchangeNodeValue>();

			// Assert

			Assert.AreEqual(expectedWallets, wallets);
			Assert.AreEqual(expectedExchanges, exchanges);
		}
	}
}