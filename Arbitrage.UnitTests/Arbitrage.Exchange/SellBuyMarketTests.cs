﻿using Arbitrage.Models;
using Arbitrage.Models.Exchange;
using Arbitrage.Models.Exchange.Concretes;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Orders;
using Arbitrage.UnitTests.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace Arbitrage.UnitTests.Arbitrage.Exchange
{
	[TestClass]
	public class SellBuyMarketTests
	{
		private const decimal TotalOfFirstOrder = 2.16m;
		private const decimal TotalsOfTwoOrders = TotalOfFirstOrder + 1.08m;
		private const decimal VolumeOfFirstOrder = 3m;
		private const decimal VolumesOfTwoOrders = VolumeOfFirstOrder + 2m;

		[TestMethod]
		public async Task ApproximateAvailableAmountToBuy()
		{
			// Assign

			var expectedApproximateAvailableAmountToBuy = TotalOfFirstOrder;

			var sellAmount = VolumeOfFirstOrder;

			var sellBuyMarket = SellBuyMarket();

			// Act

			var approximateAvailableAmountToBuy = await sellBuyMarket.Rate(MarketRate.Approximate).AvailableAmountToBuy(sellAmount);

			// Assert

			Assert.AreEqual(expectedApproximateAvailableAmountToBuy, approximateAvailableAmountToBuy);
		}

		[TestMethod]
		public async Task ApproximateRequiredAmountToSell()
		{
			// Assign

			var expectedApproximateRequiredAmountToSell = VolumeOfFirstOrder;

			var buyAmount = TotalOfFirstOrder;

			var sellBuyMarket = SellBuyMarket();

			// Act

			var approximateRequiredAmountToSell = await sellBuyMarket.Rate(MarketRate.Approximate).RequiredAmountToSell(buyAmount);

			// Assert

			Assert.AreEqual(expectedApproximateRequiredAmountToSell, approximateRequiredAmountToSell);
		}

		[TestMethod]
		public async Task AvailableAmountToBuy_ShouldTakeTwoOrders()
		{
			// Assign

			var expectedAvailableAmountToBuy = TotalsOfTwoOrders;

			var sellAmount = VolumesOfTwoOrders;

			var sellBuyMarket = SellBuyMarket();

			// Act

			var availableAmountToBuy = await sellBuyMarket.Rate(MarketRate.BasedOnOrders).AvailableAmountToBuy(sellAmount);

			// Assert

			Assert.AreEqual(expectedAvailableAmountToBuy, availableAmountToBuy);
		}

		[TestMethod]
		public async Task AvailableAmountToBuy_WithSmallExtraSellAmount_ShouldTakeTwoOrders()
		{
			// Assign

			var smallExtraSellAmount = 0.005m;

			var expectedAvailableAmountToBuy = TotalsOfTwoOrders;

			var sellAmount = VolumesOfTwoOrders + smallExtraSellAmount;

			var sellBuyMarket = SellBuyMarket();

			// Act

			var availableAmountToBuy = await sellBuyMarket.Rate(MarketRate.BasedOnOrders).AvailableAmountToBuy(sellAmount);

			// Assert

			Assert.AreEqual(expectedAvailableAmountToBuy, availableAmountToBuy);
		}

		[TestMethod]
		public async Task AvailableAmountToBuy_WithExtraSellAmount_ShouldTakeTwoAndHalfOrders()
		{
			// Assign

			var extraSellAmount = 0.5m;

			var extraBuyAmount = 0.09m;

			var expectedAvailableAmountToBuy = TotalsOfTwoOrders + extraBuyAmount;

			var sellAmount = VolumesOfTwoOrders + extraSellAmount;

			var sellBuyMarket = SellBuyMarket();

			// Act

			var availableAmountToBuy = await sellBuyMarket.Rate(MarketRate.BasedOnOrders).AvailableAmountToBuy(sellAmount);

			// Assert

			Assert.AreEqual(expectedAvailableAmountToBuy, availableAmountToBuy);
		}

		[TestMethod]
		public async Task RequiredAmountToSell_ShouldTakeTwoOrders()
		{
			// Assign

			var expectedRequiredAmountToSell = VolumesOfTwoOrders;

			var buyAmount = TotalsOfTwoOrders;

			var sellBuyMarket = SellBuyMarket();

			// Act

			var requiredAmountToSell = await sellBuyMarket.Rate(MarketRate.BasedOnOrders).RequiredAmountToSell(buyAmount);

			// Assert

			Assert.AreEqual(expectedRequiredAmountToSell, requiredAmountToSell);
		}

		[TestMethod]
		public async Task RequiredAmountToSell_WithSmallExtraBuyAmount_ShouldTakeTwoOrders()
		{
			// Assign

			var smallExtraBuyAmount = 0.005m;

			var expectedRequiredAmountToSell = VolumesOfTwoOrders;

			var buyAmount = TotalsOfTwoOrders + smallExtraBuyAmount;

			var sellBuyMarket = SellBuyMarket();

			// Act

			var requiredAmountToSell = await sellBuyMarket.Rate(MarketRate.BasedOnOrders).RequiredAmountToSell(buyAmount);

			// Assert

			Assert.AreEqual(expectedRequiredAmountToSell, requiredAmountToSell);
		}

		[TestMethod]
		public async Task RequiredAmountToSell_WithExtraBuyAmount_ShouldTakeTwoAndHalfOrders()
		{
			// Assign

			var extraSellAmount = 0.5m;

			var extraBuyAmount = 0.09m;

			var expectedRequiredAmountToSell = VolumesOfTwoOrders + extraSellAmount;

			var buyAmount = TotalsOfTwoOrders + extraBuyAmount;

			var sellBuyMarket = SellBuyMarket();

			// Act

			var requiredAmountToSell = await sellBuyMarket.Rate(MarketRate.BasedOnOrders).RequiredAmountToSell(buyAmount);

			// Assert

			Assert.AreEqual(expectedRequiredAmountToSell, requiredAmountToSell);
		}

		/// <summary>
		/// Creates market with three buy orders.
		/// FeeFactor = 0.9	(10%)
		/// Price [ETH]	|	Volume [ALT]	|	Total (Price * Volume * FeeFactor) [ETH]	|	Fee [ETH]
		/// 0.8			|	3				|	2.16										|	0.24
		/// 0.6			|	2				|	1.08										|	0.12
		/// 0.2			|	1				|	0.18										|	0.02
		/// </summary>
		/// <returns>Market</returns>
		private IMarket SellBuyMarket()
		{
			var tradeFeeInPercentage = 10m;
			var minimumBaseCoinAmount = 0.1m;
			var minimumQuoteCoinAmount = 0.01m;
			var baseCoin = new Coin("ALT");
			var quoteCoin = new Coin("ETH");

			var tradeFeeFactor = 1m - tradeFeeInPercentage / 100m;

			var sellBuyMarket = new FakeMarket
			(
				tradePair: new TradePair
				(
					coinToSell: baseCoin,
					coinToBuy: quoteCoin,
					minimumCoinToSellAmount: minimumBaseCoinAmount,
					minimumCoinToBuyAmount: minimumQuoteCoinAmount
				),
				orders: new Order[]
				{
					new BuyOrder(0.8m, 3m, baseCoin, quoteCoin, tradeFeeFactor),
					new BuyOrder(0.6m, 2m, baseCoin, quoteCoin, tradeFeeFactor),
					new BuyOrder(0.2m, 1m, baseCoin, quoteCoin, tradeFeeFactor)
				}
			);

			return sellBuyMarket;
		}
	}
}
