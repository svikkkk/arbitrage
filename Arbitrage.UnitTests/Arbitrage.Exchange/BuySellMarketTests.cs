﻿using Arbitrage.Models;
using Arbitrage.Models.Exchange;
using Arbitrage.Models.Exchange.Concretes;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Orders;
using Arbitrage.UnitTests.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace Arbitrage.UnitTests.Arbitrage.Exchange
{
	[TestClass]
	public class BuySellMarketTests
	{
		private const decimal TotalOfFirstOrder = 0.22m;
		private const decimal TotalsOfTwoOrders = TotalOfFirstOrder + 1.32m;
		private const decimal VolumeOfFirstOrder = 1m;
		private const decimal VolumesOfTwoOrders = VolumeOfFirstOrder + 2m;

		[TestMethod]
		public async Task ApproximateAvailableAmountToBuy()
		{
			// Assign

			var expectedApproximateAvailableAmountToBuy = VolumeOfFirstOrder;

			var sellAmount = TotalOfFirstOrder;

			var buySellMarket = BuySellMarket();

			// Act

			var approximateAvailableAmountToBuy = await buySellMarket.Rate(MarketRate.Approximate).AvailableAmountToBuy(sellAmount);

			// Assert

			Assert.AreEqual(expectedApproximateAvailableAmountToBuy, approximateAvailableAmountToBuy);
		}

		[TestMethod]
		public async Task ApproximateRequiredAmountToSell()
		{
			// Assign

			var expectedApproximateRequiredAmountToSell = TotalOfFirstOrder;

			var buyAmount = VolumeOfFirstOrder;

			var buySellMarket = BuySellMarket();

			// Act

			var approximateRequiredAmountToSell = await buySellMarket.Rate(MarketRate.Approximate).RequiredAmountToSell(buyAmount);

			// Assert

			Assert.AreEqual(expectedApproximateRequiredAmountToSell, approximateRequiredAmountToSell);
		}

		[TestMethod]
		public async Task AvailableAmountToBuy_ShouldTakeTwoOrders()
		{
			// Assign

			var expectedAvailableAmountToBuy = VolumesOfTwoOrders;

			var sellAmount = TotalsOfTwoOrders;

			var buySellMarket = BuySellMarket();

			// Act

			var availableAmountToBuy = await buySellMarket.Rate(MarketRate.BasedOnOrders).AvailableAmountToBuy(sellAmount);

			// Assert

			Assert.AreEqual(expectedAvailableAmountToBuy, availableAmountToBuy);
		}

		[TestMethod]
		public async Task AvailableAmountToBuy_WithSmallExtraSellAmount_ShouldTakeTwoOrders()
		{
			// Assign

			var smallExtraSellAmount = 0.005m;

			var expectedAvailableAmountToBuy = VolumesOfTwoOrders;

			var sellAmount = TotalsOfTwoOrders + smallExtraSellAmount;

			var buySellMarket = BuySellMarket();

			// Act

			var availableAmountToBuy = await buySellMarket.Rate(MarketRate.BasedOnOrders).AvailableAmountToBuy(sellAmount);

			// Assert

			Assert.AreEqual(expectedAvailableAmountToBuy, availableAmountToBuy);
		}

		[TestMethod]
		public async Task AvailableAmountToBuy_WithExtraSellAmount_ShouldTakeTwoAndHalfOrders()
		{
			// Assign

			var extraSellAmount = 0.88m;

			var extraBuyAmount = 1m;

			var expectedAvailableAmountToBuy = VolumesOfTwoOrders + extraBuyAmount;

			var sellAmount = TotalsOfTwoOrders + extraSellAmount;

			var buySellMarket = BuySellMarket();

			// Act

			var availableAmountToBuy = await buySellMarket.Rate(MarketRate.BasedOnOrders).AvailableAmountToBuy(sellAmount);

			// Assert

			Assert.AreEqual(expectedAvailableAmountToBuy, availableAmountToBuy);
		}

		[TestMethod]
		public async Task RequiredAmountToSell_ShouldTakeTwoOrders()
		{
			// Assign

			var expectedRequiredAmountToSell = TotalsOfTwoOrders;

			var buyAmount = VolumesOfTwoOrders;

			var buySellMarket = BuySellMarket();

			// Act

			var requiredAmountToSell = await buySellMarket.Rate(MarketRate.BasedOnOrders).RequiredAmountToSell(buyAmount);

			// Assert

			Assert.AreEqual(expectedRequiredAmountToSell, requiredAmountToSell);
		}

		[TestMethod]
		public async Task RequiredAmountToSell_WithSmallExtraBuyAmount_ShouldTakeTwoOrders()
		{
			// Assign

			var smallExtraBuyAmount = 0.05m;

			var expectedRequiredAmountToSell = TotalsOfTwoOrders;

			var buyAmount = VolumesOfTwoOrders + smallExtraBuyAmount;

			var buySellMarket = BuySellMarket();

			// Act

			var requiredAmountToSell = await buySellMarket.Rate(MarketRate.BasedOnOrders).RequiredAmountToSell(buyAmount);

			// Assert

			Assert.AreEqual(expectedRequiredAmountToSell, requiredAmountToSell);
		}

		[TestMethod]
		public async Task RequiredAmountToSell_WithExtraBuyAmount_ShouldTakeTwoAndHalfOrders()
		{
			// Assign

			var extraSellAmount = 0.88m;

			var extraBuyAmount = 1m;

			var expectedRequiredAmountToSell = TotalsOfTwoOrders + extraSellAmount;

			var buyAmount = VolumesOfTwoOrders + extraBuyAmount;

			var buySellMarket = BuySellMarket();

			// Act

			var requiredAmountToSell = await buySellMarket.Rate(MarketRate.BasedOnOrders).RequiredAmountToSell(buyAmount);

			// Assert

			Assert.AreEqual(expectedRequiredAmountToSell, requiredAmountToSell);
		}

		/// <summary>
		/// Creates market with three sell orders.
		/// FeeFactor = 1.1	(10%)
		/// Price [ETH]	|	Volume [ALT]	|	Total (Price * Volume * FeeFactor) [ETH]	|	Fee [ETH]
		/// 0.2			|	1				|	0.22										|	0.02
		/// 0.6			|	2				|	1.32										|	0.12
		/// 0.8			|	3				|	2.64										|	0.24
		/// </summary>
		/// <returns>Market</returns>
		private IMarket BuySellMarket()
		{
			var tradeFeeInPercentage = 10m;
			var minimumBaseCoinAmount = 0.1m;
			var minimumQuoteCoinAmount = 0.01m;
			var baseCoin = new Coin("ALT");
			var quoteCoin = new Coin("ETH");

			var tradeFeeFactor = 1m + tradeFeeInPercentage / 100m;

			var buySellMarket = new FakeMarket
			(
				tradePair: new TradePair
				(
					coinToSell: quoteCoin, 
					coinToBuy: baseCoin, 
					minimumCoinToSellAmount: minimumQuoteCoinAmount,
					minimumCoinToBuyAmount: minimumBaseCoinAmount
				),
				orders: new Order[]
				{
					new SellOrder(0.2m, 1m, quoteCoin, baseCoin, tradeFeeFactor),
					new SellOrder(0.6m, 2m, quoteCoin, baseCoin, tradeFeeFactor),
					new SellOrder(0.8m, 3m, quoteCoin, baseCoin, tradeFeeFactor)
				}
			);

			return buySellMarket;
		}
	}
}
