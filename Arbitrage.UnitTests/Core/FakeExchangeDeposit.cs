﻿using System.Threading.Tasks;
using Arbitrage.Models;
using Arbitrage.Models.Exchange.Interfaces;

namespace Arbitrage.UnitTests.Core
{
    class FakeExchangeDeposit : IExchangeDeposit
    {
	    public Coin Coin { get; set; }
	    public string FakeAddress { get; set; } = "FakeAdddress";

	    public decimal FeeFactor { get; set; }

	    public Task<string> Address()
	    {
		    return Task.FromResult(FakeAddress);
	    }

	    public decimal Fee(decimal depositAmount)
	    {
		    return FeeFactor;
	    }

	    public decimal ReverseFee(decimal amountToBeDeposited)
	    {
		    return FeeFactor;
		}
    }
}
