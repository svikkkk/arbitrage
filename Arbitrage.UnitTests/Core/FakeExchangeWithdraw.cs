﻿using System.Threading.Tasks;
using Arbitrage.Models;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Wallet;

namespace Arbitrage.UnitTests.Core
{
    class FakeExchangeWithdraw : IExchangeWithdraw
    {
	    public Coin Coin { get; set; }

		public decimal WithdrawFeePercent { get; set; }

	    public Task<decimal> Fee(decimal withdrawAmount)
	    {
		    return Task.FromResult(WithdrawFeePercent * withdrawAmount);
		}

	    public Task<decimal?> MinimalWithdraw()
	    {
			return Task.FromResult((decimal?)null);
		}

	    public Task WithdrawTo(IWallet wallet, decimal amount)
	    {
		    return Task.FromResult(true);
	    }

	    public Task WithdrawTo(IExchange exchange, decimal amount)
	    {
			return Task.FromResult(true);
		}
    }
}
