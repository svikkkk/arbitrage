﻿using Arbitrage.Models.Exchange.Concretes;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Orders;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Arbitrage.UnitTests.Core
{
	internal class FakeMarket : Market
	{
		private readonly IList<Order> _orders;

		public FakeMarket(Order order) : base(
			tradePair: new TradePair(
				coinToSell: order.Sell,
				coinToBuy: order.Buy, 
				minimumCoinToSellAmount: 0, 
				minimumCoinToBuyAmount: 0), 
			approximateOrder: order)
		{
			_orders = new[] { order };
		}

		public FakeMarket(ITradePair tradePair, IList<Order> orders)
			: this(tradePair, orders, orders[0]) { }

		public FakeMarket(ITradePair tradePair, IList<Order> orders, Order approximateOrder) 
			: base(tradePair, approximateOrder)
		{
			_orders = orders;
		}

		protected override Task OnInit()
		{
			return Task.CompletedTask;
		}

		protected override Task<Order> OrderAt(int index)
		{
			if (index >= _orders.Count)
			{
				RaiseNotEnoughOrdersException();
			}

			return Task.FromResult(_orders[index]);
		}
	}
}
