﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Arbitrage.Models;
using Arbitrage.Models.Exchange;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Orders;
using Newtonsoft.Json;

namespace Arbitrage.UnitTests.Core
{
    class FakeExchange : IExchange
    {
	    public string Name { get; set; } = "FakeExchange";

	    [JsonIgnore]
		public Dictionary<string, decimal> Balances { get; set; } = new Dictionary<string, decimal>();

	    [JsonIgnore]
		public MarketsCollection MarketsCollection { get; set; } = new MarketsCollection(new List<IMarket>());

		[JsonIgnore]
		public IEnumerable<IExchangeDeposit> ExchangeDeposits { get; set; } = new IExchangeDeposit[0];

		[JsonIgnore]
		public IEnumerable<IExchangeWithdraw> ExchangeWithdraws { get; set; } = new List<IExchangeWithdraw>();

		public Task<decimal> Balance(Coin coin)
		{
			decimal coinBalance = 0;
		    if (Balances.ContainsKey(coin.Code))
		    {
			    coinBalance = Balances[coin.Code];
		    }

			return Task.FromResult(coinBalance);
		}

	    public Task<IMarketsCollection> Markets()
	    {
		    return Task.FromResult((IMarketsCollection)MarketsCollection);
	    }

	    public Task<IEnumerable<IExchangeDeposit>> DepositsCollection()
	    {
			return Task.FromResult(ExchangeDeposits);
		}

	    public Task<IEnumerable<IExchangeWithdraw>> WithdrawsCollection()
	    {
			return Task.FromResult(ExchangeWithdraws);
		}

	    public Task<CoinsCollection> CoinsCollection()
	    {
		    throw new NotImplementedException();
	    }

	    public Task Trade(Order order)
	    {
		    throw new NotImplementedException();
	    }

	    public void Clear()
	    {
	    }
    }
}
