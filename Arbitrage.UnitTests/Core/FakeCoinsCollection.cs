﻿using System.Collections.Generic;
using Arbitrage.Models;

namespace Arbitrage.UnitTests.Core
{
    class FakeCoinsCollection
    {
	    private readonly int _coinsCount;

	    public FakeCoinsCollection(int coinsCount)
	    {
		    _coinsCount = coinsCount;
	    }

		public IList<Coin> ToList()
	    {
		    var coins = new List<Coin>();
		    for (int i = 0; i < _coinsCount; i++)
		    {
			    coins.Add(new Coin($"Code{i}"));

			}

		    return coins;
	    }
    }
}
