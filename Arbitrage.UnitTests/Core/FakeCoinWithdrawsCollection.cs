﻿using System.Collections.Generic;
using Arbitrage.Models;

namespace Arbitrage.UnitTests.Core
{
    class FakeCoinWithdrawsCollection
	{
	    private readonly IList<Coin> _exchangeCoins;

	    public FakeCoinWithdrawsCollection(IList<Coin> exchangeCoins)
	    {
		    _exchangeCoins = exchangeCoins;
	    }

	    public IEnumerable<FakeExchangeWithdraw> ToList()
	    {
		    var deposits = new List<FakeExchangeWithdraw>();
		    foreach (var exchangeCoin in _exchangeCoins)
		    {
			    deposits.Add(new FakeExchangeWithdraw {Coin = exchangeCoin, WithdrawFeePercent = 0.01m});
		    }

		    return deposits;
	    }
	}
}
