﻿using Arbitrage.Models;
using Arbitrage.Models.Exchange;
using Arbitrage.Models.Exchange.Interfaces;
using System.Collections.Generic;

namespace Arbitrage.UnitTests.Core
{
	class FakeExchangeCollection
	{
		private readonly int _exchangeCount;
		private readonly IList<Coin> _exchangeCoins;
		private readonly IList<IMarket> _fakeMarkets;

		public FakeExchangeCollection(int exchangeCount, IList<Coin> exchangeCoins)
			: this(exchangeCount, exchangeCoins, new FakeCoinMarketsCollection(exchangeCoins).ToList())
		{
		}

		public FakeExchangeCollection(int exchangeCount, IList<Coin> exchangeCoins, IList<FakeMarket> fakeMarkets)
		{
			_exchangeCount = exchangeCount;
			_exchangeCoins = exchangeCoins;
			_fakeMarkets = new List<IMarket>(fakeMarkets);
		}

		public IList<FakeExchange> ToList()
		{
			var exchangesList = new List<FakeExchange>();
			for (var exchangeNumber = 0; exchangeNumber < _exchangeCount; exchangeNumber++)
			{
				exchangesList.Add(new FakeExchange
				{
					Name = $"FakeExchange{exchangeNumber}",
					MarketsCollection = new MarketsCollection(_fakeMarkets),
					ExchangeDeposits = new FakeCoinDepositsCollection(_exchangeCoins).ToList(),
					ExchangeWithdraws = new FakeCoinWithdrawsCollection(_exchangeCoins).ToList(),
					Balances = FakeCoinsBalance()
				});
			}

			return exchangesList;
		}

		private Dictionary<string, decimal> FakeCoinsBalance()
		{
			var fakeCoinsBalance = new Dictionary<string, decimal>();

			foreach (var exchangeCoin in _exchangeCoins)
			{
				fakeCoinsBalance.Add(exchangeCoin.Code, 100000);
			}

			return fakeCoinsBalance;
		}
	}
}
