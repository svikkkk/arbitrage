﻿using System.Threading.Tasks;
using Arbitrage.Models;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Wallet;

namespace Arbitrage.UnitTests.Core
{
	class FakeWallet : IWallet
	{
		private readonly decimal _balance;

		public Coin Coin { get; private set; }

		public string Address { get; } = "FakeWalletAddress";

		public FakeWallet(Coin coin, decimal balance)
		{
			_balance = balance;
			Coin = coin;
		}

		public Task<decimal> Balance()
		{
			return Task.FromResult(_balance);
		}

		public Task DepositTo(IExchange exchange, decimal amount)
		{
			return Task.FromResult(true);
		}

		public Task DepositTo(IWallet wallet, decimal amount)
		{
			return Task.FromResult(true);
		}
	}
}
