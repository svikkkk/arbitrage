﻿using System.Collections.Generic;
using Arbitrage.Models;
using Arbitrage.Models.Orders;

namespace Arbitrage.UnitTests.Core
{
    class FakeCoinMarketsCollection
    {
	    private readonly IList<Coin> _exchangeCoins;

	    public FakeCoinMarketsCollection(IList<Coin> exchangeCoins)
	    {
		    _exchangeCoins = exchangeCoins;
	    }

	    public IList<FakeMarket> ToList()
	    {
			var markets = new List<FakeMarket>();
		    for (int currentCoinIndex = 0; currentCoinIndex < _exchangeCoins.Count - 1; currentCoinIndex++)
		    {
			    for (int nextCoinIndex = currentCoinIndex + 1; nextCoinIndex < _exchangeCoins.Count; nextCoinIndex++)
			    {
				    var currentCoin = _exchangeCoins[currentCoinIndex];
				    var nextCoin = _exchangeCoins[nextCoinIndex];

				    markets.Add(new FakeMarket(new BuyOrder(1, 1000, nextCoin, currentCoin, 1)));
				    markets.Add(new FakeMarket(new SellOrder(1000, 1000, currentCoin, nextCoin, 1)));
				}
		    }

			return markets;
		}
	}
}
