﻿using System.Collections.Generic;
using Arbitrage.Models;

namespace Arbitrage.UnitTests.Core
{
    class FakeCoinDepositsCollection
    {
	    private readonly IList<Coin> _exchangeCoins;

	    public FakeCoinDepositsCollection(IList<Coin> exchangeCoins)
	    {
		    _exchangeCoins = exchangeCoins;
	    }

	    public IEnumerable<FakeExchangeDeposit> ToList()
	    {
		    var deposits = new List<FakeExchangeDeposit>();
		    foreach (var exchangeCoin in _exchangeCoins)
		    {
				deposits.Add(new FakeExchangeDeposit{ Coin = exchangeCoin, FeeFactor = 1 });
			}

		    return deposits;
	    }
	}
}
