﻿using Arbitrage.Models;
using Arbitrage.Models.Exchange.Interfaces;
using System;
using System.Threading.Tasks;

namespace Arbitrage.UnitTests.Core
{
	class FakeExchangeClient : IExchangeClient
	{
		private readonly IMarketsCollection _marketsCollection;
		private readonly decimal _withdrawFeePercentage;

		public FakeExchangeClient(IMarketsCollection marketsCollection, decimal withdrawFeePercentageForAnyCoin)
		{
			_marketsCollection = marketsCollection;
			_withdrawFeePercentage = withdrawFeePercentageForAnyCoin;
		}

		public Task<decimal> Balance(Coin coin)
		{
			throw new NotImplementedException();
		}

		public Task<IMarketsCollection> Markets()
		{
		    return Task.FromResult(_marketsCollection);
		}

		public Task SubmitWithdraw(Coin coin, decimal amount, string destinationAddress)
		{
			throw new NotImplementedException();
		}

		public Task<decimal> WithdrawFee(Coin coin, decimal amount)
		{
			return Task.FromResult(amount * _withdrawFeePercentage / 100m);
		}

		public Task<decimal?> MinimalWithdraw(Coin coin)
		{
			return Task.FromResult((decimal?)null);
		}

		public Task<string> DepositDestinationAddress(Coin coin)
		{
			throw new NotImplementedException();
		}

		public void Dispose()
		{
		}
	}
}
