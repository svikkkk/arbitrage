﻿using Arbitrage.Models;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Wallet;
using Arbitrage.Planner.Interfaces;
using Arbitrage.Planner.Plan;
using Arbitrage.Planner.Wallet;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Arbitrage.Planner
{
	public class Planner : IPlanner
	{
		private readonly IList<IExchange> _exchanges;

		public Planner(IList<IExchange> exchanges)
		{
			_exchanges = exchanges;
		}

		public Task<TradingPlan> Exit(TradingPlan plan)
		{
			throw new System.NotImplementedException();
		}

		public Task Update(TradingPlan plan)
		{
			throw new System.NotImplementedException();
		}

		public Task<IEnumerable<TradingPlan>> Plans(IExchange startExchange, Coin startCoin, decimal amount, int maxSteps)
		{
			throw new System.NotImplementedException();
		}

		public async Task<IEnumerable<TradingPlan>> Plans(IWallet wallet, decimal amount, int maxSteps)
		{
			return await new WalletPlansCollection(wallet, amount, _exchanges).ToList();
		}

		public Task<IEnumerable<TradingPlan>> Plans(TradingPlan plan, Coin finishCoin, int maxSteps)
		{
			throw new System.NotImplementedException();
		}

		public Task<IEnumerable<TradingPlan>> Plan(TradingPlan plan, int maxSteps)
		{
			throw new System.NotImplementedException();
		}
	}
}
