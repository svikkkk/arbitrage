﻿using System.Collections.Generic;

namespace Arbitrage.Planner.Plan
{
	public class BackTraceItem
	{
		public int NodeId { get; }

		public BackTraceItem Parent { get; }

		public BackTraceItem(int nodeId, BackTraceItem parent)
		{
			NodeId = nodeId;
			Parent = parent;
		}

		public IList<int> ToNodeIdList()
		{
			var parent = this;
			var nodeStack = new List<int>();
			while (parent != null)
			{
				nodeStack.Add(parent.NodeId);
				parent = parent.Parent;
			}

			nodeStack.Reverse();
			return nodeStack;
		}
	}
}
