﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Arbitrage.Models;
using Arbitrage.Planner.Interfaces;

namespace Arbitrage.Planner.Plan
{
    class OptimalAmountGoodPlansCalculation
    {
	    private readonly GoodPlansCollection _goodPlansCollection;
	    private readonly IPlanFilter _filter;

	    private AnalyzationResult _analyzationResult;

		private TaskCompletionSource<bool> _taskCompletionSource;
	    private long _balancedPlans;
	    private long _allPossiblePlansFound = 0;
	    public event Action<TradingPlan> PlanFound = delegate { };

		public OptimalAmountGoodPlansCalculation(GoodPlansCollection goodPlansCollection, Interfaces.IPlanFilter filter)
		{
			_goodPlansCollection = goodPlansCollection;
			_filter = filter;
		}
		
	    public async Task<AnalyzationResult> Calculate()
	    {
		    try
		    {
			    Clear();

				_goodPlansCollection.PlanFound += OnGoodPlanFound;
				var foundGoodPlans = await _goodPlansCollection.ToList();
			    Interlocked.Increment(ref _allPossiblePlansFound);
				if (foundGoodPlans.Count != Interlocked.Read(ref _balancedPlans))
			    {
				    await _taskCompletionSource.Task;
			    }
			}
		    finally
		    {
				_goodPlansCollection.PlanFound -= OnGoodPlanFound;
			}

		    return _analyzationResult;
	    }

	    private void OnGoodPlanFound(TradingPlan goodPlan)
	    {
		    Interlocked.Increment(ref _analyzationResult.PotentiallyProfitablePlans);
			Console.WriteLine($"Plan: {goodPlan.ProfitRatio}");
			goodPlan.WithOptimalStartAmount().ContinueWith(OnOptimalStartAmountCalculated);
		}

	    private void OnOptimalStartAmountCalculated(Task<TradingPlan> planTask)
	    {
			var balancedPlans = Interlocked.Increment(ref _balancedPlans);
		    if (planTask.Exception != null)
		    {
			    Console.Write(planTask.Exception);
			    Interlocked.Increment(ref _analyzationResult.ExceptionDuringCalculationPlans);
		    }
		    else
		    {
			    var plan = planTask.Result;
			    if (plan.ProfitRatio < 1)
			    {
				    Interlocked.Increment(ref _analyzationResult.NotProfitableAfterAmountRecalculation);
			    }
			    else if (!_filter.Pass(plan))
			    {
				    Interlocked.Increment(ref _analyzationResult.NotFilteredRecalculation);
			    }
			    else
			    {
				    Interlocked.Increment(ref _analyzationResult.ProfitablePlans);
					PlanFound.Invoke(planTask.Result);
			    }
		    }

		    if (Interlocked.Read(ref _allPossiblePlansFound) == 1 && balancedPlans == Interlocked.Read(ref _analyzationResult.PotentiallyProfitablePlans))
		    {
			    _taskCompletionSource.SetResult(true);
		    }
		}

	    private void Clear()
	    {
		    _analyzationResult = new AnalyzationResult();
			_taskCompletionSource = new TaskCompletionSource<bool>();
		    _balancedPlans = 0;
		    _allPossiblePlansFound = 0;
	    }
	}
}
