﻿using System.Threading.Tasks;
using Arbitrage.Models.Exchange;

namespace Arbitrage.Planner.Plan
{
	public class TradingStartAmount
    {
	    private readonly TradingPlan _tradingPlan;
	    private readonly decimal _deltaAmount;
	    private readonly bool _needCalculateOrdersActualVolume;

	    private TradingProfitableLevelsCollection _profitableLevelsCollection;

		internal TradingProfitableLevelsCollection ProfitableLevelsCollection
	    {
		    get
		    {
			    if (_profitableLevelsCollection == null)
			    {
				    _profitableLevelsCollection = new TradingProfitableLevelsCollection(
						new TradingProfitRatio(_tradingPlan), 
						new TradingPlanOrdersCollection(_tradingPlan, _needCalculateOrdersActualVolume),
						_deltaAmount);
				}

				_profitableLevelsCollection.Clear();
			    return _profitableLevelsCollection;
		    }
	    }

		public TradingStartAmount(TradingPlan tradingPlan, decimal deltaAmount, bool needCalculateOrdersActualVolume)
	    {
		    _tradingPlan = tradingPlan;
		    _deltaAmount = deltaAmount;
		    _needCalculateOrdersActualVolume = needCalculateOrdersActualVolume;
	    }

	    public async Task<decimal> ToDecimal()
	    {
			var mostProfitableLevel = await ToMostProfitableLevel();
			return mostProfitableLevel?.StartAmount ?? 0;
		}

	    internal Task<PlanProfitableLevel> ToMostProfitableLevel()
	    {
		    return new MostProfitableLevel(ProfitableLevelsCollection).ToProfitableLevel();
		}
	}
}
