﻿using System.Collections.Generic;
using Arbitrage.Models.Orders;
using Arbitrage.Planner.Interfaces;

namespace Arbitrage.Planner.Plan
{
	internal class PlanProfitableLevel
	{
		public decimal ProfitRatio { get; }

		public decimal StartAmount { get; }

		public Dictionary<IPlanStep, IList<Order>> Orders { get; }

		public decimal Outcome => StartAmount * ProfitRatio - StartAmount;


		public bool HasCoin(string coinCode)
		{

			foreach (var stepNumber in Orders.Keys)
			{
				foreach (var order in Orders[stepNumber])
				{
					if (order.ToString().Contains(coinCode))
					{
						return true;
					}
				}
			}

			return false;
		}

		public PlanProfitableLevel(decimal startAmount, decimal profitRatio,Dictionary<IPlanStep, IList<Order>> orders)
		{
			ProfitRatio = profitRatio;
			StartAmount = startAmount;
			Orders = orders;
		}
	}
}
