﻿using Arbitrage.Models;
using Arbitrage.Planner.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arbitrage.Planner.Plan
{
	public class TradingPlan
	{
		public decimal StartAmount { get; }

		public Coin StartCoin => Steps.First().StartCoin;

		public Coin EndCoin => Steps.Last().EndCoin;

		public IList<IPlanStep> Steps { get; }

		public decimal ProfitRatio { get; }

		public int Id => GetHashCode();

		public TradingPlan(IList<IPlanStep> steps, decimal startAmount, decimal profitRatio)
		{
			StartAmount = startAmount;
			Steps = steps;
			ProfitRatio = profitRatio;
		}

		public async Task<TradingPlan> WithOptimalStartAmount(bool needCalculateOrdersActualVolume = false)
		{
			var mostProfitableLevel = await new TradingStartAmount(this, 0.00001m, needCalculateOrdersActualVolume).ToMostProfitableLevel();
			return new TradingPlan(Steps, mostProfitableLevel.StartAmount, mostProfitableLevel.ProfitRatio);
		}

		public override string ToString()
		{
			var stringBuilder = new StringBuilder();
			foreach (var planAction in Steps)
			{
				stringBuilder.Append(planAction);
				stringBuilder.AppendLine();
			}

			return stringBuilder.ToString();
		}
	}
}
