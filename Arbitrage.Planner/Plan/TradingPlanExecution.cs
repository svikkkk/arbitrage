﻿using System;
using System.Threading.Tasks;
using Arbitrage.Planner.Interfaces;

namespace Arbitrage.Planner.Plan
{
    public class TradingPlanExecution
    {
	    private readonly TradingPlan _plan;

	    private int _passedStreps;

	    private decimal _prevStepAmount;

		public bool IsFinished => _passedStreps == _plan.Steps.Count;

	    public IPlanStep CurrentStep { get; private set; }

		public TradingPlanExecution(TradingPlan plan)
	    {
		    _plan = plan;
		    _passedStreps = 0;
		    _prevStepAmount = _plan.StartAmount;
			FirstStep();
		}

	    public async Task ExecuteStep()
	    {
		    ValidateStep();
		    _prevStepAmount = await CurrentStep.Execute(_prevStepAmount);
		    _passedStreps++;
		    NextStep();
	    }

	    private void NextStep()
	    {
		    var currentStepIndex = _plan.Steps.IndexOf(CurrentStep);
		    CurrentStep = _plan.Steps[currentStepIndex + 1];
	    }

	    private void FirstStep()
	    {
		    CurrentStep = _plan.Steps[0];
	    }

	    private void ValidateStep()
	    {
		    if (IsFinished)
		    {
			    throw new Exception("Plan is finished");
		    }
	    }
	}
}
