﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Arbitrage.Planner.Plan
{
    class ProfitableLevel
    {
	    private readonly TradingProfitRatio _factor;
	    private readonly decimal _deltaAmount;

	    public ProfitableLevel(TradingProfitRatio factor, decimal deltaAmount)
	    {
		    _factor = factor;
		    _deltaAmount = deltaAmount;
	    }

	    public async Task<List<(float profitRatio, decimal amount)>> ToList()
	    {
		    var profitableLevels = new List<(float profit, decimal startAmount)>();

			var totalAmount = 0m;

			do
			{
				var profitableLevel = await NextProfitableLevel(totalAmount);
				if (profitableLevel.profit > 1)
				{
					profitableLevels.Add(profitableLevel);
					totalAmount = profitableLevel.startAmount;
				}
				else
				{
					break;
				}
			} while (true);

		    return profitableLevels;
	    }

	    private async Task<(float profit, decimal startAmount)> NextProfitableLevel(decimal totalAmount)
	    {
		    var amount = totalAmount + _deltaAmount;
			var prevProfit = await _factor.ToDecimal(amount);

			do
		    {
			    var nextProfit = await _factor.ToDecimal(amount + _deltaAmount);
			    if (nextProfit < prevProfit && Math.Abs(nextProfit - prevProfit) > 0.001m)
			    {
				    break;
			    }

				amount += _deltaAmount;
			    if (nextProfit > prevProfit)
			    {
					prevProfit = nextProfit;
				}
		    } while (true);

		    return ((float)prevProfit, amount);
	    }
	}
}
