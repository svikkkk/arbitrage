﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Arbitrage.Planner.Plan
{
    class OptimalAmountGoodPlansCollection
    {
	    private readonly GoodPlansCollection _goodPlansCollection;

	    public event Action<TradingPlan> PlanFound = delegate { };

		public OptimalAmountGoodPlansCollection(GoodPlansCollection goodPlansCollection)
	    {
		    _goodPlansCollection = goodPlansCollection;
	    }

		public async Task<List<TradingPlan>> ToList(int topLimit = 0)
	    {
		    var potentialPlansList = await GoodPlans(topLimit);
			return await BalancedOrdersPlans(potentialPlansList);
	    }

	    private async Task<List<TradingPlan>> BalancedOrdersPlans(List<TradingPlan>  potentialPlansList)
	    {
		    var totalPlans = potentialPlansList.Count;

		    var balancedPlansCollection = new List<TradingPlan>(totalPlans);
		    var planRecalculationTasks = new List<Task>();

		    var recalculatedCount = 0;
			foreach (var tradingPlan in potentialPlansList)
		    {
			    planRecalculationTasks.Add(tradingPlan.WithOptimalStartAmount().ContinueWith(planTask =>
			    {
				    var count = Interlocked.Increment(ref recalculatedCount);
				    Console.WriteLine($"{count}/{totalPlans}");
				    if (planTask.Exception != null)
				    {
					    Console.Write(planTask.Exception);
				    }
				    else
				    {
					    lock (balancedPlansCollection)
					    {
						    balancedPlansCollection.Add(planTask.Result);
							PlanFound.Invoke(planTask.Result);
					    }
					}
			    }));
		    }

		    await Task.WhenAll(planRecalculationTasks);
		    return balancedPlansCollection.ToList();
		}

		private async Task<List<TradingPlan>> GoodPlans(int topLimit)
	    {
			var goodPlansList = await _goodPlansCollection.ToList();

		    var totalPlans = goodPlansList.Count;
		    Console.WriteLine($"Good plans orders balancing: {totalPlans}");

		    goodPlansList.Sort((p1, p2) => p1.ProfitRatio.CompareTo(p2.ProfitRatio));
		    if (topLimit > 0)
		    {
			    goodPlansList = goodPlansList.Skip(Math.Max(0, goodPlansList.Count - topLimit)).ToList();
		    }

		    return goodPlansList;
	    }
	}
}
