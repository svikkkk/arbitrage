﻿using Arbitrage.Models.Exchange.Exceptions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Arbitrage.Models.Orders;
using Arbitrage.Planner.Interfaces;

namespace Arbitrage.Planner.Plan
{
	internal class TradingProfitableLevelsCollection
	{
		private readonly TradingPlanOrdersCollection _ordersCollection;
		private readonly TradingProfitRatio _protifRatio;

		private readonly DeltaAmount _deltaAmount;

		private decimal _totalAmount = 0m;

		public TradingProfitableLevelsCollection(
			TradingProfitRatio protifRatio, 
			TradingPlanOrdersCollection ordersCollection, 
			decimal deltaAmount)
			: this(protifRatio, new DeltaAmount(deltaAmount))
		{
			_ordersCollection = ordersCollection;
			_deltaAmount = new DeltaAmount(deltaAmount);
		}

		public TradingProfitableLevelsCollection(TradingProfitRatio protifRatio, DeltaAmount deltaAmount)
		{
			_protifRatio = protifRatio;
			_deltaAmount = deltaAmount;
		}

		public async Task<List<PlanProfitableLevel>> ToList(int count)
		{
			var profitLevels = new List<PlanProfitableLevel>();

			Clear();

			for (var i = 0; i < count; i++)
			{
				var nextProfitLevel = await Next();
				if (nextProfitLevel != null)
				{
					profitLevels.Add(nextProfitLevel);
				}
				else
				{
					break;
				}
			}

			return profitLevels;
		}

		public async Task<PlanProfitableLevel> Next()
		{
			try
			{
				var profitableLevel = await Next(_totalAmount);
				_totalAmount = profitableLevel.StartAmount;
				return profitableLevel;
			}
			catch (NotEnoughOrdersException e)
			{
				// Next Profit level can't be taken
			}

			return null;
		}

		public void Clear()
		{
			_totalAmount = 0m;
		}

		private async Task<PlanProfitableLevel> Next(decimal totalAmount)
		{
			var amount = totalAmount + _deltaAmount.Next();
			var initialOrders = await _ordersCollection.ToStepOrders(amount);
			var initialOrdersCount = OrdersCount(initialOrders);
			var prevOrders = initialOrders;

			do
			{
				var delta = _deltaAmount.Next();
				Dictionary<IPlanStep, IList<Order>> amountOrders = null;

				var hasError = false;
				try
				{
					amountOrders = await _ordersCollection.ToStepOrders(amount + delta);
				}
				catch (NotEnoughOrdersException e)
				{
					hasError = true;
				}
				 
				if (amountOrders != null && OrdersCount(amountOrders) != initialOrdersCount || hasError)
				{
					if (!_deltaAmount.IsDefault)
					{
						_deltaAmount.Clear();
						continue;
					}

					var ratio = await _protifRatio.ToDecimal(amount);
					return new PlanProfitableLevel(amount, ratio, prevOrders);
				}

				amount += delta;
				prevOrders = amountOrders;
			} while (true);
		}

		private int OrdersCount(Dictionary<IPlanStep, IList<Order>> stepsOrders)
		{
			int count = 0;
			foreach (var step in stepsOrders.Keys)
			{
				count += stepsOrders[step].Count;
			}

			return count;
		}
	}
}
