﻿using System.Threading.Tasks;

namespace Arbitrage.Planner.Plan
{
    class MostProfitableLevel
    {
	    private readonly TradingProfitableLevelsCollection _profitableLevelsCollection;

	    public MostProfitableLevel(TradingProfitableLevelsCollection profitableLevelsCollection)
	    {
		    _profitableLevelsCollection = profitableLevelsCollection;
	    }

		public async Task<PlanProfitableLevel> ToProfitableLevel()
	    {
		    PlanProfitableLevel mostProfitableLevel = null;
		    var prevProfitRatio = decimal.MinValue;
		    var prevLevelOutcome = decimal.MinValue;
			do
		    {
			    var profitLevel = await _profitableLevelsCollection.Next();
			    if (profitLevel != null)
			    {
				    var levelOutcome = profitLevel.Outcome;
					if (profitLevel.ProfitRatio < prevProfitRatio)
				    {
						if (profitLevel.ProfitRatio < 1)
					    {
						    break;
						}

					    if (levelOutcome <= prevLevelOutcome)
					    {
						    break;
					    }
				    }

				    prevLevelOutcome = levelOutcome;
				    prevProfitRatio = profitLevel.ProfitRatio;
				    mostProfitableLevel = profitLevel;
			    }
			    else
			    {
				    break;
			    }
		    } while (true);

		    return mostProfitableLevel;
		}
	}
}
