﻿using Arbitrage.Models.Exchange;
using Arbitrage.Planner.Tree;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Node = Arbitrage.Planner.Tree.Node;

namespace Arbitrage.Planner.Plan
{
	public class GoodPlansCollection
	{
		private readonly decimal _startAmount;
		private readonly decimal _goodPlanRatio;

		private int _totalPlans;

		private int _processedPlans;

		public TradingTree TradingTree { get; }

		public event Action<TradingPlan> PlanFound = delegate { };

		public GoodPlansCollection(TradingTree tradingTree, decimal startAmount, decimal goodPlanRatio = 1m)
		{
			TradingTree = tradingTree;
			_startAmount = startAmount;
			_goodPlanRatio = goodPlanRatio;
		}

		public async Task<List<TradingPlan>> ToList()
		{
			_totalPlans = await TradingTree.Details().Variants();

			var plans = new List<TradingPlan>();
			await Task.WhenAll(CollectPotentialPlans(TradingTree.RootNode, _startAmount, null, plans));
			return plans;
		}

		private IList<Task> CollectPotentialPlans(Node currentNode, decimal amount, BackTraceItem parent, List<TradingPlan> plans)
		{
			var tasksList = new List<Task>();
			var backTraceItem = new BackTraceItem(currentNode.DataNodeId, parent);

            if (TradingTree.IsLeafNode(currentNode))
			{
				tasksList.Add(TryToBuildPlan(amount, backTraceItem, plans));
			}
			else
			{
				tasksList.AddRange(ProcessNodeChildren(currentNode, amount, backTraceItem, plans));
			}

			return tasksList;
		}

		private Task TryToBuildPlan(decimal amount, BackTraceItem backTraceItem, List<TradingPlan> plans)
		{
			var incrementedProcessedPlans = Interlocked.Increment(ref _processedPlans);
			var planFactor = amount / _startAmount;

			if (IsGoodPlan(planFactor))
			{
				Console.WriteLine($"{incrementedProcessedPlans}/{_totalPlans} ({planFactor} - {backTraceItem.ToNodeIdList().Count})");
				return Task.Run(async () =>
				{
					var plan = await BuildPlan(planFactor, backTraceItem);
					lock (plans)
					{
						plans.Add(plan);
					}

					PlanFound(plan);
				});
			}

			Console.WriteLine($"{incrementedProcessedPlans}/{_totalPlans}");
			return Task.FromResult(true);
		}

		private List<Task> ProcessNodeChildren(Node currentNode, decimal amount, BackTraceItem backTraceItem, List<TradingPlan> plans)
		{
			var tasksList = new List<Task>();
			for (int i = 0; i < currentNode.Children.Count; i++)
			{
				var child = currentNode.Children[i];
				tasksList.Add(Task.Run(async () =>
				{
					try
					{
						var cost = await TradingTree.Cost(currentNode.DataNodeId, child.DataNodeId);
						var childAmount = await cost.Ratio(amount);
						await Task.WhenAll(CollectPotentialPlans(child, childAmount, backTraceItem, plans));
					}
					catch (MarketNotFoundException e)
					{
						Console.WriteLine(e);
					}
				}));
			}

			return tasksList;
		}

		private bool IsGoodPlan(decimal planFactor)
		{
			return planFactor > _goodPlanRatio;
		}

		private async Task<TradingPlan> BuildPlan(decimal planFactor, BackTraceItem backTraceItem)
		{
			var planActions = await TradingTree.PlanActions(backTraceItem.ToNodeIdList());
			return new TradingPlan(planActions, _startAmount, planFactor);
		}
	}
}
