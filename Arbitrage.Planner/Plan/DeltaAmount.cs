﻿using System;

namespace Arbitrage.Planner.Plan
{
	internal class DeltaAmount
	{
		private readonly decimal _deltaAmount;

		private int _multiplier = 0;

		public DeltaAmount(decimal deltaAmount)
		{
			_deltaAmount = deltaAmount;
		}

		public bool IsDefault
		{
			get
			{
				if (_multiplier == 0)
				{
					throw new Exception("'Next()' must be called first");
				}

				return _multiplier == 1;
			}
		}

		public decimal Next()
		{
			var newAmount = _deltaAmount * (1 << _multiplier);
			_multiplier++;
			return newAmount;
		}

		public void Clear()
		{
			_multiplier = 0;
		}
	}
}
