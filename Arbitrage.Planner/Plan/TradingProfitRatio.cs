﻿using System.Threading.Tasks;

namespace Arbitrage.Planner.Plan
{
	public class TradingProfitRatio
    {
	    private readonly TradingPlan _tradingPlan;

	    public decimal Amount { get; private set; }

	    public TradingProfitRatio(TradingPlan tradingPlan)
	    {
		    _tradingPlan = tradingPlan;
	    }

	    public Task<decimal> ToDecimal()
	    {
		    return ToDecimal(_tradingPlan.StartAmount);
	    }

		public async Task<decimal> ToDecimal(decimal startAmount)
	    {
			Amount = startAmount;
			foreach (var planStep in _tradingPlan.Steps)
			{
				Amount = await planStep.TestRun(Amount);
			}

		    return Amount / startAmount;
	    }
	}
}
