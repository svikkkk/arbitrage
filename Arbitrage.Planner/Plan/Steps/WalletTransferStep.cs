﻿using System.Linq;
using System.Threading.Tasks;
using Arbitrage.Models;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Wallet;
using Arbitrage.Planner.Costs;
using Arbitrage.Planner.Interfaces;

namespace Arbitrage.Planner.Plan.Steps
{
	public class WalletTransferStep : IPlanStep
	{
		public IExchange FromExchange { get; }
		public IWallet ToWallet { get; }
		public Coin StartCoin { get; }
		public Coin EndCoin { get; }

		public WalletTransferStep(IExchange fromExchange, IWallet toWallet, Coin coin)
		{
			FromExchange = fromExchange;
			ToWallet = toWallet;
			StartCoin = coin;
			EndCoin = coin;
		}

		public async Task<decimal> Execute(decimal amount)
		{
			var withdraws = await FromExchange.WithdrawsCollection();
			var coinWithdraw = withdraws.First(w => w.Coin == StartCoin);
			await coinWithdraw.WithdrawTo(ToWallet, amount);
			return await coinWithdraw.Fee(amount);
		}

		public Task<decimal> TestRun(decimal amount)
		{
			return new WithdrawCost(StartCoin, FromExchange).Ratio(amount);
		}

		public override string ToString()
		{
			var stepCharacter = new StepCharacter();
			return $"E:{FromExchange.Name}{StartCoin}{stepCharacter.ToString(this)}W:{EndCoin};";
		}
	}
}
