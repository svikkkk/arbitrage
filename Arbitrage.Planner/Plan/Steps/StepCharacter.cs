﻿using Arbitrage.Planner.Interfaces;

namespace Arbitrage.Planner.Plan.Steps
{
    public class StepCharacter
    {
	    public string ToString(IPlanStep planStep)
	    {
		    return planStep is TradeStep ? "\u2192" : "\u2229";
	    }
	}
}
