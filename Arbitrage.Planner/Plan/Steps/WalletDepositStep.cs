﻿using System.Linq;
using System.Threading.Tasks;
using Arbitrage.Models;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Wallet;
using Arbitrage.Planner.Costs;
using Arbitrage.Planner.Interfaces;

namespace Arbitrage.Planner.Plan.Steps
{
	public class WalletDepositStep : IPlanStep
	{
		public IExchange ToExchange { get; }
		public IWallet FromWallet { get; }

		public Coin StartCoin { get; }
		public Coin EndCoin { get; }

		public WalletDepositStep(IWallet fromWallet, IExchange toExchange, Coin coin)
		{
			FromWallet = fromWallet;
			ToExchange = toExchange;
			StartCoin = coin;
			EndCoin = coin;
		}

		public async Task<decimal> Execute(decimal amount)
		{
			var deposits = await ToExchange.DepositsCollection();
			var coinDeposit = deposits.First(w => w.Coin == StartCoin);
			await FromWallet.DepositTo(ToExchange, amount);
			return coinDeposit.Fee(amount);
		}

		public Task<decimal> TestRun(decimal amount)
		{
			return new DepositCost(StartCoin, ToExchange).Ratio(amount);
		}

		public override string ToString()
		{
			var stepCharacter = new StepCharacter();
			return $"W:{StartCoin.Code}{stepCharacter.ToString(this)}{ToExchange.Name}:{EndCoin.Code};";
		}
	}
}
