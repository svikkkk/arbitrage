﻿using Arbitrage.Models;
using Arbitrage.Models.Exchange;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Orders;
using Arbitrage.Planner.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Arbitrage.Planner.Plan.Steps
{
	public class TradeStep : IPlanStep
	{
		private readonly bool _needCalculateOrdersActualVolume;

		public IExchange Exchange { get; }
		public Coin StartCoin { get; }
		public Coin EndCoin { get; }
		public decimal StartAmount { get; private set; }
		public decimal AmountAfterTrade { get; private set; }
		public decimal Ratio => StartAmount / AmountAfterTrade;

		public IList<Order> OrdersToTake
		{
			get;
			private set;
		}

		public TradeStep(IExchange exchange, Coin startCoin, Coin endCoin) : this(exchange, startCoin, endCoin, false)
		{
		}

		private TradeStep(IExchange exchange, Coin startCoin, Coin endCoin, bool needCalculateOrdersActualVolume)
		{
			_needCalculateOrdersActualVolume = needCalculateOrdersActualVolume;
			Exchange = exchange;
			StartCoin = startCoin;
			EndCoin = endCoin;
		}

		public async Task<decimal> Execute(decimal amount)
		{
			// await Exchange.Trade(new Order { Buy = StartCoin, BuyAmount = amount, Sell = EndCoin });
			var markets = await Exchange.Markets();
			return await markets.Market(StartCoin, EndCoin, false)
				.Rate(MarketRate.BasedOnOrders)
				.AvailableAmountToBuy(amount);
		}

		public async Task<decimal> TestRun(decimal amount)
		{
			StartAmount = amount;
			var market = await Market(_needCalculateOrdersActualVolume);
			OrdersToTake = await market.OrdersToTake(amount, decimal.MaxValue);
			AmountAfterTrade = OrdersToTake.Sum(o => o.BuyAmount);
			return AmountAfterTrade;
		}

		public async Task<IMarket> Market(bool needCalculateOrdersActualVolume)
		{
			var markets = await Exchange.Markets();
			return markets.Market(StartCoin, EndCoin, needCalculateOrdersActualVolume);
		}

		public TradeStep WithOrderActualVolume()
		{
			return new TradeStep(Exchange, StartCoin, EndCoin, true);
		}

		public override string ToString()
		{
			var stepCharacter = new StepCharacter();
			return $"{Exchange.Name} {StartCoin.Code} {StartAmount:F5} {stepCharacter.ToString(this)} {EndCoin.Code} {AmountAfterTrade:F5}";
		}
	}
}
