﻿using System.Linq;
using System.Threading.Tasks;
using Arbitrage.Models;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Planner.Costs;
using Arbitrage.Planner.Interfaces;

namespace Arbitrage.Planner.Plan.Steps
{
	public class TransferStep : IPlanStep
	{
		private decimal _xFactor;
		private decimal _transferAmount;

		public IExchange FromExchange { get; }
		public IExchange ToExchange { get; }

		public Coin StartCoin { get; }
		public Coin EndCoin { get; }

		public TransferStep(IExchange fromExchange, IExchange toExchange, Coin coin)
		{
			FromExchange = fromExchange;
			ToExchange = toExchange;
			StartCoin = coin;
			EndCoin = coin;
		}

		public async Task<decimal> Execute(decimal amount)
		{
			var withdraws = await FromExchange.WithdrawsCollection();
			var coinWithdraw = withdraws.First(w => w.Coin == StartCoin);
			await coinWithdraw.WithdrawTo(ToExchange, amount);
			return await coinWithdraw.Fee(amount);
		}

		public async Task<decimal> TestRun(decimal amount)
		{
			_transferAmount = amount;
			_xFactor = await new TransferCost(StartCoin, FromExchange, ToExchange).Ratio(amount);
			return _xFactor;
		}

		public override string ToString()
		{
			var stepCharacter = new StepCharacter();
			return $"{FromExchange.Name} {StartCoin.Code} {_transferAmount:F5} {stepCharacter.ToString(this)} {ToExchange.Name} {EndCoin.Code} {_xFactor:F5}";
		}
	}
}
