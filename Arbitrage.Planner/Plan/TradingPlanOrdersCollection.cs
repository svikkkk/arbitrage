﻿using Arbitrage.Models.Orders;
using Arbitrage.Planner.Interfaces;
using Arbitrage.Planner.Plan.Steps;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Arbitrage.Planner.Plan
{
	public class TradingPlanOrdersCollection
    {
	    private readonly TradingPlan _tradingPlan;
	    private readonly bool _needCalculateOrdersActualVolume;

	    public TradingPlanOrdersCollection(TradingPlan tradingPlan, bool needCalculateOrdersActualVolume)
	    {
		    _tradingPlan = tradingPlan;
		    _needCalculateOrdersActualVolume = needCalculateOrdersActualVolume;
	    }

	    public async Task<Dictionary<IPlanStep, IList<Order>>> ToStepOrders(decimal amount)
	    {
			var orders = new Dictionary<IPlanStep, IList<Order>>();
		    foreach (var step in _tradingPlan.Steps)
		    {
			    if (step is TradeStep tradeStep)
			    {
				    if (_needCalculateOrdersActualVolume)
				    {
					    tradeStep = tradeStep.WithOrderActualVolume();
				    }

					amount = await tradeStep.TestRun(amount);
					orders.Add(step, tradeStep.OrdersToTake);
			    }
			    else
				{
					amount = await step.TestRun(amount);
				}
		    }

		    return orders;
	    }
	}
}
