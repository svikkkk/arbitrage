﻿namespace Arbitrage.Planner.HashCode
{
	public interface IHashCode
	{
		string GetHashCodeAndConvertToX2(string input);

		string GetHashCode(string input);
	}
}