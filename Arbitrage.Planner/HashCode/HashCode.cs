﻿using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Arbitrage.Planner.HashCode
{
	public class HashCode : IHashCode
	{
		private static readonly Encoding Encoding;

		static HashCode()
		{
			Encoding = Encoding.UTF8;
		}

		public string GetHashCodeAndConvertToX2(string input)
		{
			var hash = GetHashCodeInternal(input);
			return HexStringFromBytes(hash);
		}

		public string GetHashCode(string input)
		{
			var hashCodeInBytes = GetHashCodeInternal(input);
			return Encoding.GetString(hashCodeInBytes);
		}

		private static byte[] GetHashCodeInternal(string input)
		{
			byte[] hash;
			using (var sha1 = SHA1.Create())
			{
				hash = sha1.ComputeHash(Encoding.GetBytes(input));
			}

			return hash;
		}

		private static string HexStringFromBytes(IEnumerable<byte> bytes)
		{
			var sb = new StringBuilder();
			foreach (var b in bytes)
			{
				// can be "x2" if you want lowercase
				var hex = b.ToString("X2");
				sb.Append(hex);
			}

			return sb.ToString();
		}
	}
}
