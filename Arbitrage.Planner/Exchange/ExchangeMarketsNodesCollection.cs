﻿using Arbitrage.Models.Exchange.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Arbitrage.Planner.Exchange
{
	public class ExchangeMarketNodesCollection
	{
		private readonly IList<Models.Coin> _coinsToTrade;
		private readonly IList<IExchange> _exchanges;

		public ExchangeMarketNodesCollection(IList<Models.Coin> coinsToTrade, IList<IExchange> exchanges)
		{
			_coinsToTrade = coinsToTrade;
			_exchanges = exchanges;
		}

		public async Task<IList<ExchangeNode>> ToList()
		{
			var exchangesNodes = new List<ExchangeNode>();
			foreach (var exchange in _exchanges)
			{
				var sellingMarketNodes = new MarketSellingNodesCollection(_coinsToTrade, exchange);
				exchangesNodes.AddRange(await new MarketTradingNodesCollection(exchange, sellingMarketNodes).ToList());
			}

			return WithTransfersBetweenExchanges(exchangesNodes);
		}

		public IList<IExchange> Exchanges()
		{
			return _exchanges;
		}

		private IList<ExchangeNode> WithTransfersBetweenExchanges(IList<ExchangeNode> exchangeNodes)
		{
			var groupedByCoinExchnageNodes = new CoinExchangeNodesCollection(exchangeNodes).GroupByCoin();
			foreach (var coinFullName in groupedByCoinExchnageNodes.Keys)
			{
				var coinExchangesNodes = groupedByCoinExchnageNodes[coinFullName];
				for (int currentExchangeNodeIndex = 0; currentExchangeNodeIndex < coinExchangesNodes.Count - 1; currentExchangeNodeIndex++)
				{
					for (int nextExchangeNodeIndex = currentExchangeNodeIndex + 1; nextExchangeNodeIndex < coinExchangesNodes.Count; nextExchangeNodeIndex++)
					{
						var currentExchangeNode = coinExchangesNodes[currentExchangeNodeIndex];
						var nextExchangeNode = coinExchangesNodes[nextExchangeNodeIndex];

						currentExchangeNode.TransferPath(nextExchangeNode);
						nextExchangeNode.TransferPath(currentExchangeNode);
					}
				}
			}

			return exchangeNodes;
		}
	}
}
