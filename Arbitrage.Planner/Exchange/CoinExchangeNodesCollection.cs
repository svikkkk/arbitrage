﻿using System.Collections.Generic;

namespace Arbitrage.Planner.Exchange
{
	class CoinExchangeNodesCollection
	{
		private readonly IList<ExchangeNode> _exchangeNodes;

		public CoinExchangeNodesCollection(IList<ExchangeNode> exchangeNodes)
		{
			_exchangeNodes = exchangeNodes;
		}

		/// <summary>
		/// Coin Full Name as a key
		/// </summary>
		/// <returns></returns>
		public Dictionary<string, IList<ExchangeNode>> GroupByCoin()
		{
			var coins = new Dictionary<string, IList<ExchangeNode>>(_exchangeNodes.Count);
			foreach (var exchangeNode in _exchangeNodes)
			{
				var coinCode = exchangeNode.Coin.Code;
				if (!coins.ContainsKey(coinCode))
				{
					coins.Add(coinCode, new List<ExchangeNode>());
				}

				coins[coinCode].Add(exchangeNode);
			}

			return coins;
		}
	}
}
