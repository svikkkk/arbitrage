﻿using Arbitrage.Models.Exchange;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Planner.Costs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Arbitrage.Planner.Exchange
{
	public class MarketTradingNodesCollection
	{
		private readonly IExchange _exchange;
		private readonly MarketSellingNodesCollection _exchangeSellingMarketNodesCollection;

		public MarketTradingNodesCollection(IExchange exchange, MarketSellingNodesCollection exchangeSellingMarketNodesCollection)
		{
			_exchange = exchange;
			_exchangeSellingMarketNodesCollection = exchangeSellingMarketNodesCollection;
		}

		public async Task<IList<ExchangeNode>> ToList()
		{
			var sellingMarketNodesList = await _exchangeSellingMarketNodesCollection.ToList();
			var markets = await _exchange.Markets();
			foreach (var market in markets.ToList())
			{
				var marketTradingNodes = MarketTradingNodes(market, sellingMarketNodesList);
				if (marketTradingNodes.To != null && marketTradingNodes.From != null)
				{
					MarketTradingPath(marketTradingNodes, new MarketCost(
						_exchange, 
						marketTradingNodes.From.Coin, 
						marketTradingNodes.To.Coin, 
						MarketRate.Approximate));
				}
			}

			return sellingMarketNodesList;
		}

		private void MarketTradingPath((ExchangeNode From, ExchangeNode To) marketNodes, MarketCost marketCost)
		{
			if (marketNodes.From != null && marketNodes.To != null)
			{
				marketNodes.From.AddNeighbor(marketNodes.To, marketCost);
			}
		}

		private (ExchangeNode From, ExchangeNode To) MarketTradingNodes(IMarket market, IList<ExchangeNode> marketNodes)
		{
			ExchangeNode sellingMarketNode = null;
			ExchangeNode buyingMarketNode = null;
			foreach (var marketNode in marketNodes)
			{
				if (market.TradePair.CoinToSell.Equals(marketNode.Coin))
				{
					sellingMarketNode = marketNode;
				}

				if (market.TradePair.CoinToBuy.Equals(marketNode.Coin))
				{
					buyingMarketNode = marketNode;
				}

				if (sellingMarketNode != null && buyingMarketNode != null)
				{
					return (From: sellingMarketNode, To: buyingMarketNode);
				}
			}

			return (From: sellingMarketNode, To: buyingMarketNode);
		}
	}
}
