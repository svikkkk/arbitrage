﻿using Arbitrage.Models;
using Arbitrage.Models.Exchange.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Arbitrage.Planner.Exchange
{
	public class MarketSellingNodesCollection
	{
		private readonly IList<Coin> _coinsToTrade;
		private readonly IExchange _exchange;

		public MarketSellingNodesCollection(IList<Coin> coinsToTrade, IExchange exchange)
		{
			_coinsToTrade = coinsToTrade;
			_exchange = exchange;
		}

		public async Task<List<ExchangeNode>> ToList()
		{
			var sellingMarketNodes = new List<ExchangeNode>();

			var exchangeMarkets = await _exchange.Markets();
			foreach (var market in exchangeMarkets.ToList())
			{
				if (IsCoinSupported(market.TradePair.CoinToSell) && !Contains(sellingMarketNodes, market.TradePair.CoinToSell))
				{
					sellingMarketNodes.Add(new ExchangeNode(_exchange, market.TradePair.CoinToSell));
				}
			}

			return sellingMarketNodes;
		}

		private bool Contains(List<ExchangeNode> nodes, Coin coin)
		{
			return nodes.Count(n => n.Coin.Equals(coin)) > 0;
		}

		private bool IsCoinSupported(Coin coin)
		{
			var isCoinSupported = true;
			if (_coinsToTrade != null)
			{
				isCoinSupported = _coinsToTrade.Any(coinToTrade => coinToTrade.Equals(coin));
			}

			return isCoinSupported;
		}
	}
}
