﻿using Arbitrage.Models;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Planner.Costs;
using Arbitrage.Planner.Graph;
using Arbitrage.Planner.Interfaces;
using Arbitrage.Planner.Plan.Steps;
using Arbitrage.Planner.Wallet;

namespace Arbitrage.Planner.Exchange
{
	public class ExchangeNodeValue : INodeValue
	{
		public IExchange Exchange { get; }

		public Coin Coin { get; }

		public ExchangeNodeValue(IExchange exchange, Coin coin)
		{
			Exchange = exchange;
			Coin = coin;
		}

		public IPlanStep ToAction(INodeValue nextNode)
		{
			if (nextNode is ExchangeNodeValue targetExchangeNodeValue)
			{
				if (Exchange.Name == targetExchangeNodeValue.Exchange.Name)
				{
					return new TradeStep(Exchange, Coin, targetExchangeNodeValue.Coin);
				}

				return new TransferStep(Exchange, targetExchangeNodeValue.Exchange, Coin);
			}

			return new WalletTransferStep(Exchange, ((WalletNodeValue)nextNode).Wallet, Coin);
		}

		public bool Equals(INodeValue node)
		{
			if (node is ExchangeNodeValue exchangeNode)
			{
				return Coin.Equals(exchangeNode.Coin); // && Exchange == exchangeNode.Exchange;
			}

			return false;
		}
	}

	public class ExchangeNode : Node
	{

		public IExchange Exchange => ((ExchangeNodeValue)Value).Exchange;

		public Coin Coin => ((ExchangeNodeValue)Value).Coin;

		public ExchangeNode(IExchange exchange, Coin coin)
			: this(new ExchangeNodeValue(exchange, coin))
		{
		}

		public ExchangeNode(ExchangeNodeValue value) : base(value)
		{
		}

		public void WithdrawPath(WalletNode walletNode)
		{
			AddNeighbor(walletNode, new WithdrawCost(walletNode.Wallet.Coin, Exchange));
		}

		public void TransferPath(ExchangeNode transferToExchangeNode)
		{
			AddNeighbor(transferToExchangeNode, new TransferCost(Coin, Exchange, transferToExchangeNode.Exchange));
		}
	}
}
