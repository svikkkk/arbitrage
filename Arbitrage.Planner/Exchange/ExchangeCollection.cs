﻿using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Planner.Graph;
using System.Collections.Generic;

namespace Arbitrage.Planner.Exchange
{
	class ExchangeCollection
	{
		private readonly IList<IExchange> _exchanges;

		public ExchangeCollection(IList<Node> nodes) : this(Exchnages(nodes))
		{
		}

		public ExchangeCollection(IList<IExchange> exchanges)
		{
			_exchanges = exchanges;
		}

		public IList<IExchange> ToList()
		{
			return _exchanges;
		}

		private static IList<IExchange> Exchnages(IList<Node> nodes)
		{
			var exchanges = new List<IExchange>();
			foreach (var node in nodes)
			{
				if (node.Value is ExchangeNodeValue exchangeNodeValue)
				{
					if (!exchanges.Contains(exchangeNodeValue.Exchange))
					{
						exchanges.Add(exchangeNodeValue.Exchange);
					}
				}
			}

			return exchanges;
		}
	}
}
