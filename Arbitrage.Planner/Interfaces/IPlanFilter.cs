﻿using Arbitrage.Planner.Plan;

namespace Arbitrage.Planner.Interfaces
{
    public interface IPlanFilter
    {
	    bool Pass(TradingPlan plan);
    }
}
