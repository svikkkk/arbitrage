﻿using Arbitrage.Models;
using System.Threading.Tasks;

namespace Arbitrage.Planner.Interfaces
{
	public interface IPlanStep
	{
		Coin StartCoin { get; }

		Coin EndCoin { get; }

		Task<decimal> Execute(decimal startAmount);

		Task<decimal> TestRun(decimal startAmount);
	}
}
