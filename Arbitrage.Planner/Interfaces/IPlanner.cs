﻿using Arbitrage.Models;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Wallet;
using Arbitrage.Planner.Plan;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Arbitrage.Planner.Interfaces
{
	public interface IPlanner
	{
		Task<TradingPlan> Exit(TradingPlan plan);

		Task Update(TradingPlan plan);

		Task<IEnumerable<TradingPlan>> Plans(IExchange startExchange, Coin startCoin, decimal amount, int maxSteps);

		Task<IEnumerable<TradingPlan>> Plans(IWallet wallet, decimal amount, int maxSteps);

		Task<IEnumerable<TradingPlan>> Plans(TradingPlan plan, Coin finishCoin, int maxSteps);
	}
}
