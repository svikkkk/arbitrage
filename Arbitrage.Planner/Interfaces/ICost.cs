﻿using System.Threading.Tasks;

namespace Arbitrage.Planner.Interfaces
{
	public interface ICost
	{
		Task<decimal> Ratio(decimal amount);
	}
}
