﻿using Arbitrage.Models;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Planner.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace Arbitrage.Planner.Costs
{
	public class WithdrawCost : ICost
	{
		private readonly Coin _coin;
		private readonly IExchange _exchange;

		public WithdrawCost(Coin coin, IExchange exchange)
		{
			_coin = coin;
			_exchange = exchange;
		}

		public async Task<decimal> Ratio(decimal amount)
		{
			var coinWithdraw = await CoinWithdraw();
			var minimalWithdraw = await coinWithdraw.MinimalWithdraw();
			if (minimalWithdraw.HasValue && amount < minimalWithdraw)
			{
				// that means we can't transfer such small amount
				return 0m;
			}

			var fee = await coinWithdraw.Fee(amount);
			return amount - fee;
		}

		private async Task<IExchangeWithdraw> CoinWithdraw()
		{
			var withdrawCollection = await _exchange.WithdrawsCollection();
			return withdrawCollection.First(w => w.Coin.Equals(_coin));
		}
	}
}
