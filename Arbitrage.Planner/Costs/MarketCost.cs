﻿using Arbitrage.Models;
using Arbitrage.Models.Exchange;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Planner.Interfaces;
using System.Threading.Tasks;

namespace Arbitrage.Planner.Costs
{
	public class MarketCost : ICost
	{
		private readonly IExchange _exchange;
		private readonly Coin _coinFrom;
		private readonly Coin _coinTo;
		private readonly MarketRate _marketRate;

		public MarketCost(IExchange exchange, Coin coinFrom, Coin coinTo, MarketRate marketRate)
		{
			_exchange = exchange;
			_coinFrom = coinFrom;
			_coinTo = coinTo;
			_marketRate = marketRate;
		}

		public async Task<decimal> Ratio(decimal sellingAmount)
		{
			var exchangeMarkets = await _exchange.Markets();
			return await exchangeMarkets.Market(_coinFrom, _coinTo, _marketRate == MarketRate.OrdersWithActualVolume)
				.Rate(_marketRate)
				.AvailableAmountToBuy(sellingAmount);
		}
	}
}
