﻿using Arbitrage.Models;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Planner.Interfaces;
using System.Threading.Tasks;

namespace Arbitrage.Planner.Costs
{
	public class TransferCost : ICost
	{
		private readonly Coin _coin;
		private readonly IExchange _exchangeFrom;
		private readonly IExchange _exchangeTo;

		public TransferCost(Coin coin, IExchange exchangeFrom, IExchange exchangeTo)
		{
			_coin = coin;
			_exchangeFrom = exchangeFrom;
			_exchangeTo = exchangeTo;
		}

		public async Task<decimal> Ratio(decimal amount)
		{
			var amountAfterWithdraw = await new WithdrawCost(_coin, _exchangeFrom).Ratio(amount);
			return amountAfterWithdraw;
		}
	}
}
