﻿using Arbitrage.Models;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Planner.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace Arbitrage.Planner.Costs
{
	public class DepositCost : ICost
	{
		private readonly Coin _coin;
		private readonly IExchange _exchange;

		public DepositCost(Coin coin, IExchange exchange)
		{
			_coin = coin;
			_exchange = exchange;
		}

		public async Task<decimal> Ratio(decimal amount)
		{
			var coinDeposit = await CoinDeposit();
			var fee = coinDeposit.Fee(amount);
			return amount - fee;
		}

		private async Task<IExchangeDeposit> CoinDeposit()
		{
			var depositsCollection = await _exchange.DepositsCollection();

			return depositsCollection.First(d => d.Coin.Equals(_coin));
		}
	}
}
