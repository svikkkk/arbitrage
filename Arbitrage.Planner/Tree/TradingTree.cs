﻿using Arbitrage.Planner.Graph;
using Arbitrage.Planner.Interfaces;
using Arbitrage.Planner.Plan;
using Arbitrage.Planner.Tree.Serialization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Arbitrage.Models;

namespace Arbitrage.Planner.Tree
{
	public class TradingTree
	{
		private readonly TradingGraph _tradingGraph;

		public event Action<TradingPlan> PlanFound = delegate { };

		public Node RootNode { get; }

		public TradingTree(string jsonTree, TradingGraph tradingGraph)
			: this(DeserializeTree(jsonTree), tradingGraph)
		{
		}

		public TradingTree(Graph.Node rootNode, TradingGraph tradingGraph) : this(new Node(rootNode.Id), tradingGraph)
		{
		}

		public TradingTree(Node rootNode, TradingGraph tradingGraph)
		{
			_tradingGraph = tradingGraph;
			RootNode = rootNode;
		}

		public string ToJson()
		{
			return JsonConvert.SerializeObject(RootNode, new TreeNodeSerializationConverter());
		}

		public Task<List<TradingPlan>> ToGoodPlansList(decimal startAmount, int topLimit = 0)
		{
			var goodPlansWithOptimalAmountVolume = new OptimalAmountGoodPlansCollection(new GoodPlansCollection(this, startAmount));

			goodPlansWithOptimalAmountVolume.PlanFound += GoodPlansWithOptimalAmountVolume_PlanFound;

			return goodPlansWithOptimalAmountVolume.ToList(topLimit);
		}

		public Task<AnalyzationResult> GoodPlansCalculation(decimal startAmount, IPlanFilter filter)
		{
			var goodPlansWithOptimalAmountVolume = new OptimalAmountGoodPlansCalculation(new GoodPlansCollection(this, startAmount), filter);
			goodPlansWithOptimalAmountVolume.PlanFound += GoodPlansWithOptimalAmountVolume_PlanFound;
			return goodPlansWithOptimalAmountVolume.Calculate();
		}

		public TradingTreeDetails Details()
		{
			return new TradingTreeDetails(this, _tradingGraph);
		}

		public async Task<List<IPlanStep>> PlanActions(IList<int> nodesId)
		{
			var planActions = new List<IPlanStep>();
			for (var nodeIndex = 0; nodeIndex < nodesId.Count - 1; nodeIndex++)
			{
				var startNode = await _tradingGraph.Node(nodesId[nodeIndex]);
				var endNode = await _tradingGraph.Node(nodesId[nodeIndex + 1]);
				planActions.Add(startNode.Value.ToAction(endNode.Value));
			}

			return planActions;
		}

		public bool IsLeafNode(Node currentNode)
		{
		    return !currentNode.Children.Any();
		}

		public async Task<ICost> Cost(int nodeId, int childId)
		{
			var node = await _tradingGraph.Node(nodeId);
			return node.Cost(childId);
		}

		private void GoodPlansWithOptimalAmountVolume_PlanFound(TradingPlan plan)
		{
			PlanFound.Invoke(plan);
		}

		private static Node DeserializeTree(string jsonTree)
		{
			return JsonConvert.DeserializeObject<Node>(jsonTree, new TreeNodeDeserializationConverter());
		}
	}
}
