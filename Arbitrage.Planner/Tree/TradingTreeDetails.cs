﻿using System;
using System.Threading.Tasks;
using Arbitrage.Planner.Graph;
using Arbitrage.Planner.Interfaces;

namespace Arbitrage.Planner.Tree
{
    public class TradingTreeDetails
    {
	    private readonly TradingTree _tradingTree;
	    private readonly TradingGraph _tradingGraph;

	    public TradingTreeDetails(TradingTree tradingTree, TradingGraph tradingGraph)
	    {
		    _tradingTree = tradingTree;
		    _tradingGraph = tradingGraph;
	    }

		public int Depth()
		{
			return Depth(_tradingTree.RootNode, 0);
		}

		public async Task<int> Variants()
		{
			var rootNode = await _tradingGraph.Node(_tradingTree.RootNode.DataNodeId);
			return await ChildrenFits(_tradingTree.RootNode, async (parent, node) =>
			{
				var graphNode = await _tradingGraph.Node(node.DataNodeId);
				return graphNode.Value.Equals(rootNode.Value);
			});
		}

		public async Task<int> Costs<T>() where T : ICost
		{
			var costType = typeof(T);
			var childrenCount = await ChildrenFits(_tradingTree.RootNode, async (parent, node) =>
			{
				var parentNode = await _tradingGraph.Node(parent.DataNodeId);
				var nodeCost = parentNode.Cost(node.DataNodeId);
				return nodeCost.GetType() == costType;
			});

			//			var rootNode = await _tradingGraph.Node(RootNode.DataNodeId);
			//			
			//			if (RootNode.Cost != null && RootNode.Cost.GetType() == costType)
			//			{
			//				childrenCount++;
			//			}

			return childrenCount;
		}

		public async Task<int> DataNodes<T>() where T : INodeValue
		{
			var dataNodeType = typeof(T);
			var childrenCount = await ChildrenFits(_tradingTree.RootNode, async (parent, node) =>
			{
				var graphNode = await _tradingGraph.Node(node.DataNodeId);
				return graphNode.Value.GetType() == dataNodeType;
			});

			var rootNode = await _tradingGraph.Node(_tradingTree.RootNode.DataNodeId);
			if (rootNode.Value.GetType() == dataNodeType)
			{
				childrenCount++;
			}

			return childrenCount;
		}

		private int Depth(Node node, int depth)
		{
			var childrenMaxDepth = depth;
			foreach (var nodeChild in node.Children)
			{
				var childDepth = Depth(nodeChild, depth + 1);
				if (childDepth > childrenMaxDepth)
				{
					childrenMaxDepth = childDepth;
				}
			}

			return childrenMaxDepth;
		}

		private async Task<int> ChildrenFits(Node node, Func<Node, Node, Task<bool>> isNoteFit)
		{
			int count = 0;
			foreach (var nodeChild in node.Children)
			{
				if (await isNoteFit(node, nodeChild))
				{
					count++;
				}

				count += await ChildrenFits(nodeChild, isNoteFit);
			}

			return count;
		}
	}
}
