﻿using System.Threading;
using Arbitrage.Planner.Graph;

namespace Arbitrage.Planner.Tree
{
	public class OneWayTradingTreeBuilder : TreeBuilder
	{
		public OneWayTradingTreeBuilder(Graph.Node rootNode, TradingGraph tradingGraph, CancellationTokenSource cancellationTokenSource) 
			: base(rootNode, tradingGraph, cancellationTokenSource)
		{
		}

		protected override bool IsCorrectNode(BackTraceItem backTraceItem, int currentNodeId, int neighborNodeId)
		{
			return !backTraceItem.HasNode(neighborNodeId);
		}
	}
}
