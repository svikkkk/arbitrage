﻿using System.Collections.Generic;
using System.Linq;

namespace Arbitrage.Planner.Tree
{
	public class Node
	{
		public int DataNodeId { get; }

		public List<Node> Children { get; set; } = new List<Node>();

		public Node(int dataNodeId)
		{
			DataNodeId = dataNodeId;
		}

		public Node Child(int dataId)
		{
			lock (Children)
			{
				return Children.FirstOrDefault(child => child.DataNodeId == dataId);
			}
		}

		public Node Add(int dataId)
		{
			lock (Children)
			{
				var alreadyAddedChild = Child(dataId);
				if (alreadyAddedChild == null)
				{
					var child = new Node(dataId);
					Children.Add(child);
					return child;
				}

				return alreadyAddedChild;
			}
		}
	}
}
