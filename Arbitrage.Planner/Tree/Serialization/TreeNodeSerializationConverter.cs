﻿using Newtonsoft.Json;
using System;

namespace Arbitrage.Planner.Tree.Serialization
{
	class TreeNodeSerializationConverter : JsonConverter
	{
		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}

		public override bool CanConvert(Type objectType)
		{
			return typeof(Node).IsAssignableFrom(objectType);
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			var node = (Node)value;
			serializer.Serialize(writer, new
			{
				Id = node.DataNodeId,
				Cldrn = node.Children
			});
		}
	}
}
