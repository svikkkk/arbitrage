﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Arbitrage.Planner.Tree.Serialization
{
	class TreeNodeDeserializationConverter : JsonConverter
	{
		private readonly Stack<Node> _nodesStack = new Stack<Node>();

		public override bool CanConvert(Type objectType)
		{
			return typeof(Node).IsAssignableFrom(objectType);
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (reader.TokenType != JsonToken.Null)
			{
				if (reader.TokenType == JsonToken.StartObject)
				{
					JObject item = JObject.Load(reader);
					var nodeValue = item["Id"].Value<int>();
					var node = new Node(nodeValue);
					_nodesStack.Push(node);
					node.Children = item["Cldrn"].ToObject<List<Node>>(serializer);
					_nodesStack.Pop();
					return node;
				}

				if (reader.TokenType == JsonToken.StartArray)
				{
					return JToken.Load(reader).ToObject<List<Node>>();
				}
			}

			return null;
		}
	}
}
