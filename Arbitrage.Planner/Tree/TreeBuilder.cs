﻿using System;
using Arbitrage.Planner.Graph;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Arbitrage.Planner.Tree
{
	public class TreeBuilder
	{
		private readonly Graph.Node _rootNode;
		private readonly TradingGraph _tradingGraph;
		private readonly CancellationTokenSource _cancellationTokenSource;
		private Queue<BackTraceItem> _nodeProcessingQueue;

		private TradingTree _tradingTree;

		private long _workingTasksCount;
		private int _maxDepth;

		public TreeBuilder(Graph.Node rootNode, TradingGraph tradingGraph, CancellationTokenSource cancellationTokenSource)
		{
			_rootNode = rootNode;
			_tradingGraph = tradingGraph;
			_cancellationTokenSource = cancellationTokenSource;
		}

		public async Task<TradingTree> ToTree(int maxDepth)
		{
			_nodeProcessingQueue = new Queue<BackTraceItem>();
			_tradingTree = new TradingTree(_rootNode, _tradingGraph);
			_maxDepth = maxDepth;

			Interlocked.Increment(ref _workingTasksCount);
			var pushNodesTask = Task.Run(() => PushNodesToQueue(_rootNode, null, 0)).ContinueWith(t =>
			{
				Interlocked.Decrement(ref _workingTasksCount);
			});

			await ProcessingNodes();
			await pushNodesTask;
			return _tradingTree;
		}

		protected virtual bool IsCorrectNode(BackTraceItem backTraceItem, int currentNodeId, int neighborNodeId)
		{
			return !backTraceItem.IsCircularStep(currentNodeId, neighborNodeId);
		}

		private Task ProcessingNodes()
		{
			while (Interlocked.Read(ref _workingTasksCount) > 0)
			{
				while (true)
				{
					BackTraceItem nodeItem;
					_cancellationTokenSource.Token.ThrowIfCancellationRequested();
					lock (_nodeProcessingQueue)
					{
						if (_nodeProcessingQueue.Count > 0)
						{
							nodeItem = _nodeProcessingQueue.Dequeue();
						}
						else
						{
							break;
						}
					}

					if (nodeItem != null)
					{
						Interlocked.Increment(ref _workingTasksCount);
						Task.Run(() => ProcessNode(nodeItem)).ContinueWith((t) => Interlocked.Decrement(ref _workingTasksCount));
					}
				}
			}

			return Task.CompletedTask;
		}

		private void ProcessNode(BackTraceItem nodeItem)
		{
			nodeItem.BuildTree(_tradingTree);
		}

		private async Task PushNodesToQueue(Graph.Node currentNode, BackTraceItem parentItem, int depth)
		{
			_cancellationTokenSource.Token.ThrowIfCancellationRequested();
			var backTraceItem = new BackTraceItem(currentNode.Id, parentItem);
			if (IsLeafNode(currentNode, parentItem))
			{
				if (HasAtLeastTwoSteps(parentItem))
				{
					lock (_nodeProcessingQueue)
					{
						_nodeProcessingQueue.Enqueue(backTraceItem);
					}
				}

				return;
			}

			if (IsMaxDepthExceeded(depth))
			{
				return;
			}

			var tasksList = new List<Task>();

			for (int neighborIndex = 0; neighborIndex < currentNode.Neighbors.Count; neighborIndex++)
			{
				var neighborNode = currentNode.Neighbors[neighborIndex];
				if (IsCorrectNode(backTraceItem, currentNode.Id, neighborNode.Id))
				{
					tasksList.Add(PushNodesToQueue(neighborNode, backTraceItem, depth + 1));
				}
			}

			await Task.WhenAll(tasksList);
		}

		/// <summary>
		/// Because 2 steps is minimum possible trading plan
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		private bool HasAtLeastTwoSteps(BackTraceItem item)
		{
			return item.Count >= 2;
		}

		private bool IsLeafNode(Graph.Node currentNode, BackTraceItem parentItem)
		{
			return parentItem != null && currentNode.Value.Equals(_rootNode.Value);
		}

		private bool IsMaxDepthExceeded(int depth)
		{
			return _maxDepth > 0 && depth >= _maxDepth;
		}
	}
}
