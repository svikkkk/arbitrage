﻿using Arbitrage.Planner.Graph;
using Arbitrage.Planner.Tree;
using System.Threading.Tasks;
using Node = Arbitrage.Planner.Graph.Node;

namespace Arbitrage.Planner.Cache
{
	public interface ITreeCache
	{
		Task Add(TradingGraph tradingGraph, TradingTree tree, int maxDepth);

		Task<TradingTree> Get(TradingGraph tradingGraph, Node rootNode, int maxDepth);
	}
}
