﻿using Arbitrage.Planner.Exchange;
using Arbitrage.Planner.Graph;
using Arbitrage.Planner.Tree;
using Arbitrage.Planner.Wallet;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Node = Arbitrage.Planner.Graph.Node;

namespace Arbitrage.Planner.Cache
{
	public class TreeCache : ITreeCache
	{
		private readonly string _cacheFoderPath;

		public TreeCache(string cacheFoderPath)
		{
			_cacheFoderPath = cacheFoderPath;
		}

		public async Task Add(TradingGraph tradingGraph, TradingTree tree, int maxDepth)
		{
			var graphFolder = await Folder(tradingGraph);
			var treeName = await TreeFileName(tradingGraph, tree);
			var treePath = TreePath(graphFolder.Name, treeName, maxDepth);
			using (var stream = File.CreateText(treePath))
			{
				await stream.WriteAsync(tree.ToJson());
			}
		}

		public async Task<TradingTree> Get(TradingGraph tradingGraph, Node rootNode, int maxDepth)
		{
			var graphFolder = await Folder(tradingGraph);
			var treeName = TreeFileName(rootNode);
			var treePath = TreePath(graphFolder.Name, treeName, maxDepth);
			if (File.Exists(treePath))
			{
				using (var stream = File.OpenText(treePath))
				{
					string data = await stream.ReadToEndAsync();
					return new TradingTree(data, tradingGraph);
				}
			}

			return null;
		}

		private string TreePath(string graphFolder, string treeName, int maxDepth)
		{
			return $"{_cacheFoderPath}/{graphFolder}/{treeName}_{maxDepth}.json";
		}

		private async Task<string> TreeFileName(TradingGraph tradingGraph, TradingTree tree)
		{
			var graphNodes = await tradingGraph.ToNodesList();
			var node = graphNodes.First(n => n.Id == tree.RootNode.DataNodeId);
			return TreeFileName(node);
		}

		private string TreeFileName(Node node)
		{
			return new NodeHashCode(node, new HashCode.HashCode()).ToString();
		}

		private async Task<DirectoryInfo> Folder(TradingGraph tradingGraph)
		{
			var folderName = await tradingGraph.ToHashCode();
			var directoryPath = _cacheFoderPath + "/" + folderName;
			if (!Directory.Exists(directoryPath))
			{
				return Directory.CreateDirectory(directoryPath);
			}

			return new DirectoryInfo(directoryPath);
		}
	}
}
