﻿using Arbitrage.Models.Wallet;
using Arbitrage.Planner.Graph;
using System.Collections.Generic;

namespace Arbitrage.Planner.Wallet
{
	internal class WalletCollection
	{
		private readonly IList<IWallet> _wallets;

		public WalletCollection(IList<Node> nodes) : this(Wallets(nodes))
		{
		}

		public WalletCollection(IList<IWallet> wallets)
		{
			_wallets = wallets;
		}

		public IList<IWallet> ToList()
		{
			return _wallets;
		}

		private static IList<IWallet> Wallets(IList<Node> nodes)
		{
			var wallets = new List<IWallet>();
			foreach (var node in nodes)
			{
				if (node.Value is WalletNodeValue walletNodeValue)
				{
					if (!wallets.Contains(walletNodeValue.Wallet))
					{
						wallets.Add(walletNodeValue.Wallet);
					}
				}
			}

			return wallets;
		}
	}
}
