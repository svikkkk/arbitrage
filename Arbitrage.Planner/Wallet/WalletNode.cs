﻿using Arbitrage.Models.Wallet;
using Arbitrage.Planner.Costs;
using Arbitrage.Planner.Exchange;
using Arbitrage.Planner.Graph;
using Arbitrage.Planner.Interfaces;
using Arbitrage.Planner.Plan.Steps;

namespace Arbitrage.Planner.Wallet
{
	public class WalletNodeValue : INodeValue
	{
		public IWallet Wallet { get; }

		public WalletNodeValue(IWallet wallet)
		{
			Wallet = wallet;
		}

		public IPlanStep ToAction(INodeValue nextNode)
		{
			var exchangeNodeValue = (ExchangeNodeValue)nextNode;
			return new WalletDepositStep(Wallet, exchangeNodeValue.Exchange, exchangeNodeValue.Coin);
		}

		public bool Equals(INodeValue node)
		{
			if (node is WalletNodeValue walletNode)
			{
				return Wallet == walletNode.Wallet;
			}

			return false;
		}
	}

	public class WalletNode : Node
	{
		public IWallet Wallet => ((WalletNodeValue)Value).Wallet;

		public WalletNode(IWallet wallet) : this(new WalletNodeValue(wallet))
		{
		}

		public WalletNode(WalletNodeValue value) : base(value)
		{
		}

		public void DepositPath(ExchangeNode exchangeGraphNode)
		{
			AddNeighbor(exchangeGraphNode, new DepositCost(Wallet.Coin, exchangeGraphNode.Exchange));
		}
	}
}
