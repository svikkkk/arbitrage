﻿using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Wallet;
using Arbitrage.Planner.Graph;
using Arbitrage.Planner.Plan;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Arbitrage.Planner.Wallet
{
	public class WalletPlansCollection
	{
		private readonly IWallet _wallet;

		private readonly decimal _amount;

		private readonly IList<IExchange> _exchanges;

		public WalletPlansCollection(IWallet wallet, decimal amount, IList<IExchange> exchanges)
		{
			_wallet = wallet;
			_amount = amount;
			_exchanges = exchanges;
		}

		public async Task<IList<TradingPlan>> ToList()
		{
			await new ExchangeBalanceValidator(_wallet).Validate(_amount);

			var wallets = new List<IWallet> { _wallet };

			var tradingSet = new TradingSet(_exchanges, wallets);
			var tradingStartPointsFilter = new TradingStartPoints(wallets);

			// Act

			var walletTradeTreeList = await new TradingGraph(tradingSet, tradingStartPointsFilter, null).ToTreeList();

			return new List<TradingPlan>();
		}
	}
}
