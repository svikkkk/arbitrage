﻿using Arbitrage.Models;
using Arbitrage.Models.Exchange;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Wallet;
using Arbitrage.Planner.Exchange;
using Arbitrage.Planner.Graph;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Arbitrage.Planner.Wallet
{
	/// <summary>
	/// Links Walllet and other exchanges making deposit and withdraw link
	/// </summary>
	public class WalletExchangeLinkedNodesCollection
	{
		private readonly ExchangeMarketNodesCollection _exchangeMarketNodes;

		public IEnumerable<WalletNode> WalletNodes { get; }

		public WalletExchangeLinkedNodesCollection(IEnumerable<IWallet> wallets, ExchangeMarketNodesCollection exchangeMarketNodes)
			: this(wallets.Select(w => new WalletNode(w)), exchangeMarketNodes)
		{
		}

		public WalletExchangeLinkedNodesCollection(IEnumerable<WalletNode> walletNodes, ExchangeMarketNodesCollection exchangeMarketNodes)
		{
			WalletNodes = walletNodes;
			_exchangeMarketNodes = exchangeMarketNodes;
		}

		public async Task<IList<Node>> ToList()
		{
			var exchangeMarketNodesList = await _exchangeMarketNodes.ToList();
			var coinExchangesList = await new SupportedCoinsExchangeCollection(Coins(WalletNodes), _exchangeMarketNodes.Exchanges()).ToList();
			var result = new List<Node>(exchangeMarketNodesList);
			foreach (var walletNode in WalletNodes)
			{
				foreach (var exchange in coinExchangesList)
				{
					var coinExchangeNode = CoinExchangeNode(exchangeMarketNodesList, walletNode.Wallet.Coin, exchange);
					walletNode.DepositPath(coinExchangeNode);
					coinExchangeNode.WithdrawPath(walletNode);
				}

				result.Add(walletNode);
			}

			return result;
		}

		private IEnumerable<Coin> Coins(IEnumerable<WalletNode> walletNodes)
		{
			return walletNodes.Select(w => w.Wallet.Coin).Distinct(new CoinCompare());
		}

		private ExchangeNode CoinExchangeNode(IList<ExchangeNode> exchangeNodes, Coin coin, IExchange exchange)
		{
			ExchangeNode coinExchangeNode = null;
			foreach (var exchangeNode in exchangeNodes)
			{
				if (exchangeNode.Exchange == exchange && exchangeNode.Coin.Equals(coin))
				{
					coinExchangeNode = exchangeNode;
					break;
				}
			}

			return coinExchangeNode;
		}
	}
}
