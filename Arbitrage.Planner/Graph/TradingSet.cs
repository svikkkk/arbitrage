﻿using Arbitrage.Models;
using Arbitrage.Models.Exchange;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Wallet;
using Arbitrage.Planner.Exchange;
using Arbitrage.Planner.Wallet;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Arbitrage.Planner.Graph
{
	public class TradingSet
	{
		private readonly IEnumerable<Coin> _coins;
		private readonly IList<IExchange> _exchanges;
		private readonly IEnumerable<IWallet> _wallets;

		public int MaxDepth { get; }

		public TradingSet(
			IList<IExchange> exchanges,
			IEnumerable<IWallet> wallets,
			int maxDepth = 0) : this(new List<Coin>(), exchanges, wallets, maxDepth)
		{
		}

		public TradingSet(
			IEnumerable<Coin> coins,
			IList<IExchange> exchanges,
			int maxDepth = 0) : this(coins, exchanges, null, maxDepth)
		{
		}

		public TradingSet(
			IEnumerable<Coin> coins,
			IList<IExchange> exchanges,
			IEnumerable<IWallet> wallets,
			int maxDepth = 0)
		{
			_coins = coins;
			_exchanges = exchanges;
			_wallets = wallets;
			MaxDepth = maxDepth;
		}

		public async Task<IList<IExchange>> CoinExchanges()
		{
			if (_coins.Any())
			{
				return await new SupportedCoinsExchangeCollection(_coins, _exchanges).ToList();
			}

			return _exchanges;
		}

		public IEnumerable<IWallet> CoinWallets()
		{
			if (_coins.Any())
			{
				return new CoinWalletCollection(_coins, _wallets).ToList();
			}

			return _wallets;
		}

		public async Task<IList<Node>> TradingNodes()
		{
			var coinExchanges = await CoinExchanges();
			var walletExchangesNodesCollection = new WalletExchangeLinkedNodesCollection(
				CoinWallets(),
				new ExchangeMarketNodesCollection(_coins.ToList(), coinExchanges));

			return await walletExchangesNodesCollection.ToList();
		}
	}
}
