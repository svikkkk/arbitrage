﻿using Arbitrage.Models;
using Arbitrage.Models.Exchange.Interfaces;
using Arbitrage.Models.Wallet;
using Arbitrage.Planner.Exchange;
using Arbitrage.Planner.Wallet;
using System.Collections.Generic;
using System.Linq;

namespace Arbitrage.Planner.Graph
{
	public class TradingStartPoints
	{
		private readonly IEnumerable<Coin> _startFromCoins;
		private readonly IList<IExchange> _startFromExchanges;
		private readonly IEnumerable<IWallet> _startFromWallets;

		public TradingStartPoints() : this(null, null, null)
		{

		}

		public TradingStartPoints(IEnumerable<Coin> startFromCoins)
			: this(startFromCoins, null)
		{
		}

		public TradingStartPoints(IList<IExchange> startFromExchanges)
			: this(null, startFromExchanges, new List<IWallet>())
		{
		}

		public TradingStartPoints(IEnumerable<IWallet> startFromWallets)
			: this(new List<Coin>(), new List<IExchange>(), startFromWallets)
		{
		}

		public TradingStartPoints(IEnumerable<Coin> startFromCoins, IList<IExchange> startFromExchanges)
			: this(startFromCoins, startFromExchanges, null)
		{

		}

		public TradingStartPoints(
			IEnumerable<Coin> startFromCoins,
			IList<IExchange> startFromExchanges,
			IEnumerable<IWallet> startFromWallets)
		{
			_startFromCoins = startFromCoins;
			_startFromExchanges = startFromExchanges;
			_startFromWallets = startFromWallets;
		}

		public IList<Node> StartPointsFilter(IList<Node> allPossibleNodes)
		{
			var filteredNodes = new List<Node>();
			foreach (var node in allPossibleNodes)
			{
				if (CanBeStartPoint(node))
				{
					filteredNodes.Add(node);
				}
			}

			return filteredNodes;
		}

		private bool CanBeStartPoint(Node node)
		{
			return FitsToStartFromExchange(node) || FitsStartFromWallet(node);
		}

		private bool FitsStartFromWallet(Node node)
		{
			var isFit = false;
			if (node.Value is WalletNodeValue walletNodeValue)
			{
				if (_startFromWallets != null)
				{
					isFit = _startFromWallets.Contains(walletNodeValue.Wallet);
				}
				else if (_startFromCoins != null)
				{
					isFit = IsCoinSupported(walletNodeValue.Wallet.Coin);
				}
				else
				{
					isFit = true;
				}
			}

			return isFit;
		}

		private bool FitsToStartFromExchange(Node node)
		{
			var isFit = false;
			if (node.Value is ExchangeNodeValue exchangeNodeValue)
			{
				if (_startFromExchanges != null)
				{
					isFit = _startFromExchanges.Contains(exchangeNodeValue.Exchange);
					if (isFit && _startFromCoins != null)
					{
						isFit = IsCoinSupported(exchangeNodeValue.Coin);
					}
				}
				else if (_startFromCoins != null)
				{
					isFit = IsCoinSupported(exchangeNodeValue.Coin);
				}
				else
				{
					isFit = true;
				}
			}

			return isFit;
		}

		private bool IsCoinSupported(Coin coin)
		{
			return _startFromCoins.Count(c => c.Equals(coin)) > 0;
		}
	}
}
