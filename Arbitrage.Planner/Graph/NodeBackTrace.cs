﻿using Arbitrage.Planner.Tree;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Arbitrage.Planner.Graph
{
	public class NodeBackTrace
	{
		private readonly int _maxDepth;
		private readonly List<Node> _nodeStack;

		public NodeBackTrace(int maxDepth = 0)
		{
			_maxDepth = maxDepth;
			_nodeStack = new List<Node>();
		}

		public NodeBackTrace(int capacity, int maxDepth = 0)
		{
			_maxDepth = maxDepth;
			_nodeStack = new List<Node>(capacity);
		}

		public Node Last()
		{
			return _nodeStack.Count > 0 ? _nodeStack.Last() : null;
		}

		public void Push(Node node)
		{
			_nodeStack.Add(node);
		}

		public void Pop()
		{
			if (_nodeStack.Count > 0)
			{
				_nodeStack.RemoveAt(_nodeStack.Count - 1);
			}
		}

		/// <summary>
		/// Method return true in the case when this nodes pair is already in the stack
		/// </summary>
		/// <param name="nodeFrom"></param>
		/// <param name="nodeTo"></param>
		/// <returns></returns>
		public bool IsCircularStep(Node nodeFrom, Node nodeTo)
		{
			var isCircularStep = false;
			for (int i = 0; i < _nodeStack.Count - 1; i++)
			{
				if (_nodeStack[i] == nodeFrom && _nodeStack[i + 1] == nodeTo)
				{
					isCircularStep = true;
					break;
				}
			}

			return isCircularStep;
		}

		public void Clear()
		{
			_nodeStack.Clear();
		}

		public bool IsMaxDepthExceeded(int currentDepth)
		{
			return _maxDepth > 0 && currentDepth > _maxDepth;
		}

		public void BuildTree(TradingTree tradingTree, Node leafNode)
		{
			var currentTreeNode = tradingTree.RootNode;
			for (int nodeIndex = 0; nodeIndex < _nodeStack.Count; nodeIndex++)
			{
				var node = _nodeStack[nodeIndex];
				if (node.Id != currentTreeNode.DataNodeId)
				{
					var child = currentTreeNode.Child(node.Id);
					currentTreeNode = child ?? currentTreeNode.Add(node.Id);
				}
			}

			var childLeft = currentTreeNode.Child(leafNode.Id);
			if (childLeft != null)
			{
				throw new Exception($"Dublicate child leaf parameter");
			}

			currentTreeNode.Add(leafNode.Id);
		}
	}
}
