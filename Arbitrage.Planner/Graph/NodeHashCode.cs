﻿using Arbitrage.Planner.HashCode;
using System.Collections.Generic;
using System.Text;

namespace Arbitrage.Planner.Graph
{
	public class NodeHashCode
	{
		private readonly Node _node;
		private readonly IHashCode _hashCode;

		public NodeHashCode(Node node, IHashCode hashCode)
		{
			_node = node;
			_hashCode = hashCode;
		}

		public override string ToString()
		{
			var stringBuilder = new StringBuilder();
			stringBuilder.AppendLine(_node.ToString());

			AppendNodeChildrenDetails(stringBuilder);

			return _hashCode.GetHashCodeAndConvertToX2(stringBuilder.ToString());
		}

		private void AppendNodeChildrenDetails(StringBuilder stringBuilder)
		{
			var nodeDetails = NodeFullDetails();
			nodeDetails.Sort();
			foreach (var nodeDetail in nodeDetails)
			{
				stringBuilder.AppendLine(nodeDetail);
			}
		}

		private List<string> NodeFullDetails()
		{
			var details = new List<string>(_node.Neighbors.Count);
			for (var i = 0; i < _node.Neighbors.Count; i++)
			{
				details.Add(_node.Neighbors[i].ToString());
			}

			return details;
		}
	}
}
