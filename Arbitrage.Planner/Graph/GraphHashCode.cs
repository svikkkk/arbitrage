﻿using Arbitrage.Planner.HashCode;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arbitrage.Planner.Graph
{
	public class GraphHashCode
	{
		private readonly TradingGraph _tradingGraph;
		private readonly IHashCode _hashCode;

		public GraphHashCode(TradingGraph tradingGraph, IHashCode hashCode)
		{
			_tradingGraph = tradingGraph;
			_hashCode = hashCode;
		}

		public new async Task<string> ToString()
		{
			var nodesHashCodes = await NodesHashCodes();
			var stringBuilder = new StringBuilder();
			foreach (var nodesHashCode in nodesHashCodes)
			{
				stringBuilder.AppendLine(nodesHashCode);
			}

			return _hashCode.GetHashCodeAndConvertToX2(stringBuilder.ToString());
		}

		private async Task<List<string>> NodesHashCodes()
		{
			var nodes = await _tradingGraph.ToNodesList();
			var nodesHashCodes = nodes.Select(node => new NodeHashCode(node, _hashCode).ToString()).ToList();
			nodesHashCodes.Sort();
			return nodesHashCodes;
		}
	}
}
