﻿using Arbitrage.Planner.Cache;
using Arbitrage.Planner.Tree;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Arbitrage.Planner.Graph
{
	public class TradingGraph
	{
		private readonly TradingSet _tradingSet;

		private readonly TradingStartPoints _tradingStartPoints;
		private readonly CancellationTokenSource _cancellationTokenSource;
		private readonly ITreeCache _treeCache;

		private IList<Node> _nodesList;

		private string _graphHashCode;

		public TradingGraph(
			TradingSet tradingSet, 
			TradingStartPoints tradingStartPoints, 
			CancellationTokenSource cancellationTokenSource, 
			ITreeCache treeCache = null)
		{
			_tradingSet = tradingSet;
			_tradingStartPoints = tradingStartPoints;
			_cancellationTokenSource = cancellationTokenSource;
			_treeCache = treeCache;
		}

		public async Task<IList<Node>> ToNodesList()
		{
			if (_nodesList == null)
			{
				_nodesList = await _tradingSet.TradingNodes();
			}

			return _nodesList;
		}

		public async Task<Node> Node(int nodeId)
		{
			var nodes = await ToNodesList();
			return nodes.FirstOrDefault(n => n.Id == nodeId);
		}

		public async Task<IList<TradingTree>> ToTreeList()
		{
			var startFromNodes = _tradingStartPoints.StartPointsFilter(await ToNodesList());

			var treeTaskList = new List<Task<TradingTree>>(startFromNodes.Count);
			foreach (var startFromNode in startFromNodes)
			{
				_cancellationTokenSource.Token.ThrowIfCancellationRequested();
				treeTaskList.Add(BuildTree(startFromNode));
			}

			await Task.WhenAll(treeTaskList);
			return treeTaskList.Select(t => t.Result).ToList();
		}

		public async Task<string> ToHashCode(bool cached = true)
		{
			if (string.IsNullOrEmpty(_graphHashCode) || !cached)
			{
				_graphHashCode = await new GraphHashCode(this, new HashCode.HashCode()).ToString();
			}

			return _graphHashCode;
		}

		private async Task<TradingTree> BuildTree(Node node)
		{
			TradingTree tree;
			if (_treeCache != null)
			{
				tree = await _treeCache.Get(this, node, _tradingSet.MaxDepth);
				if (tree != null)
				{
					return tree;
				}
			}

			tree = await new TreeBuilder(node, this, _cancellationTokenSource).ToTree(_tradingSet.MaxDepth);
			if (_treeCache != null)
			{
				await _treeCache.Add(this, tree, _tradingSet.MaxDepth);
			}

			return tree;
		}
	}
}
