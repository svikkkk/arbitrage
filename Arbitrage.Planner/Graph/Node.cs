﻿using Arbitrage.Planner.Exchange;
using Arbitrage.Planner.Interfaces;
using Arbitrage.Planner.Wallet;
using System.Collections.Generic;

namespace Arbitrage.Planner.Graph
{
	public interface INodeValue
	{
		IPlanStep ToAction(INodeValue nextNode);

		bool Equals(INodeValue node);
	}

	public class Node
	{
		private int? _id;

		public int Id
		{
			get
			{
				if (!_id.HasValue)
				{
					_id = GetHashCode();
				}

				return _id.Value;
			}
			set => _id = value;
		}

		public INodeValue Value { get; set; }

		public List<ICost> NeighborsCost { get; set; } = new List<ICost>();

		public List<Node> Neighbors { get; set; } = new List<Node>();

		public Node(INodeValue value)
		{
			Value = value;
		}

		public void AddNeighbor(Node neighbor, ICost cost)
		{
			Neighbors.Add(neighbor);
			NeighborsCost.Add(cost);
		}

		public override string ToString()
		{
			if (Value is WalletNodeValue walletNodeValue)
			{
				return $"W:{walletNodeValue.Wallet.Address}-{walletNodeValue.Wallet.Coin}";
			}

			if (Value is ExchangeNodeValue exchangeNodeValue)
			{
				return $"E:{exchangeNodeValue.Exchange.Name}-{exchangeNodeValue.Coin}";
			}

			return string.Empty;
		}

		public string ToHashCode()
		{
			return new NodeHashCode(this, new HashCode.HashCode()).ToString();
		}

		public ICost Cost(int dataNodeId)
		{
			for (int nodeIndex = 0; nodeIndex < Neighbors.Count; nodeIndex++)
			{
				if (Neighbors[nodeIndex].Id == dataNodeId)
				{
					return NeighborsCost[nodeIndex];
				}
			}

			return null;
		}
	}
}
