﻿using System;
using Arbitrage.Planner.Tree;
using System.Collections.Generic;
using System.Text;

namespace Arbitrage.Planner.Graph
{
	public class BackTraceItem
	{
		public int NodeId { get; }
		public BackTraceItem Parent { get; }

		public int Count
		{
			get
			{
				var count = 0;
				var parent = this;
				while (parent != null)
				{
					count++;
					parent = parent.Parent;
				}

				return count;
			}
		}

		public BackTraceItem(int nodeId, BackTraceItem parent)
		{
			NodeId = nodeId;
			Parent = parent;
		}

		/// <summary>
		/// Method return true in the case when this nodes pair is already in the stack
		/// </summary>
		/// <param name="nodeFromId"></param>
		/// <param name="nodeToId"></param>
		/// <returns></returns>
		public bool IsCircularStep(int nodeFromId, int nodeToId)
		{
			var isCircularStep = false;
			var current = this;
			var parent = current.Parent;
			while (parent != null)
			{
				if (parent.NodeId == nodeFromId && current.NodeId == nodeToId)
				{
					isCircularStep = true;
					break;
				}

				current = parent;
				parent = current.Parent;
			}

			return isCircularStep;
		}

		public bool HasNode(int nodeId)
		{
			var hasNode = false;
			var current = this;
			while (current != null)
			{
				if (current.NodeId == nodeId)
				{
					hasNode = true;
					break;
				}

				current = current.Parent;
			}

			return hasNode;
		}

		public void BuildTree(TradingTree tradingTree)
		{
			var currentTreeNode = tradingTree.RootNode;
			var nodeStack = ToList();

			// nodeIndex == 0 it's a root node
			for (int nodeIndex = 1; nodeIndex < nodeStack.Count; nodeIndex++)
			{
				var stackItem = nodeStack[nodeIndex];
				currentTreeNode = currentTreeNode.Add(stackItem.NodeId);
			}
		}

		private IList<BackTraceItem> ToList()
		{
			var parent = this;
			var nodeStack = new List<BackTraceItem>();
			while (parent != null)
			{
				nodeStack.Add(parent);
				parent = parent.Parent;
			}

			nodeStack.Reverse();
			return nodeStack;
		}
	}
}
